/**
 * @author egiadtya
 */
package com.edukasi.kelase.callbacks;

public interface OnCallAPI {
	public void onAPIsuccess();
	public void onAPIFailed(String errorMessage);
	public void executeAPI();
}

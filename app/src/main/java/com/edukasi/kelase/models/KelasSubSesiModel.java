package com.edukasi.kelase.models;

import android.content.Context;

import com.edukasi.kelase.entities.KelasSubSesi;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu-rizky on 11/29/15.
 */

public class KelasSubSesiModel extends BaseModel<KelasSubSesi> {

    public KelasSubSesiModel(Context context){
        super(context, KelasSubSesi.class);
    }

    public List<KelasSubSesi> byIdSesi(String idKelas, String idSesi){
        List<KelasSubSesi> kelasSubSesi = new ArrayList<>();
        try {
            QueryBuilder<KelasSubSesi, ?> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq("ID_KELAS", idKelas).and().eq("ID_SESI", idSesi);
            kelasSubSesi = queryBuilder.orderBy("id", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kelasSubSesi;
    }
}

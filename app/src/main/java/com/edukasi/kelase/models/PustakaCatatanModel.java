package com.edukasi.kelase.models;

import android.content.Context;

import com.edukasi.kelase.entities.PustakaCatatan;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaCatatanModel extends BaseModel<PustakaCatatan> {

    public PustakaCatatanModel(Context context) {
        super(context, PustakaCatatan.class);
    }
}

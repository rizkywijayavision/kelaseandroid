package com.edukasi.kelase.models;

import android.content.Context;

import com.edukasi.kelase.entities.PustakaTautan;

/**
 * Created by ebizu-rizky on 1/25/16.
 */
public class PustakaTautanModel extends BaseModel<PustakaTautan> {

    public PustakaTautanModel(Context context) {
        super(context, PustakaTautan.class);
    }
}

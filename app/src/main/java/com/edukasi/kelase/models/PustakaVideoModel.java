package com.edukasi.kelase.models;

import android.content.Context;

import com.edukasi.kelase.entities.PustakaVideo;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaVideoModel extends BaseModel<PustakaVideo> {

    public PustakaVideoModel(Context context) {
        super(context, PustakaVideo.class);
    }
}

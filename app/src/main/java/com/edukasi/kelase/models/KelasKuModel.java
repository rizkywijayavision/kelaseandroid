package com.edukasi.kelase.models;

import android.content.Context;

import com.edukasi.kelase.entities.KelasKu;

/**
 * Created by ebizu-rizky on 11/29/15.
 */
public class KelasKuModel extends BaseModel<KelasKu> {

    public KelasKuModel(Context context) {
        super(context, KelasKu.class);
    }

}

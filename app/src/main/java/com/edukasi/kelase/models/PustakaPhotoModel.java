package com.edukasi.kelase.models;

import android.content.Context;

import com.edukasi.kelase.entities.PustakaPhoto;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaPhotoModel extends BaseModel<PustakaPhoto> {

    public PustakaPhotoModel(Context context) {
        super(context, PustakaPhoto.class);
    }
}

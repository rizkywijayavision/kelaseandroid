package com.edukasi.kelase.models;

import android.content.Context;

import com.edukasi.kelase.entities.BaseEntity;
import com.edukasi.kelase.entities.Kelas;
import com.edukasi.kelase.entities.KelasKu;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu-rizky on 11/29/15.
 */
public class KelasModel extends BaseModel<Kelas> {

    public KelasModel(Context context) {
        super(context, Kelas.class);
    }

    public List<Kelas> byType(String Type){
        List<Kelas> kelas = new ArrayList<>();
        try {
            QueryBuilder<Kelas, ?> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq("TYPE_KELAS", Type);
            kelas = queryBuilder.orderBy(BaseEntity.ID, true).query();
            for (Kelas kelasNew : kelas){
                kelasNew.setUpdateDate(0l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kelas;
    }

    public List<Kelas> allKelasKu(){
        List<Kelas> kelas = new ArrayList<>();
        try {
            QueryBuilder<KelasKu, ?> kelasKuQb = dbHelper.getDao(KelasKu.class).queryBuilder();
            for (KelasKu kelasKu : kelasKuQb.query()){
                kelas.addAll(findMany(kelasKu.getId()));
                kelasKu.setUpdateDate(0l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kelas;
    }

}

package com.edukasi.kelase.models;

import android.content.Context;

import com.edukasi.kelase.entities.KelasSesi;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu-rizky on 11/29/15.
 */

public class KelasSesiModel extends BaseModel<KelasSesi> {

    public KelasSesiModel(Context context){
        super(context, KelasSesi.class);
    }

    public List<KelasSesi> byIdKelas(String idKelas){
        List<KelasSesi> kelasSesi = new ArrayList<>();
        try {
            kelasSesi =  dao.queryBuilder().where().eq("ID_KELAS", idKelas)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kelasSesi;
    }

}

package com.edukasi.kelase.activities;

import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.edukasi.kelase.R;

/**
 * Created by ebizu-rizky on 11/24/15.
 */

public class ProfileSettingActivity extends BaseActivity {

    private TextView editProfile, changePass, logout;

    @Override
    public void initView() {
        setContentView(R.layout.activity_setting);
        editProfile = (TextView) findViewById(R.id.setting_edit_profile);
        changePass = (TextView) findViewById(R.id.setting_change_password);
        logout = (TextView) findViewById(R.id.setting_logout);
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {
        editProfile.setOnClickListener(onClickListener);
        changePass.setOnClickListener(onClickListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id==android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            if (view == editProfile){
                Intent editprofileIntent = new Intent(ProfileSettingActivity.this, ProfileEditActivity.class);
                startActivity(editprofileIntent);
                finish();
            }
            if (view == changePass){
                Intent changeIntent = new Intent(ProfileSettingActivity.this, ProfileChangePasswordActivity.class);
                startActivity(changeIntent);
                finish();
            }
        }
    };
}

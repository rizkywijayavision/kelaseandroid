package com.edukasi.kelase.activities.subsesiactivities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.BaseInfoActivity;
import com.edukasi.kelase.controllers.SubmitTugasController;
import com.edukasi.kelase.entities.KelasSubSesi;
import com.edukasi.kelase.models.KelasSubSesiModel;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import java.io.File;

/**
 * Created by ebizu-rizky on 12/2/15.
 */
public class TugasActivity extends BaseInfoActivity{

    private WebView webView;
    private String id_kelas, idSesi, idSubSesi;
    private KelasSubSesi kelasSubSesi;
    private Button chooseFileButton;
    private ProgressDialog progress;
    private String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id_kelas = bundle.getString("ID_KELAS");
            idSesi = bundle.getString("ID_SESI");
            idSubSesi = bundle.getString("ID_SUBSESI");
        }
        KelasSubSesiModel kelasSubSesiModel = new KelasSubSesiModel(this);
        kelasSubSesi = (KelasSubSesi) kelasSubSesiModel.find(idSubSesi);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_tugas);
        webView = (WebView) findViewById(R.id.launcherWebView);
        if(kelasSubSesi != null){
            setTitle(kelasSubSesi.getJUDUL_SUB_SESI());
            webView.loadData(kelasSubSesi.getDESKRIPSI_SUB_SESI(),"text/html; charset=UTF-8", null);
        }
        chooseFileButton = (Button) findViewById(R.id.button_choose_file);

        progress = new ProgressDialog(this);
        progress.setMessage("Upload Tugas");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.setButton(ProgressDialog.BUTTON_POSITIVE, "Cancel", dialogClickListener);
        progress.setButton(ProgressDialog.BUTTON_NEGATIVE, "Retry", dialogClickListener);
        progress.setProgress(0);
    }

    @Override
    public void setUICallbacks() {
        chooseFileButton.setOnClickListener(onClickListener);

    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    private View.OnClickListener onClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            if (view == chooseFileButton){
                Intent intent = new Intent(getApplicationContext(), FilePickerActivity.class);
                startActivityForResult(intent, 1);
            }
        }
    };

    private DialogInterface.OnClickListener dialogClickListener =  new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            //button click stuff here
            switch (which){
                case ProgressDialog.BUTTON_NEGATIVE :
                    progress.dismiss();
                    break;
                case ProgressDialog.BUTTON_POSITIVE :
                    break;
            }
        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {
            filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            File image = new File(filePath);
            // Do anything with file
            progress.show();
            progress.getButton(ProgressDialog.BUTTON_NEGATIVE).setVisibility(View.INVISIBLE);
            new SubmitTugasController(context, id_kelas, idSesi, idSubSesi, App.session.getUserDetails().getId(), App.session.getUserDetails().getRole_id(), image){
                @Override
                public void onAPIsuccess() {
                    super.onAPIsuccess();
                    Toast.makeText(getApplicationContext(), "success upload", Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                    finish();
                }

                @Override
                public void onAPIFailed(String errorMessage) {
                    super.onAPIFailed(errorMessage);
                    Toast.makeText(getApplicationContext(), "failed upload", Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                }
            }.executeAPI();
        }
    }

}

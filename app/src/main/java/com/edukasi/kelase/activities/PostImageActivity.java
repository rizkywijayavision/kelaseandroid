package com.edukasi.kelase.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.controllers.PostStatusController;
import com.edukasi.kelase.controllers.SendImageController;

import java.io.File;
import java.util.ArrayList;

import me.iwf.photopicker.PhotoPickerActivity;
import me.iwf.photopicker.utils.PhotoPickerIntent;

/**
 * Created by ebizu-rizky on 11/20/15.
 */

public class PostImageActivity extends BaseActivity {

    private EditText statusMsg;
    private ProgressDialog progressDialog;
    private ArrayList<String> photos;
    private ImageView photoMoment;
    private final static int REQUEST_CODE = 1;
    private PostStatusController postStatusController;
    private SendImageController sendImageController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PhotoPickerIntent intent = new PhotoPickerIntent(this);
        intent.setPhotoCount(1);
        intent.setShowCamera(true);
        intent.setShowGif(true);
        startActivityForResult(intent, REQUEST_CODE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_post_image);
        statusMsg = (EditText) findViewById(R.id.postmoment_momentstatus_txtv);
        statusMsg.requestFocus();

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Sedang Proses ...");
        progressDialog.setMessage("Upload ...");

        photoMoment = (ImageView) findViewById(R.id.postmoment_propic_imgv);
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            if (data != null) {
                photos = data.getStringArrayListExtra(PhotoPickerActivity.KEY_SELECTED_PHOTOS);
                Bitmap thumbnail = (BitmapFactory.decodeFile(photos.get(0).toString()));
                photoMoment.setVisibility(View.VISIBLE);
                photoMoment.setImageBitmap(thumbnail);
            }
        } else {
            onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate(R.menu.menu_post_status, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        if (id==android.R.id.home){
            Intent successIntent = new Intent(this, HomeActivity.class);
            startActivity(successIntent);
            finish();
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_post) {
            postStatus();

        }

        return super.onOptionsItemSelected(item);
    }

    public void postStatus(){
        final String statusContent = statusMsg.getText().toString();
        if(statusContent != null || statusContent.equals("")){
            progressDialog.show();

            File image = new File(photos.get(0).toString());

            sendImageController = new SendImageController(context, image){
                @Override
                public void onAPIsuccess() {
                    super.onAPIsuccess();
                    new PostStatusController(context, App.session.getUserDetails().getId(), statusContent, sendImageParser.getResultSingle()){
                        @Override
                        public void onAPIsuccess() {
                            super.onAPIsuccess();
                            progressDialog.dismiss();
                            Intent successIntent = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(successIntent);
                            finish();
                        }

                        @Override
                        public void onAPIFailed(String errorMessage) {
                            super.onAPIFailed(errorMessage);
                            progressDialog.dismiss();
                        }
                    }.executeAPI();
                }

                @Override
                public void onAPIFailed(String errorMessage) {
                    super.onAPIFailed(errorMessage);
                    progressDialog.dismiss();
                }
            };
            sendImageController.executeAPI();
        }
    }
}

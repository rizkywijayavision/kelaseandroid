package com.edukasi.kelase.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.edukasi.kelase.R;
import com.edukasi.kelase.base.interfaces.ActivityInterface;
import com.edukasi.kelase.helpers.AlertHelper;

/**
 * Created by ebizu-rizky on 11/4/15.
 */

public abstract class BaseActivity extends AppCompatActivity implements ActivityInterface {

    protected Context context;
    protected ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        initView();
        setUICallbacks();
        updateUI();
        showCustomActionBar();
    }

    public void showCustomActionBar() {
        showCustomActionBar(R.layout.actionbar_centered_logo);
    }

    public void showCustomActionBar(int resourceView) {
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setCustomView(resourceView);
        }
    }

    @Override
    public void onBackPressed() {
        String activityName = getClass().getSimpleName();
        if (activityName.equalsIgnoreCase("HomeActivity")) {
            AlertDialog.Builder confirmation = AlertHelper.getInstance().showAlertWithoutListener(this, "Confirmation", "Are you sure you want to exit?");
            confirmation.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            confirmation.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    exit();
                }
            });
            confirmation.show();
        } else {
            exit();
        }
    }

    private void exit() {
        if(this.getParentActivityIntent() != null) startActivity(this.getParentActivityIntent());
        super.onBackPressed();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id==android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}

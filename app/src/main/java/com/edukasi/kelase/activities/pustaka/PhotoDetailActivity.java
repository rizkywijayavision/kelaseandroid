package com.edukasi.kelase.activities.pustaka;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.Util;
import com.edukasi.kelase.activities.BaseActivity;
import com.edukasi.kelase.entities.KelaseUtil;
import com.edukasi.kelase.entities.PustakaPhoto;
import com.edukasi.kelase.helpers.PictureHelper;
import com.edukasi.kelase.models.PustakaPhotoModel;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PhotoDetailActivity extends BaseActivity {

    private ImageView image;
    private TextView judul, date;
    private String idPustaka;
    private PustakaPhoto pustakaPhoto;
    private PictureHelper pictureHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            idPustaka = bundle.getString("id_pustaka");
            PustakaPhotoModel pustakaPhotoModel = new PustakaPhotoModel(this);
            pustakaPhoto = (PustakaPhoto) pustakaPhotoModel.find(idPustaka);
        }
        pictureHelper = PictureHelper.getInstance(context);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_pustaka_photo_detail);
        image = (ImageView) findViewById(R.id.image);
        judul = (TextView) findViewById(R.id.judul_catatan);
        date = (TextView) findViewById(R.id.date_created);


        if (pustakaPhoto != null){
            pictureHelper.displayImage(KelaseUtil.KELASE_SERVICE_IMAGE_URL + pustakaPhoto.getFILE_FOTO(), image);
            judul.setText(pustakaPhoto.getNAMA_FOTO());
            date.setText(Util.prettyDate(pustakaPhoto.getDATE_CREATED()));
        }
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }
}

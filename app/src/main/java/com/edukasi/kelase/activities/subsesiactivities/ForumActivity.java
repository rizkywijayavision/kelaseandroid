package com.edukasi.kelase.activities.subsesiactivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.Util;
import com.edukasi.kelase.activities.BaseInfoActivity;
import com.edukasi.kelase.adapters.TopikAdapter;
import com.edukasi.kelase.controllers.TopikController;
import com.edukasi.kelase.entities.KelasSubSesi;
import com.edukasi.kelase.entities.SubSesiTopik;
import com.edukasi.kelase.models.KelasSubSesiModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class ForumActivity extends BaseInfoActivity implements AdapterView.OnItemClickListener  {

    private WebView webView;
    private String id_kelas, idSesi, idSubSesi;
    private KelasSubSesi kelasSubSesi;
    private ProgressDialog progress;
    private ListView topikListView;
    private TopikAdapter topikAdapter;
    private List<SubSesiTopik> listTopik;
    private Button tambahTopik;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id_kelas = bundle.getString("ID_KELAS");
            idSesi = bundle.getString("ID_SESI");
            idSubSesi = bundle.getString("ID_SUBSESI");
        }
        KelasSubSesiModel kelasSubSesiModel = new KelasSubSesiModel(this);
        kelasSubSesi = (KelasSubSesi) kelasSubSesiModel.find(idSubSesi);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_forum);
        webView = (WebView) findViewById(R.id.launcherWebView);
        if(kelasSubSesi != null){
            setTitle(kelasSubSesi.getJUDUL_SUB_SESI());
            webView.loadData(kelasSubSesi.getDESKRIPSI_SUB_SESI(), "text/html; charset=UTF-8", null);
        }
        setupWebView();
        progress = ProgressDialog.show(this, "Loading Data",
                "Mohon tunggu sebentar...", true);

        topikListView = (ListView) findViewById(R.id.topik_list_view);
        topikAdapter = new TopikAdapter(this);
        listTopik = new ArrayList<>();
        topikAdapter.setData(listTopik);
        topikListView.setAdapter(topikAdapter);
        tambahTopik = (Button) findViewById(R.id.tambah_topik_button);
    }

    @Override
    public void setUICallbacks() {
        tambahTopik.setOnClickListener(onClickListener);
        topikListView.setOnItemClickListener(this);
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {
        new TopikController(this, id_kelas, idSesi, idSubSesi, App.session.getUserDetails().getId(), App.session.getUserDetails().getRole_id()){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                progress.dismiss();
                listTopik.addAll(topikParser.getResult());
                topikAdapter.notifyDataSetChanged();
                Util.setListViewHeightBasedOnChildren(topikListView);
            }

            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                progress.dismiss();
            }
        }.executeAPI();

    }

    private void setupWebView() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                webView.loadUrl("javascript:MyApp.resize(document.body.getBoundingClientRect().height)");
                super.onPageFinished(view, url);
            }
        });
        webView.addJavascriptInterface(this, "MyApp");
    }

    @JavascriptInterface
    public void resize(final float height) {
        ForumActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                float newHeight = height + 10f;
                webView.setLayoutParams(new LinearLayout.LayoutParams(getResources().getDisplayMetrics().widthPixels, (int) (newHeight * getResources().getDisplayMetrics().density)));
            }
        });
    }

    public View.OnClickListener onClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            if (view == tambahTopik){
                Intent intent = new Intent(getApplicationContext(), TambahTopikActivity.class);
                intent.putExtra("ID_KELAS", id_kelas);
                intent.putExtra("ID_SESI", idSesi);
                intent.putExtra("ID_SUBSESI", idSubSesi);
                startActivityForResult(intent, 1);
            }
        }
    };

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(getApplicationContext(), TopikActivity.class);
        intent.putExtra("ID_KELAS", id_kelas);
        intent.putExtra("ID_SESI", idSesi);
        intent.putExtra("ID_SUBSESI", idSubSesi);
        intent.putExtra("ID_TOPIK", listTopik.get(i).getID_TOPIK());
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                SubSesiTopik subSesiTopik = new SubSesiTopik();
                subSesiTopik.setID_TOPIK( data.getStringExtra("id_topik"));
                subSesiTopik.setID_SESI(idSesi);
                subSesiTopik.setID_SUB_SESI(idSubSesi);
                subSesiTopik.setJUDUL_TOPIK(data.getStringExtra("judul_topik"));
                subSesiTopik.setDESKRIPSI_TOPIK(data.getStringExtra("desc_topik"));
                subSesiTopik.setNAMA_PENGGUNA(App.session.getUserDetails().getName());
                subSesiTopik.setDATE_CREATED(data.getStringExtra("date_created"));
                listTopik.add(0, subSesiTopik);
                topikAdapter.notifyDataSetChanged();
                Util.setListViewHeightBasedOnChildren(topikListView);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    } //onActivityResult
}

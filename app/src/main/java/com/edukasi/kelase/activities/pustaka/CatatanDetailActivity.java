package com.edukasi.kelase.activities.pustaka;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.BaseActivity;
import com.edukasi.kelase.entities.PustakaCatatan;
import com.edukasi.kelase.models.PustakaCatatanModel;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class CatatanDetailActivity extends BaseActivity {

    private String idPustaka;
    private PustakaCatatan pustakaCatatan;
    private EditText judul, konten;
    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            idPustaka = bundle.getString("id_pustaka");
            PustakaCatatanModel pustakaCatatanModel = new PustakaCatatanModel(this);
            pustakaCatatan = (PustakaCatatan) pustakaCatatanModel.find(idPustaka);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_pustaka_catatan);
        judul = (EditText) findViewById(R.id.judul);
        konten = (EditText) findViewById(R.id.konten);
        submit = (Button) findViewById(R.id.submit);

        if (pustakaCatatan != null){
            judul.setText(pustakaCatatan.getJUDUL_CATATAN());
            konten.setText(pustakaCatatan.getKONTEN_CATATAN());
        }
    }

    @Override
    public void setUICallbacks() {
        submit.setOnClickListener(onClickListener);
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == submit){

            }
        }
    };
}

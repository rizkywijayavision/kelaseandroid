package com.edukasi.kelase.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.controllers.SigninController;
import com.edukasi.kelase.controllers.SignupWithAccessCodeController;
import com.edukasi.kelase.controllers.SignupWithoutAccessCodeController;
import com.edukasi.kelase.entities.ProfileData;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ebizu-rizky on 11/19/15.
 */
public class SignupActivity extends BaseActivity implements OnClickListener {

    private List<String> arrayPeran, arrayProvinsi, arrayNegara, arrayInstitution;
    private ArrayAdapter<String> peranAdapter, institusiAdapter, negaraAdapter, provinsiAdapter;
    private Spinner peranSpiner, institusiSpiner, negaraSpiner, provinsiSpiner;
    private RadioGroup groupRadio;
    private LinearLayout radio_institutionText, radio_accesscodeText;
    private Button signupbtn;
    private EditText firstname, lastname, username, password, email, namalembaga, alamatlembaga, kodeakses;
    private ProgressDialog progressBar;
    private Animation animslidein, animslideout;
    private int typeSignup = 0;
    private SignupWithAccessCodeController signupWithAccessCodeController;
    private SignupWithoutAccessCodeController signupWithoutAccessCodeController;
    private SigninController signinController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this).registerReceiver(userBroadcast,
                new IntentFilter("user_broadcast"));
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_signup);

        //bounding view
        firstname = (EditText) findViewById(R.id.signupFirstname);
        lastname = (EditText) findViewById(R.id.signupLastname);
        username = (EditText) findViewById(R.id.signupUsername);
        password = (EditText) findViewById(R.id.signupPassword);
        email = (EditText) findViewById(R.id.signupEmail);
        namalembaga = (EditText) findViewById(R.id.signupInstitutionName);
        alamatlembaga = (EditText) findViewById(R.id.signupInstitutionAddr);
        kodeakses = (EditText) findViewById(R.id.signupAccesscode);
        signupbtn = (Button) findViewById(R.id.signup_signup_btn);
        peranSpiner = (Spinner) findViewById(R.id.signup_peran);
        institusiSpiner = (Spinner) findViewById(R.id.signup_institutionType);
        negaraSpiner = (Spinner) findViewById(R.id.signup_country);
        provinsiSpiner = (Spinner) findViewById(R.id.signup_province);
        groupRadio=(RadioGroup)findViewById(R.id.groupRadio);
        radio_institutionText=(LinearLayout)findViewById(R.id.radio_institutionText);
        radio_accesscodeText=(LinearLayout)findViewById(R.id.radio_accesscodeText);

        //setting animation
        animslidein = AnimationUtils.loadAnimation(this, R.anim.abc_slide_in_top);
        animslideout = AnimationUtils.loadAnimation(this, R.anim.abc_slide_out_top);

        //setting progress diaglog
        progressBar = new ProgressDialog(this);
        progressBar.setIndeterminate(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setTitle("Sedang Proses ...");
        progressBar.setMessage("Mohon tunggu ...");

        //setting spinner
        arrayPeran = Arrays.asList(getResources().getStringArray(R.array.peran));
        arrayProvinsi  = Arrays.asList(getResources().getStringArray(R.array.provinsi));
        arrayNegara = Arrays.asList(getResources().getStringArray(R.array.negara));
        arrayInstitution  = Arrays.asList(getResources().getStringArray(R.array.institution));
        // spinner peran kode akses
        peranAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,arrayPeran);
        peranSpiner.setAdapter(peranAdapter);
        // spinner jenis lembaga
        institusiAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item, arrayInstitution);
        institusiSpiner.setAdapter(institusiAdapter);
        // spinner negara
        negaraAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item, arrayNegara);
        negaraSpiner.setAdapter(negaraAdapter);
        //spinner provinsi
        provinsiAdapter =  new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item, arrayProvinsi);
        provinsiSpiner.setAdapter(provinsiAdapter);

    }

    @Override
    public void setUICallbacks() {
        groupRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.accesscodeRadio:
                        radio_institutionText.setVisibility(View.GONE);
                        radio_accesscodeText.setVisibility(View.VISIBLE);
                        typeSignup = 0;
                        break;
                    case R.id.institutionRadio:
                        radio_accesscodeText.setVisibility(View.GONE);
                        radio_institutionText.setVisibility(View.VISIBLE);
                        typeSignup = 1;
                        break;
                }
            }
        });
        signupbtn.setOnClickListener(SignupActivity.this);
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void onClick(View view) {
        if (view == signupbtn){
            if (typeSignup == 0){
                doSignupWithAccessCode();
            } else {
                doSignupWithoutAccesCode();
            }
        }
    }

    private void doSignupWithAccessCode(){
        if (firstname.length()>0 && lastname.length()>0 && username.length()>0 && password.length()>0 && email.length()>0 && kodeakses.length()>0 && peranSpiner.getSelectedItemPosition()!=0) {
            signupWithAccessCodeController = new SignupWithAccessCodeController(this, progressBar,
                    firstname.getText().toString(),
                    lastname.getText().toString(),
                    username.getText().toString(),
                    password.getText().toString(),
                    email.getText().toString(),
                    kodeakses.getText().toString(),
                    String.valueOf(peranSpiner.getSelectedItemPosition()));
            signupWithAccessCodeController.executeAPI();

        } else {
            Toast.makeText(getApplication(), "maaf anda harus mengisi semua form", Toast.LENGTH_LONG).show();
        }
    }
    private void doSignupWithoutAccesCode(){
        if (firstname.length()>0 && lastname.length()>0 && username.length()>0 && password.length()>0 && email.length()>0 && institusiSpiner.getSelectedItemPosition()>0 && namalembaga.length()>0 && alamatlembaga.length()>0 ) {
            signupWithoutAccessCodeController = new SignupWithoutAccessCodeController(this, progressBar,
                    firstname.getText().toString(),
                    lastname.getText().toString(),
                    username.getText().toString(),
                    password.getText().toString(),
                    email.getText().toString(),
                    String.valueOf(institusiSpiner.getSelectedItemPosition()),
                    namalembaga.getText().toString(),
                    alamatlembaga.getText().toString(),
                    String.valueOf(negaraSpiner.getSelectedItemPosition()),
                    String.valueOf(provinsiSpiner.getSelectedItemPosition()));
        } else {
            Toast.makeText(getApplication(), "maaf anda harus mengisi semua form", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(userBroadcast);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private BroadcastReceiver userBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ProfileData profile = new ProfileData();
            if(intent.getStringExtra("message").equals("with_code_access")){
                profile.setNama_depan(firstname.getText().toString());
                profile.setNama_belakang(lastname.getText().toString());
                App.session.saveProfileSession(profile);

            }
            if(intent.getStringExtra("message").equals("without_code_access")){
                profile.setNama_depan(firstname.getText().toString());
                profile.setNama_belakang(lastname.getText().toString());
                profile.setAlamat(alamatlembaga.getText().toString());
                profile.setNegara(String.valueOf(negaraSpiner.getSelectedItemPosition()));
                profile.setProvinsi(String.valueOf(provinsiSpiner.getSelectedItemPosition()));
                profile.setNama_negara(negaraSpiner.getSelectedItem().toString());
                profile.setNama_provinsi(provinsiSpiner.getSelectedItem().toString());
                App.session.saveProfileSession(profile);
            }

            signinController = new SigninController(getApplicationContext(), progressBar, username.getText().toString(), password.getText().toString());
            signinController.executeAPI();

            if(intent.getStringExtra("message").equals("login")){
                Intent i = new Intent(getApplicationContext(), SplashActivity.class);
                startActivity(i);
                finish();
            }

        }
    };

}

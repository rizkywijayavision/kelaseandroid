package com.edukasi.kelase.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.controllers.ChangeProfpictController;
import com.edukasi.kelase.controllers.EditProfileController;
import com.edukasi.kelase.entities.KelaseUtil;
import com.edukasi.kelase.entities.ProfileData;
import com.edukasi.kelase.helpers.PictureHelper;
import com.edukasi.kelase.sessions.KelaseSessionManager;
import com.edukasi.kelase.sessions.LoginSession;
import com.pkmmte.view.CircularImageView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import me.iwf.photopicker.PhotoPickerActivity;
import me.iwf.photopicker.utils.PhotoPickerIntent;

/**
 * Created by ebizu-rizky on 11/24/15.
 */

public class ProfileEditActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private TextView birthdate, profpict;
    private CircularImageView profilePicture;
    private EditText lahir, phone, alamat, name, username, peran;
    private Spinner negaraSpiner, provinsiSpiner;
    private List<String> arrayPeran, arrayNegara, arrayProvinsi;
    private ArrayAdapter<String> peranAdapter, negaraAdapter, provinsiAdapter;
    private ProgressDialog pd;
    private KelaseSessionManager session;
    private LoginSession loginSession;
    private PictureHelper pictureHelper;
    private ProfileData profileData;
    public final static int REQUEST_CODE = 1;
    private ArrayList<String> photos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        session = App.session;
        profileData = App.session.getProfileData();
        loginSession = App.session.getUserDetails();
        pictureHelper = PictureHelper.getInstance(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_edit_profile);
        pd = new ProgressDialog(this);
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setTitle("Sedang Proses ...");
        pd.setMessage("Mohon tunggu ...");
        //prof pict
        profilePicture = (CircularImageView) findViewById(R.id.editprofile_profpic);
        String imageProfpic = session.getProfileData().getProfile_image();
        String profpicApi = KelaseUtil.KELASE_SERVICE_IMAGE_URL;
        String imageProfileUri = profpicApi+imageProfpic;
        pictureHelper.displayImage(imageProfileUri, profilePicture);
        profpict = (TextView) findViewById(R.id.profpict);

        //name
        name = (EditText) findViewById(R.id.name);
        name.setText(session.getUserDetails().getName());

        //username
        username = (EditText) findViewById(R.id.username);
        username.setText(session.getUserDetails().getUsername());

        //setting spinner
        provinsiSpiner = (Spinner) findViewById(R.id.province);
        negaraSpiner = (Spinner) findViewById(R.id.country);
        arrayPeran = Arrays.asList(getResources().getStringArray(R.array.peran));
        arrayProvinsi  = Arrays.asList(getResources().getStringArray(R.array.provinsi));
        arrayNegara = Arrays.asList(getResources().getStringArray(R.array.negara));
        peranAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,arrayPeran);
        negaraAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item, arrayNegara);
        negaraSpiner.setAdapter(negaraAdapter);
        provinsiAdapter =  new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item, arrayProvinsi);
        provinsiSpiner.setAdapter(provinsiAdapter);

        //phone
        phone = (EditText) findViewById(R.id.phone);
        phone.setText(session.getProfileData().getTelepon());

        //date picker
        birthdate = (TextView) findViewById(R.id.birthdate);
        birthdate.setText(session.getProfileData().getTanggal_lahir());
        birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ProfileEditActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setThemeDark(true);
                dpd.vibrate(true);
                dpd.dismissOnPause(true);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });
    }

    @Override
    public void setUICallbacks() {
        profpict.setOnClickListener(onClickListener);

    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == profpict){
                PhotoPickerIntent intent = new PhotoPickerIntent(getApplicationContext());
                intent.setPhotoCount(1);
                intent.setShowCamera(true);
                intent.setShowGif(true);
                startActivityForResult(intent, REQUEST_CODE);
            }
        }
    };

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth+"/"+(++monthOfYear)+"/"+year;
        birthdate.setText(date.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate(R.menu.menu_post_status, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        if (id==android.R.id.home){
            Intent successIntent = new Intent(this, HomeActivity.class);
            startActivity(successIntent);
            finish();
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_post) {
            editProfile();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            if (data !=null) {
                photos = data.getStringArrayListExtra(PhotoPickerActivity.KEY_SELECTED_PHOTOS);
                final Bitmap thumbnail = (BitmapFactory.decodeFile(photos.get(0).toString()));
                ImageView photoMoment = (ImageView) findViewById(R.id.editprofile_profpic);
                photoMoment.setImageBitmap(thumbnail);
                File image = new File(photos.get(0).toString());
                pd.show();
                new ChangeProfpictController(context, session.getUserDetails().getId(), image, String.valueOf(thumbnail.getHeight()),  String.valueOf(thumbnail.getWidth())){
                    @Override
                    public void onAPIsuccess() {
                        super.onAPIsuccess();
                        pd.dismiss();
                        profileData.setProfile_image(sendImageParser.getResultSingle());
                        App.session.saveProfileSession(profileData);
                    }

                    @Override
                    public void onAPIFailed(String errorMessage) {
                        super.onAPIFailed(errorMessage);
                        pd.dismiss();
                    }
                }.executeAPI();
            }
        }
    }

    public void editProfile(){
        pd.show();
        new EditProfileController(context, session.getUserDetails().getId(), peran.getText().toString(),
                name.getText().toString(), alamat.getText().toString(), negaraSpiner.getSelectedItem().toString(),
                provinsiSpiner.getSelectedItem().toString(), phone.getText().toString(), lahir.getText().toString(), birthdate.getText().toString()){

            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                pd.dismiss();
                App.session.getUserDetails().setName(name.getText().toString());
                loginSession.setUsername(username.getText().toString());
                App.session.createLoginSession(loginSession);
                profileData.setAlamat(alamat.getText().toString());
                profileData.setTanggal_lahir(birthdate.getText().toString());
                profileData.setTempat_lahir(lahir.getText().toString());
                profileData.setTelepon(phone.getText().toString());
                profileData.setProvinsi(String.valueOf(provinsiSpiner.getSelectedItem().toString()));
                profileData.setNegara(String.valueOf(negaraSpiner.getSelectedItem().toString()));
                App.session.saveProfileSession(profileData);
                finish();
            }

            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                pd.setTitle("maaf koneksi gagal");
                pd.setMessage("mohon untuk mencoba beberapa saat lagi");
                pd.setCanceledOnTouchOutside(true);
                pd.show();
            }
        }.executeAPI();
    }
}
package com.edukasi.kelase.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.entities.KelaseUtil;
import com.edukasi.kelase.fragments.KelasFragment;
import com.edukasi.kelase.fragments.LinimasaFragment;
import com.edukasi.kelase.fragments.PustakaFragment;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.pkmmte.view.CircularImageView;

/**
 * Created by ebizu-rizky on 11/19/15.
 */

public class HomeActivity extends BaseActivity {

    protected Drawer drawer = null;
    protected View headerView;
    protected Toolbar toolbar;
    protected Fragment homeFragment;
    protected int curPositionDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        curPositionDrawer = 1;
        super.onCreate(savedInstanceState);
        // Check whether we're recreating a previously destroyed instance
        final YouTubeInitializationResult result = YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(this);

        if (result != YouTubeInitializationResult.SUCCESS) {
            //If there are any issues we can show an error dialog.
            result.getErrorDialog(this, 0).show();
        }
    }

    @Override
    protected void onPause() {
        App.log("curPositionDrawer : " + curPositionDrawer);
        App.session.saveStateDrawerHomeActivity(curPositionDrawer);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_base_with_drawer);
        LinimasaFragment linimasaFragment = new LinimasaFragment();
        homeFragment = linimasaFragment;
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, homeFragment).commit();
        setHeaderDrawer();
    }

    @Override
    public void showCustomActionBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }
        seetingNavigationDrawer();
    }

    @Override
    public void setUICallbacks() {
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen()){
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.launcher_share_menu) {
            Intent intent = new Intent(this,ProfileSettingActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setHeaderDrawer(){
        headerView = View.inflate(this, R.layout.header, null);
        CircularImageView profilePicture = (CircularImageView) headerView.findViewById(R.id.profile_header_image);
        String imageProfpic = App.session.getProfileData().getProfile_image();
        String profpicApi = KelaseUtil.KELASE_SERVICE_IMAGE_URL;
        String imageProfileUri = profpicApi+imageProfpic;

        com.edukasi.kelase.libs.imageloader.ImageLoader imgLoader = new com.edukasi.kelase.libs.imageloader.ImageLoader(this);
        imgLoader.DisplayImage(imageProfileUri, profilePicture);

        TextView namaProfile = (TextView) headerView.findViewById(R.id.profile_header_nama);
        TextView jobProfile = (TextView) headerView.findViewById(R.id.profil_header_job);
        TextView locationProfile = (TextView) headerView.findViewById(R.id.profile_header_location);
        namaProfile.setText(App.session.getProfileData().getNama_depan() +" "+ App.session.getProfileData().getNama_belakang());
        locationProfile.setText(App.session.getProfileData().getNama_provinsi()+" , "+App.session.getProfileData().getNama_negara());

        String role;
        int role_id = Integer.parseInt(App.session.getUserDetails().getRole_id());
        switch(role_id){

            case 2:
                role = "Orang Tua";
                break;
            case 3:
                role = "Peserta";
                break;
            case 4:
                role = "Guru";
                break;
            default:
                role = "Freelancer";
                break;
        }
        jobProfile.setText(role);
    }

    public void seetingNavigationDrawer(){
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withHeader(headerView)
                .withToolbar(toolbar)
                .withActionBarDrawerToggleAnimated(true)
                .withSliderBackgroundColorRes(R.color.kelase_blue)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_notification).withIcon(FontAwesome.Icon.faw_bell).withBadge("5").withIdentifier(0),
                        new PrimaryDrawerItem().withName(R.string.drawer_linimasa).withIcon(FontAwesome.Icon.faw_certificate).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.drawer_belajar).withIcon(FontAwesome.Icon.faw_book).withIdentifier(2),
//                        new PrimaryDrawerItem().withName(R.string.drawer_edukonten).withIcon(FontAwesome.Icon.faw_eye).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.drawer_pustaka).withIcon(FontAwesome.Icon.faw_folder).withIdentifier(3)
                )
                .withOnDrawerListener(drawerListener)
                .withOnDrawerItemClickListener(drawerItemClickListener)
                .withSelectedItem(curPositionDrawer)
                .build();
        drawer.setSelection(App.session.getDrawerStateHomeActivity());
    }

    private Drawer.OnDrawerListener drawerListener = new Drawer.OnDrawerListener() {
        @Override
        public void onDrawerOpened(View drawerView) {
        }

        @Override
        public void onDrawerClosed(View drawerView) {
        }

        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
        }
    };

    private Drawer.OnDrawerItemClickListener drawerItemClickListener =  new Drawer.OnDrawerItemClickListener() {
        @Override
        public boolean onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
            homeFragment = null;
            if (drawerItem != null) {
                if (drawerItem instanceof Nameable) {
                    switch (drawerItem.getIdentifier()) {
                        case 1:
                            if(curPositionDrawer != 1){
                                LinimasaFragment linimasaFragment = new LinimasaFragment();
                                homeFragment = linimasaFragment;
                                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, homeFragment).commit();
                                curPositionDrawer = 1;
                            }

                            break;
                        case 2:
                            if(curPositionDrawer != 2){
                                KelasFragment kelasFragment = new KelasFragment();
                                homeFragment = kelasFragment;
                                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, homeFragment).commit();
                                curPositionDrawer = 2;
                            }

                            break;
                        case 3:
                            if(curPositionDrawer != 3){
                                PustakaFragment pustakaFragment = new PustakaFragment();
                                homeFragment = pustakaFragment;
                                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, homeFragment).commit();
                                curPositionDrawer = 3;
                            }

                            break;
//                        case 4:
//                            homeFragment = new PustakaFragment();
//                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, homeFragment).commit();
//                            curPositionDrawer = 4;
//                            break;
                        default:
                            break;
                    }

                }

            }

            return false;
        }
    };

}

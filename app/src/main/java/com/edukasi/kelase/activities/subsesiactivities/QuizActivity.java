package com.edukasi.kelase.activities.subsesiactivities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.BaseInfoActivity;
import com.edukasi.kelase.entities.KelasSubSesi;
import com.edukasi.kelase.models.KelasSubSesiModel;

/**
 * Created by ebizu-rizky on 12/6/15.
 */
public class QuizActivity extends BaseInfoActivity {

    private WebView webView;
    private String id_kelas, idSesi, idSubSesi;
    private KelasSubSesi kelasSubSesi;
    private Button startQuizButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id_kelas = bundle.getString("ID_KELAS");
            idSesi = bundle.getString("ID_SESI");
            idSubSesi = bundle.getString("ID_SUBSESI");
        }
        KelasSubSesiModel kelasSubSesiModel = new KelasSubSesiModel(this);
        kelasSubSesi = (KelasSubSesi) kelasSubSesiModel.find(idSubSesi);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_quiz);
        webView = (WebView) findViewById(R.id.launcherWebView);
        if(kelasSubSesi != null){
            setTitle(kelasSubSesi.getJUDUL_SUB_SESI());
            webView.loadData(kelasSubSesi.getDESKRIPSI_SUB_SESI(),"text/html; charset=UTF-8", null);
        }
        startQuizButton = (Button) findViewById(R.id.start_quiz_button);
    }

    @Override
    public void setUICallbacks() {
        startQuizButton.setOnClickListener(onClickListener);
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    private View.OnClickListener onClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            if (view == startQuizButton){
                Intent i = new Intent(getApplicationContext(), StartQuizActivity.class);
                i.putExtra("ID_KELAS", id_kelas);
                i.putExtra("ID_SESI", idSesi);
                i.putExtra("ID_SUBSESI", idSubSesi);
                startActivity(i);
            }
        }
    };
}

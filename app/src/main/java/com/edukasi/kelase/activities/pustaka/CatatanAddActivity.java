package com.edukasi.kelase.activities.pustaka;

import android.app.ProgressDialog;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.BaseActivity;
import com.edukasi.kelase.activities.HomeActivity;
import com.edukasi.kelase.controllers.PustakaAddCatatanController;

/**
 * Created by ebizu-rizky on 12/13/15.
 */

public class CatatanAddActivity extends BaseActivity {

    private EditText judul, konten;
    private Button submit;
    private ProgressDialog progress;

    @Override
    public void initView() {
        setContentView(R.layout.activity_pustaka_catatan);
        judul = (EditText) findViewById(R.id.judul);
        konten = (EditText) findViewById(R.id.konten);
        submit = (Button) findViewById(R.id.submit);
        progress = new ProgressDialog(this);
        progress.setTitle("Submit Catatan");
        progress.setMessage("Mohon Tunggu Sebentar...");
    }

    @Override
    public void setUICallbacks() {
        submit.setOnClickListener(onClickListener);
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == submit){
                addCatatan();
            }
        }
    };

    public void addCatatan(){
        String judulText = judul.getText().toString();
        String kontenText = konten.getText().toString();
        progress.show();
        if (!judulText.equals("")  && !kontenText.equals("")){
            new PustakaAddCatatanController(context, App.session.getUserDetails().getId(), judulText, kontenText){
                @Override
                public void onAPIsuccess() {
                    super.onAPIsuccess();
                    progress.dismiss();
                    Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(i);
                    finish();
                }

                @Override
                public void onAPIFailed(String errorMessage) {
                    super.onAPIFailed(errorMessage);
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                }
            }.executeAPI();
        } else {
            Toast.makeText(this, "Judul & Konten tidak boleh kosong", Toast.LENGTH_SHORT).show();
        }
    }
}

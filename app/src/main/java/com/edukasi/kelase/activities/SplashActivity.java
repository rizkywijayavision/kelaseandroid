package com.edukasi.kelase.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.services.SyncronizeDataService;
import com.edukasi.kelase.sessions.KelaseSessionManager;

import java.util.HashMap;

/**
 * Created by ebizu-rizky on 11/4/15.
 */

public class SplashActivity extends AppCompatActivity{

    private ProgressDialog progress;
    private Button loginButton, signUpButton;
    private Intent fetchDataIntent;
    private boolean isLogin;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_READ_EXTERNAL_STORAGE = 1;
    private HashMap<Integer, Boolean> listPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.session = new KelaseSessionManager(this);
        setContentView(R.layout.activity_splash);
        listPermission = new HashMap<>();
        loginButton = (Button) findViewById(R.id.buttonLogin);
        signUpButton = (Button) findViewById(R.id.buttonSignup);
        isLogin = App.session.checkLogin();
        if (isLogin){
            loginButton.setVisibility(View.GONE);
            signUpButton.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (App.session.firstOpen()) {
                        download();
                    } else {
                        finishActivty();
                    }
                }
            }, 1000);
        }
    }

    public void LoginActivity(View view){
        Intent intent = new Intent(this, SigninActivity.class);
        startActivity(intent);
        finish();
    }

    public void SignupActivity(View view){
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
        finish();
    }
//    int i;


    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fetchDataBroadcastReciever);
    }

    public void download(){
        progress = new ProgressDialog(this);
        progress.setMessage("Downloading Data");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.setButton(ProgressDialog.BUTTON_NEGATIVE, "Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //button click stuff here
                download();
            }
        });
        progress.setButton(ProgressDialog.BUTTON_POSITIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //button click stuff here
                finish();
            }
        });
        progress.show();
        progress.getButton(ProgressDialog.BUTTON_NEGATIVE).setVisibility(View.INVISIBLE);

        if (isNetworkAvailable(this)) {
            // code here
            fetchDataIntent = new Intent(this, SyncronizeDataService.class);
            this.startService(fetchDataIntent);
            LocalBroadcastManager.getInstance(this).registerReceiver(fetchDataBroadcastReciever,
                    new IntentFilter("fetch_data_broadcast"));
        } else {
            progress.getButton(ProgressDialog.BUTTON_NEGATIVE).setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Tidak ada koneksi Internet", Toast.LENGTH_SHORT).show();
        }

    }

    public boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private BroadcastReceiver fetchDataBroadcastReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("status").equals("success")){
                progress.dismiss();
                App.session.setFirstOpen();
                finishActivty();
            } else if (intent.getStringExtra("status").equals("failed")){
                progress.getButton(ProgressDialog.BUTTON_NEGATIVE).setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Failed to Fetch Data", Toast.LENGTH_SHORT).show();
            }
            getApplicationContext().stopService(fetchDataIntent);
        }
    };

    private void finishActivty(){
        if (Build.VERSION.SDK_INT < 23) {
            Intent i = new Intent(getApplicationContext(), HomeActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            // Staring Login Activity
            getApplicationContext().startActivity(i);
            finish();
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                listPermission.put(REQUEST_CAMERA, true);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                listPermission.put(REQUEST_READ_EXTERNAL_STORAGE, true);
            }
            checkAllPermission();
        }
    }

    private void checkAllPermission(){
        if(listPermission.size() < 2){
            if(!listPermission.containsKey(REQUEST_CAMERA)){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA);
            } else if(!listPermission.containsKey(REQUEST_READ_EXTERNAL_STORAGE)){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_READ_EXTERNAL_STORAGE);
            }
        } else {
            if(listPermission.get(REQUEST_CAMERA) && listPermission.get(REQUEST_READ_EXTERNAL_STORAGE)){
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                // Closing all the Activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                // Staring Login Activity
                getApplicationContext().startActivity(i);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Permission denied...",Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                     finish();
                    }
                }, 2000);
            }
        }
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CAMERA) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            App.log("Received response for Camera permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed
                App.log("CAMERA permission has now been granted. Showing preview.");
                listPermission.put(REQUEST_CAMERA, true);
            } else {
                App.log("CAMERA permission was NOT granted.");
                listPermission.put(REQUEST_CAMERA, false);
            }
            // END_INCLUDE(permission_result)

        } else if (requestCode == REQUEST_READ_EXTERNAL_STORAGE) {
            App.log("Received response for contact permissions request.");

            // We have requested multiple permissions for contacts, so all of them need to be
            // checked.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                listPermission.put(REQUEST_READ_EXTERNAL_STORAGE, true);
                // All required permissions have been granted, display contacts fragment.
            } else {
                App.log("Contacts permissions were NOT granted.");
                listPermission.put(REQUEST_READ_EXTERNAL_STORAGE, false);
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        checkAllPermission();
    }
}

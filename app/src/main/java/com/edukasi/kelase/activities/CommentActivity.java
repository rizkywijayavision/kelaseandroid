package com.edukasi.kelase.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.adapters.KomentarAdapter;
import com.edukasi.kelase.controllers.KomentarListController;
import com.edukasi.kelase.controllers.KomentarSubmitController;
import com.edukasi.kelase.entities.KomentarListitem;
import com.edukasi.kelase.entities.ProfileData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ebizu-rizky on 11/20/15.
 */
public class CommentActivity extends BaseActivity {

    private String ID_STATUS, ID_USER, ID_PEMILIK_STATUS;
    private ProgressBar commentProgressbar;
    private ListView listComment;
    private KomentarAdapter komentarAdapter;
    private List<KomentarListitem> komentarList;
    private Button buttonComment;
    private EditText editText_comment;
    private String komentar;
    private ProfileData profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();;
        if (extras != null){
            ID_STATUS = extras.getString("ID_STATUS_PENGGUNA");
            ID_USER = extras.getString("ID_PENGGUNA");
            ID_PEMILIK_STATUS = extras.getString("ID_PEMILIK_STATUS");
        }
        profile = App.session.getProfileData();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_comment);
        commentProgressbar = (ProgressBar) findViewById(R.id.comment_progressbar);
        commentProgressbar.setVisibility(View.VISIBLE);
        listComment = (ListView) findViewById(R.id.list_komentar);
        komentarList = new ArrayList<>();
        komentarAdapter = new KomentarAdapter(this);
        komentarAdapter.setData(komentarList);

        buttonComment = (Button) findViewById(R.id.comment_button);
        editText_comment = (EditText) findViewById(R.id.editText_comment);
        editText_comment.setFocusable(true);
    }

    @Override
    public void setUICallbacks() {
        buttonComment.setOnClickListener(submitCommentListener);
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {
        listComment.setAdapter(komentarAdapter);
        komentarAdapter.setData(komentarList);
        getListComment();
    }

    public void getListComment(){

        new KomentarListController(this, ID_STATUS, ID_USER){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                komentarList.addAll(komentarParser.getResult());
                komentarAdapter.notifyDataSetChanged();
                commentProgressbar.setVisibility(View.GONE);
            }

            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                commentProgressbar.setVisibility(View.GONE);
            }
        }.executeAPI();

    }

    public View.OnClickListener submitCommentListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            komentar =  editText_comment.getText().toString();
            editText_comment.setFocusable(false);
            if(komentar != null || komentar.equals("")){
                commentProgressbar.setVisibility(View.VISIBLE);
                new KomentarSubmitController(context, ID_STATUS, ID_USER, ID_PEMILIK_STATUS, komentar){

                    @Override
                    public void onAPIsuccess() {
                        super.onAPIsuccess();
                        commentProgressbar.setVisibility(View.GONE);
                        Date now = new Date();
                        editText_comment.setText("");
                        editText_comment.setFocusableInTouchMode(true);
                        KomentarListitem itembaru = new KomentarListitem();
                        itembaru.setNAMA_DEPAN(profile.getNama_depan());
                        itembaru.setNAMA_BELAKANG(profile.getNama_belakang());
                        itembaru.setDATE_CREATED(now.toString());
                        itembaru.setSTATUS(komentar);
                        itembaru.setIMAGE(profile.getProfile_image());
                        komentarList.add(itembaru);
                        komentarAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onAPIFailed(String errorMessage) {
                        super.onAPIFailed(errorMessage);
                        commentProgressbar.setVisibility(View.GONE);
                    }
                }.executeAPI();
            }

        }
    };
}

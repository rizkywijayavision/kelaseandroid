package com.edukasi.kelase.activities.subsesiactivities;

import android.os.Bundle;
import android.webkit.WebView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.BaseInfoActivity;
import com.edukasi.kelase.entities.KelasSubSesi;
import com.edukasi.kelase.models.KelasSubSesiModel;

/**
 * Created by ebizu-rizky on 11/30/15.
 */

public class BacaanActivity extends BaseInfoActivity {

    private WebView webView;
    private String id_kelas, idSesi, idSubSesi;
    private KelasSubSesi kelasSubSesi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id_kelas = bundle.getString("ID_KELAS");
            idSesi = bundle.getString("ID_SESI");
            idSubSesi = bundle.getString("ID_SUBSESI");
        }
        KelasSubSesiModel kelasSubSesiModel = new KelasSubSesiModel(this);
        kelasSubSesi = (KelasSubSesi) kelasSubSesiModel.find(idSubSesi);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.fragment_webview);
        webView = (WebView) findViewById(R.id.launcherWebView);
        if(kelasSubSesi != null){
            setTitle(kelasSubSesi.getJUDUL_SUB_SESI());
            webView.loadData(kelasSubSesi.getDESKRIPSI_SUB_SESI(),"text/html; charset=UTF-8", null);
        }
    }

    @Override
    public void setUICallbacks() {
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }
}

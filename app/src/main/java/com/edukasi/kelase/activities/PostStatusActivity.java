package com.edukasi.kelase.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.controllers.PostStatusController;

/**
 * Created by ebizu-rizky on 11/20/15.
 */
public class PostStatusActivity extends BaseActivity {

    private EditText statusMsg;
    private ProgressDialog progressDialog;
    private PostStatusController postStatusController;

    @Override
    public void initView() {
        setContentView(R.layout.activity_post_status);
        statusMsg = (EditText) findViewById(R.id.postmoment_momentstatus_txtv);
        statusMsg.requestFocus();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Sedang Proses ...");
        progressDialog.setMessage("Upload ...");
    }

    @Override
    public void setUICallbacks() {
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                postStatusController.cancellRequest();
            }
        });
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate(R.menu.menu_post_status, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        if (id==android.R.id.home){
            Intent successIntent = new Intent(this, HomeActivity.class);
            startActivity(successIntent);
            finish();
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_post) {
            postStatus();

        }

        return super.onOptionsItemSelected(item);
    }

    public void postStatus(){
        String statusContent = statusMsg.getText().toString();
        if(statusContent != null || statusContent.equals("")){
            progressDialog.show();
            postStatusController = new PostStatusController(context, App.session.getUserDetails().getId(), statusContent){
                @Override
                public void onAPIsuccess() {
                    super.onAPIsuccess();
                    progressDialog.dismiss();
                    Intent successIntent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(successIntent);
                    finish();
                }

                @Override
                public void onAPIFailed(String errorMessage) {
                    super.onAPIFailed(errorMessage);
                    progressDialog.dismiss();
                }
            };
            postStatusController.executeAPI();
        }
    }


}

package com.edukasi.kelase.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.edukasi.kelase.R;

/**
 * Created by ebizu-rizky on 11/24/15.
 */
public class ProfileChangePasswordActivity extends BaseActivity {

    private ProgressDialog pd;
    private EditText old, baru, confirm;

    @Override
    public void initView() {
        setContentView(R.layout.activity_setting_password);
        pd = new ProgressDialog(this);
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setTitle("mengubah password");
        pd.setMessage("Mohon tunggu ...");
        old = (EditText) findViewById(R.id.oldpassword);
        baru = (EditText) findViewById(R.id.newpassword);
        confirm = (EditText) findViewById(R.id.confirmpassword);
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate(R.menu.menu_post_status, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        if (id==android.R.id.home){
            Intent successIntent = new Intent(this, HomeActivity.class);
            startActivity(successIntent);
            finish();
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_post) {
            changePassword();

        }

        return super.onOptionsItemSelected(item);
    }

    public void changePassword(){

    }
}

package com.edukasi.kelase.activities.subsesiactivities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.BaseInfoActivity;
import com.edukasi.kelase.controllers.StartQuizController;
import com.edukasi.kelase.controllers.SubmitQuizController;
import com.edukasi.kelase.entities.SoalQuiz;
import com.edukasi.kelase.entities.SoalQuizEssay;
import com.edukasi.kelase.entities.SoalQuizPG;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ebizu-rizky on 12/6/15.
 */
public class StartQuizActivity extends BaseInfoActivity {

    private String id_kelas, idSesi, idSubSesi;
    private ProgressDialog progress;
    private SoalQuiz soalQuiz;
    private List<RadioGroup> listJawabanPG;
    private List<EditText> listJawabanEssay;
    private Button submitQuizButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id_kelas = bundle.getString("ID_KELAS");
            idSesi = bundle.getString("ID_SESI");
            idSubSesi = bundle.getString("ID_SUBSESI");
            App.log("id_kelas : "+id_kelas);
            App.log("idSesi : "+idSesi);
            App.log("idSubSesi : "+idSubSesi);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_start_quiz);
        setTitle("Contoh Quiz");
        progress = ProgressDialog.show(this, "Loading Data",
                "Mohon tunggu sebentar...", true);
        submitQuizButton = (Button) findViewById(R.id.submit_quiz);
        submitQuizButton.setOnClickListener(onClickListener);
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {
        new StartQuizController(context, id_kelas, idSesi, idSubSesi, App.session.getUserDetails().getId(), App.session.getUserDetails().getRole_id()){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                progress.dismiss();
                soalQuiz = startQuizParser.getResultSingle();
                initSoalQuiz();
            }

            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                progress.dismiss();
                Toast.makeText(context, "Tidak Bisa Mengikuti Quiz", Toast.LENGTH_LONG).show();
                finish();
            }
        }.executeAPI();
    }

    public void onRadioButtonClicked(View v){
        // Your code on click
    }

    public void initSoalQuiz(){
        listJawabanPG = new ArrayList<>();
        listJawabanEssay = new ArrayList<>();
        LinearLayout listSoalPG = (LinearLayout) findViewById(R.id.list_soal_pg);
        for (SoalQuizPG soalQuizPG : soalQuiz.getSoalQuizPGList()){
            View view = getLayoutInflater().inflate(R.layout.adapter_soal_quiz_pg, null);
            HtmlTextView judulSoal = (HtmlTextView) view.findViewById(R.id.judul_soal);
            judulSoal.setHtmlFromString(soalQuizPG.getSOAL(), new HtmlTextView.RemoteImageGetter());

            RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.soal_pg_radiogroup);
            RadioButton OPSI_A = new RadioButton(context);
            OPSI_A.setText(soalQuizPG.getOPSI_A());
            radioGroup.addView(OPSI_A);
            RadioButton OPSI_B = new RadioButton(context);
            OPSI_B.setText(soalQuizPG.getOPSI_B());
            radioGroup.addView(OPSI_B);
            RadioButton OPSI_C = new RadioButton(context);
            OPSI_C.setText(soalQuizPG.getOPSI_C());
            radioGroup.addView(OPSI_C);
            RadioButton OPSI_D = new RadioButton(context);
            OPSI_D.setText(soalQuizPG.getOPSI_D());
            radioGroup.addView(OPSI_D);

            listJawabanPG.add(radioGroup);
            listSoalPG.addView(view);
        }

        LinearLayout listSoalEssay = (LinearLayout) findViewById(R.id.list_soal_essay);

        for (SoalQuizEssay soalQuizEssay : soalQuiz.getSoalQuizEssayList()){
            View view = getLayoutInflater().inflate(R.layout.adapter_soal_quiz_essay, null);
            HtmlTextView judulSoal = (HtmlTextView) view.findViewById(R.id.judul_soal);
            judulSoal.setHtmlFromString(soalQuizEssay.getSOAL(), new HtmlTextView.RemoteImageGetter());
            EditText jawabanEssay = (EditText) view.findViewById(R.id.edittext_soal_essay);

            listJawabanEssay.add(jawabanEssay);
            listSoalEssay.addView(view);
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        int durasi = 0;
        try {
            Date finishDate = format.parse(soalQuiz.getTANGGAL_EXPIRED_QUIZ());
            durasi = (int) (finishDate.getTime() - System.currentTimeMillis());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        new CountDownTimer(durasi, 1000) {

            public void onTick(long millisUntilFinished) {
                int minute = (int) millisUntilFinished / 60000;
                int sec = (int) millisUntilFinished % 60000;

                submitQuizButton.setText("Submit Jawaban (" + minute + " : " + sec / 1000 + ")" );
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                submitQuiz();
            }

        }.start();
    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == submitQuizButton){
                new AlertDialog.Builder(context)
                        .setTitle("Submit Quiz")
                        .setMessage("Anda yakin telah selesai mengerjakan quiz ?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                submitQuiz();
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }
    };

    public void submitQuiz(){
        try {
            progress =  ProgressDialog.show(this, "Submit Quiz",
                    "Mohon tunggu sebentar...", true);
            JSONObject jsonJawaban = new JSONObject();
            JSONArray arrayJawaban = new JSONArray();
            int i = 0;
            for (RadioGroup jawabanPG : listJawabanPG){
                JSONObject jawabanQuiz = new JSONObject();
                jawabanQuiz.put("id_soal", soalQuiz.getSoalQuizPGList().get(i).getID_SOAL());
                jawabanQuiz.put("tipe", "PG");
                switch (jawabanPG.indexOfChild(findViewById(jawabanPG.getCheckedRadioButtonId()))){
                    case 0 :
                        jawabanQuiz.put("jawaban","A");
                        break;
                    case 1 :
                        jawabanQuiz.put("jawaban", "B");
                        break;
                    case 2 :
                        jawabanQuiz.put("jawaban", "C");
                        break;
                    case 3 :
                        jawabanQuiz.put("jawaban", "D");
                        break;
                    default:
                        jawabanQuiz.put("jawaban", "");
                        break;
                }
                arrayJawaban.put(jawabanQuiz);
                i++;
            }
            i = 0;
            for (EditText jawabanEssay : listJawabanEssay){
                JSONObject jawabanQuiz = new JSONObject();
                jawabanQuiz.put("id_soal", soalQuiz.getSoalQuizEssayList().get(i).getID_SOAL());
                jawabanQuiz.put("tipe", "ESSAY");
                jawabanQuiz.put("jawaban", jawabanEssay.getText().toString());
                arrayJawaban.put(jawabanQuiz);
                i++;
            }
            jsonJawaban.put("jawaban_soal", arrayJawaban);

            App.log(arrayJawaban.toString());
            new SubmitQuizController(context, id_kelas, idSesi, idSubSesi, App.session.getUserDetails().getId(), App.session.getUserDetails().getRole_id(), arrayJawaban){
                @Override
                public void onAPIsuccess() {
                    super.onAPIsuccess();
                    progress.dismiss();
                    Toast.makeText(context, "Quiz Berhasil Diupload ke Server", Toast.LENGTH_SHORT).show();
                    finish();
                }

                @Override
                public void onAPIFailed(String errorMessage) {
                    super.onAPIFailed(errorMessage);
                    progress.dismiss();
                    Toast.makeText(context, "Gagal Upload Data Ke Server, Silahkan Ulangi Lagi", Toast.LENGTH_LONG).show();
                }
            }.executeAPI();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}

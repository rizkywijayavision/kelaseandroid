package com.edukasi.kelase.activities.subsesiactivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.BaseInfoActivity;
import com.edukasi.kelase.controllers.TambahTopikController;

/**
 * Created by ebizu-rizky on 12/5/15.
 */

public class TambahTopikActivity extends BaseInfoActivity {

    private String id_kelas, idSesi, idSubSesi, judulTopik, descTopik;
    private Button submitTopik;
    private EditText judulTopikET, descTopikET;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id_kelas = bundle.getString("ID_KELAS");
            idSesi = bundle.getString("ID_SESI");
            idSubSesi = bundle.getString("ID_SUBSESI");
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_tambah_topik);
        setTitle("Tambah Topik Baru");
        submitTopik = (Button) findViewById(R.id.submit_topik);
        judulTopikET = (EditText) findViewById(R.id.judul_topik);
        descTopikET = (EditText) findViewById(R.id.desc_topik);
        progress = new ProgressDialog(this);
        progress.setTitle("Submit Topik");
        progress.setMessage("Mohon Tunggu Sebentar...");
    }

    @Override
    public void setUICallbacks() {
        submitTopik.setOnClickListener(onClickListener);
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    private View.OnClickListener onClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            if (view == submitTopik){
                judulTopik = judulTopikET.getText().toString();
                descTopik = descTopikET.getText().toString();
                if(!judulTopik.equals("") && !descTopik.equals("")){
                    progress.show();
                    new TambahTopikController(getApplicationContext(),
                            App.session.getUserDetails().getId(),
                            App.session.getUserDetails().getRole_id(),
                            id_kelas, idSesi, idSubSesi, judulTopik, descTopik){

                        @Override
                        public void onAPIsuccess() {
                            super.onAPIsuccess();
                            progress.dismiss();
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("id_topik", idTopik);
                            returnIntent.putExtra("date_created", dateCreated);
                            returnIntent.putExtra("judul_topik", judulTopik);
                            returnIntent.putExtra("desc_topik", descTopik);
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }

                        @Override
                        public void onAPIFailed(String errorMessage) {
                            super.onAPIFailed(errorMessage);
                            progress.dismiss();
                        }
                    }.executeAPI();
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }
}

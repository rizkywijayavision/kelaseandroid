package com.edukasi.kelase.activities.pustaka;

import android.os.Bundle;

import com.edukasi.kelase.activities.BaseActivity;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class VideoDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {

    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }
}

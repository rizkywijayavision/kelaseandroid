package com.edukasi.kelase.activities;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.controllers.SigninController;
import com.edukasi.kelase.controllers.UserProfileController;

/**
 * Created by ebizu-rizky on 11/4/15.
 */

public class SigninActivity extends BaseActivity implements OnClickListener{

    private Button loginbtn;
    private EditText username, password;
    private ProgressDialog progressBar;
    private SigninController signinController;
    private UserProfileController userProfileController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this).registerReceiver(userBroadcast,
                new IntentFilter("user_broadcast"));
    }



    @Override
    public void initView() {
        setContentView(R.layout.activity_signin);
        loginbtn = (Button) findViewById(R.id.button_signin);
        username = (EditText) findViewById(R.id.loginUsername);
        password = (EditText) findViewById(R.id.loginPassword);
        progressBar = new ProgressDialog(this);
        progressBar.setIndeterminate(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setTitle("Sedang Proses ...");
        progressBar.setMessage("Log in ...");
    }

    @Override
    public void setUICallbacks() {
        loginbtn.setOnClickListener(this);
    }

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_signin :
                doLogin();
                break;
        }

    }

    public void doLogin(){
        if (username.length()>0 && password.length()>0) {
            signinController = new SigninController(this, progressBar, username.getText().toString(), password.getText().toString());
            signinController.executeAPI();
        }
        else {
            Toast.makeText(getApplication(), "maaf anda harus mengisi semua form", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onBackPressed() {
        if(progressBar.isShowing()){
            signinController.cancellRequest();
            userProfileController.cancellRequest();
        } else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(userBroadcast);
        super.onDestroy();
    }

    private BroadcastReceiver userBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("message").equals("login")){
                userProfileController = new UserProfileController(getApplicationContext(), progressBar,App.session.getUserDetails().getId(), App.session.getUserDetails().getRole_id());
                userProfileController.executeAPI();
            } else if(intent.getStringExtra("message").equals("get_profile")){
                Intent i = new Intent(getApplicationContext(), SplashActivity.class);
                startActivity(i);
                finish();
            }

        }
    };
}

package com.edukasi.kelase.activities.pustaka;

import android.app.ProgressDialog;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.BaseActivity;
import com.edukasi.kelase.activities.HomeActivity;
import com.edukasi.kelase.controllers.PustakaTautanAddController;

/**
 * Created by ebizu-rizky on 1/25/16.
 */
public class TautanAddActivity extends BaseActivity {

    private EditText link;
    private Button submit;
    private ProgressDialog progress;

    @Override
    public void initView() {
        setContentView(R.layout.activity_pustaka_tautan_add);
        link = (EditText) findViewById(R.id.link);
        submit = (Button) findViewById(R.id.submit);
        progress = new ProgressDialog(this);
        progress.setTitle("Submit Tautan");
        progress.setMessage("Mohon Tunggu Sebentar...");
    }

    @Override
    public void setUICallbacks() {
        submit.setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == submit){
                addTautan();
            }
        }
    };

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {

    }

    public void addTautan(){
        String link = this.link.getText().toString();
        progress.show();
        if (!link.equals("")){
            if (!link.toLowerCase().contains("http://")) link = "http://" + link;
            new PustakaTautanAddController(context, link){
                @Override
                public void onAPIsuccess() {
                    super.onAPIsuccess();
                    progress.dismiss();
                    Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(i);
                    finish();
                }

                @Override
                public void onAPIFailed(String errorMessage) {
                    super.onAPIFailed(errorMessage);
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                }
            }.executeAPI();
        } else {
            Toast.makeText(this, "Tautan tidak boleh kosong", Toast.LENGTH_SHORT).show();
        }
    }
}

package com.edukasi.kelase.activities.subsesiactivities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.Util;
import com.edukasi.kelase.activities.BaseInfoActivity;
import com.edukasi.kelase.adapters.TopikKomentarAdapter;
import com.edukasi.kelase.controllers.TampahKomentarTopikController;
import com.edukasi.kelase.controllers.TopikDetailController;
import com.edukasi.kelase.entities.KelaseUtil;
import com.edukasi.kelase.entities.TopikKomentar;
import com.edukasi.kelase.helpers.PictureHelper;
import com.pkmmte.view.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class TopikActivity extends BaseInfoActivity {

    private WebView webView;
    private String id_kelas, idSesi, idSubSesi, idtopik;
    private ListView komentarListView;
    private TopikKomentarAdapter komentarAdapter;
    private List<TopikKomentar> listKomentar;
    private ProgressDialog progress;
    private CircularImageView profPict;
    private TextView namaUser, komentarUser, dateCreated;
    private Button commentButton;
    private EditText textComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            id_kelas = bundle.getString("ID_KELAS");
            idSesi = bundle.getString("ID_SESI");
            idSubSesi = bundle.getString("ID_SUBSESI");
            idtopik = bundle.getString("ID_TOPIK");
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_topik);
        setTitle("Loading....");
        profPict = (CircularImageView) findViewById(R.id.komentaritem_propic);
        namaUser = (TextView) findViewById(R.id.komentaritem_namalbl);
        komentarUser = (TextView) findViewById(R.id.komentaritem_komentarlbl);
        dateCreated = (TextView) findViewById(R.id.komentaritem_timestamplbl);
        commentButton = (Button) findViewById(R.id.comment_button);
        textComment = (EditText) findViewById(R.id.editText_comment);
        webView = (WebView) findViewById(R.id.launcherWebView);
        progress = ProgressDialog.show(this, "Loading Data",
                "Mohon tunggu sebentar...", true);

        komentarListView = (ListView) findViewById(R.id.topik_list_view);
        komentarAdapter = new TopikKomentarAdapter(this);
        listKomentar = new ArrayList<>();
        komentarAdapter.setData(listKomentar);
        komentarListView.setAdapter(komentarAdapter);
    }

    @Override
    public void setUICallbacks() {
        commentButton.setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == commentButton){
                final String comment = textComment.getText().toString();
                if (!comment.equals("") || comment !=null){
                    progress = ProgressDialog.show(context, "Submit Comment",
                            "Mohon tunggu sebentar....", true);
                    new TampahKomentarTopikController(context, id_kelas, idSesi, idSubSesi, App.session.getUserDetails().getId(),
                            App.session.getUserDetails().getRole_id(), idtopik, comment){
                        @Override
                        public void onAPIsuccess() {
                            super.onAPIsuccess();
                            TopikKomentar topikKomentar = new TopikKomentar();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String currentDateandTime = sdf.format(new Date());
                            topikKomentar.setDATE_CREATED(currentDateandTime);
                            topikKomentar.setID_PENGGUNA(App.session.getUserDetails().getId());
                            topikKomentar.setNAMA_PENGGUNA(App.session.getUserDetails().getName());
                            topikKomentar.setIMAGE_PENGGUNA(App.session.getProfileData().getProfile_image());
                            topikKomentar.setKOMENTAR(comment);
                            listKomentar.add(0, topikKomentar);
                            komentarAdapter.notifyDataSetChanged();
                            textComment.setText("");
                            Util.setListViewHeightBasedOnChildren(komentarListView);
                            progress.dismiss();
                        }

                        @Override
                        public void onAPIFailed(String errorMessage) {
                            super.onAPIFailed(errorMessage);
                            progress.dismiss();
                        }
                    }.executeAPI();
                } else {
                    Toast.makeText(context, "Mohon isi komentar", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    public int getLayout() {
        return 0;
    }

    @Override
    public void updateUI() {
        new TopikDetailController(this, id_kelas, idSesi, idSubSesi, App.session.getUserDetails().getId(), App.session.getUserDetails().getRole_id(), idtopik){

            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                progress.dismiss();
                setTitle(topikKomentarParser.getResultSingle().getJUDUL_TOPIK());
                namaUser.setText(topikKomentarParser.getResultSingle().getNAMA_PENGGUNA());
                komentarUser.setText(topikKomentarParser.getResultSingle().getJUDUL_TOPIK());
                PictureHelper.getInstance(context).displayImage(KelaseUtil.KELASE_SERVICE_IMAGE_URL + topikKomentarParser.getResultSingle().getIMAGE_PENGGUNA(), profPict);
                dateCreated.setText(Util.prettyDate(topikKomentarParser.getResultSingle().getDATE_CREATED()));
                webView.loadData(topikKomentarParser.getResultSingle().getDESKRIPSI_TOPIK(), "text/html; charset=UTF-8", null);
                listKomentar.addAll(topikKomentarParser.getResultSingle().getLIST_KOMENTAR());
                komentarAdapter.notifyDataSetChanged();
                Util.setListViewHeightBasedOnChildren(komentarListView);
                setupWebView();
            }

            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                progress.dismiss();
            }
        }.executeAPI();
    }

    private void setupWebView() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                webView.loadUrl("javascript:MyApp.resize(document.body.getBoundingClientRect().height)");
                super.onPageFinished(view, url);
            }
        });
        webView.addJavascriptInterface(this, "MyApp");
    }

    @JavascriptInterface
    public void resize(final float height) {
        TopikActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                float newHeight = height + 10f;
                webView.setLayoutParams(new LinearLayout.LayoutParams(getResources().getDisplayMetrics().widthPixels, (int) (newHeight * getResources().getDisplayMetrics().density)));
            }
        });
    }
}

package com.edukasi.kelase.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.edukasi.kelase.App;
import com.edukasi.kelase.controllers.DetailKelasController;
import com.edukasi.kelase.controllers.GetListKelasController;
import com.edukasi.kelase.controllers.PustakaCatatanController;
import com.edukasi.kelase.controllers.PustakaPhotoController;
import com.edukasi.kelase.controllers.PustakaTautanController;
import com.edukasi.kelase.controllers.PustakaVideoController;
import com.edukasi.kelase.entities.Kelas;
import com.edukasi.kelase.models.KelasModel;

import java.util.HashMap;

/**
 * Created by ebizu-rizky on 12/12/15.
 */

public class SyncronizeDataService extends Service {

    Context context;
    HashMap<String, Boolean> listSuccessAPI;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private int sizeKelasku;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        listSuccessAPI = new HashMap<>();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getDataKelasPrivate();
        getDataKelasPublic();
        getDataKelasku();
        getDataPustaka();
        return super.onStartCommand(intent, flags, startId);
    }

    public void onSuccessGetData(String type){
        if (type.equals("Kelasku")){
            if(sizeKelasku != new KelasModel(context).allKelasKu().size()){
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onSuccessGetData("Kelasku");
                    }
                }, 1000);
            }
            else {
                listSuccessAPI.put(type, true);
                getAllDataDetailKelasku();
            }
        } else {
            listSuccessAPI.put(type, true);
            if(listSuccessAPI.size() >= 8){
                Intent intent = new Intent("fetch_data_broadcast");
                intent.putExtra("status", "success");
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        }
    }

    public void onFailedGetData(String type){
        Intent intent = new Intent("fetch_data_broadcast");
        intent.putExtra("status", "failed");
        listSuccessAPI.put(type, false);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public void getDataKelasPrivate(){
        new GetListKelasController(getApplicationContext(), App.session.getUserDetails().getId(), App.session.getUserDetails().getRole_id(), 1){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                onSuccessGetData("Kelas Private");
            }

            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                onFailedGetData("Kelas Private");
            }
        }.executeAPI();
    }

    public void getDataKelasPublic(){
        new GetListKelasController(context, App.session.getUserDetails().getId(), App.session.getUserDetails().getRole_id(), 2){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                onSuccessGetData("Kelas Public");
            }
            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                onFailedGetData("Kelas Public");
            }
        }.executeAPI();
    }

    public void getDataKelasku(){
        new GetListKelasController(context, App.session.getUserDetails().getId(), App.session.getUserDetails().getRole_id(), 3){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                sizeKelasku = listKelasKuParser.getResult().size();
                onSuccessGetData("Kelasku");
            }
            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                onFailedGetData("Kelasku");
            }
        }.executeAPI();
    }

    private int i;

    public void getAllDataDetailKelasku(){
        KelasModel kelasModel = new KelasModel(context);
        final int sizeKelas = kelasModel.allKelasKu().size();
        i = 0;
        for (Kelas kelas : kelasModel.allKelasKu()){
            new DetailKelasController(context, App.session.getUserDetails().getId(), App.session.getUserDetails().getRole_id(), kelas.getId()){
                @Override
                public void onAPIsuccess() {
                    super.onAPIsuccess();
                    i++;
                    if (i == sizeKelas){
                        onSuccessGetData("Detail Kelasku");
                    }

                }

                @Override
                public void onAPIFailed(String errorMessage) {
                    super.onAPIFailed(errorMessage);
                    onFailedGetData("Detail Kelasku");
                }
            }.executeAPI();
        }
    }

    public void getDataPustaka(){
        new PustakaCatatanController(context){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                onSuccessGetData("Pustaka Catatan");
            }
            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                onFailedGetData("Pustaka Catatan");
            }
        }.executeAPI();

        new PustakaPhotoController(context){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                onSuccessGetData("Pustaka Photo");
            }
            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                onFailedGetData("Pustaka Photo");
            }
        }.executeAPI();

        new PustakaVideoController(context){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                onSuccessGetData("Pustaka Video");
            }
            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                onFailedGetData("Pustaka Video");
            }
        }.executeAPI();

        new PustakaTautanController(context){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                onSuccessGetData("Pustaka Tautan");
            }
            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                onFailedGetData("Pustaka Tautan");
            }
        }.executeAPI();
    }
}

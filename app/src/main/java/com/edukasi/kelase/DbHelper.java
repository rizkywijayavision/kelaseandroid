package com.edukasi.kelase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.edukasi.kelase.entities.Kelas;
import com.edukasi.kelase.entities.KelasKu;
import com.edukasi.kelase.entities.KelasSesi;
import com.edukasi.kelase.entities.KelasSubSesi;
import com.edukasi.kelase.entities.PustakaCatatan;
import com.edukasi.kelase.entities.PustakaPhoto;
import com.edukasi.kelase.entities.PustakaTautan;
import com.edukasi.kelase.entities.PustakaVideo;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by ebizu-rizky on 11/29/15.
 */
public class DbHelper extends OrmLiteSqliteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private Context context;

    public DbHelper(Context context) {
        super(context, Config.DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        createTable();
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        App.log("---Upgrade Database v" + newVersion + "--");
        resetDatabase();
        createTable();
    }

    private void createTable() {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Kelas.class);
            TableUtils.createTableIfNotExists(connectionSource, KelasKu.class);
            TableUtils.createTableIfNotExists(connectionSource, KelasSesi.class);
            TableUtils.createTableIfNotExists(connectionSource, KelasSubSesi.class);
            TableUtils.createTableIfNotExists(connectionSource, PustakaCatatan.class);
            TableUtils.createTableIfNotExists(connectionSource, PustakaPhoto.class);
            TableUtils.createTableIfNotExists(connectionSource, PustakaVideo.class);
            TableUtils.createTableIfNotExists(connectionSource, PustakaTautan.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void resetDatabase() {
        try {
            TableUtils.dropTable(connectionSource, Kelas.class, true);
            TableUtils.dropTable(connectionSource, KelasKu.class, true);
            TableUtils.dropTable(connectionSource, KelasSesi.class, true);
            TableUtils.dropTable(connectionSource, KelasSubSesi.class, true);
            TableUtils.dropTable(connectionSource, PustakaCatatan.class, true);
            TableUtils.dropTable(connectionSource, PustakaPhoto.class, true);
            TableUtils.dropTable(connectionSource, PustakaVideo.class, true);
            TableUtils.dropTable(connectionSource, PustakaTautan.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public <D extends Dao<T, ?>, T> D getDao(Class<T> clazz)
            throws SQLException {
        return super.getDao(clazz);
    }

    @Override
    public void close() {
        super.close();

    }
}

/**
 * @author egiadtya
 * 8 September 2014
 */
package com.edukasi.kelase.base.interfaces;

public interface ActivityInterface {
	public void initView();
	public void setUICallbacks();
	public int getLayout();
	public void updateUI();
}

/**
 * @author egiadtya
 */
package com.edukasi.kelase.base.interfaces;

import org.json.JSONException;

public interface JSONParserInterface {
	public boolean parse(String json) throws JSONException;
}

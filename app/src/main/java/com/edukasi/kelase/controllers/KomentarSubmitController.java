package com.edukasi.kelase.controllers;

import android.content.Context;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/22/15.
 */
public class KomentarSubmitController extends BaseAPIController {
    
    public KomentarSubmitController(Context context, String ID_STATUS, String ID_USER, String ID_PEMILIK_STATUS, String komentar) {
        super(context);
        addParameter("status_id", ID_STATUS);
        addParameter("userid", ID_USER);
        addParameter("userid_status", ID_PEMILIK_STATUS);
        addParameter("status_msg", komentar);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return true;
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("status/comment");
    }
}

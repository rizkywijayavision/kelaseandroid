package com.edukasi.kelase.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import com.edukasi.kelase.App;
import com.edukasi.kelase.controllers.parsers.UserProfileParser;
import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/19/15.
 */
public class UserProfileController extends BaseAPIController {

    private ProgressDialog progressBar;
    private UserProfileParser userProfileParser;

    public UserProfileController(Context mContext, ProgressDialog progressBar, String username, String role_id){
        super(mContext);
        this.progressBar = progressBar;
        addParameter("userid", username);
        addParameter("role_id", role_id);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        userProfileParser = new UserProfileParser(context);
        errorMessage = userProfileParser.getParseMessage();
        userProfileParser.parseSingle(json);
        return userProfileParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {
        progressBar.dismiss();
        App.session.saveProfileSession(userProfileParser.getResultSingle());
        Intent intent = new Intent("user_broadcast");
        intent.putExtra("message", "get_profile");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onAPIFailed(String errorMessage) {
        super.onAPIFailed(errorMessage);
        progressBar.dismiss();
    }

    @Override
    public void executeAPI() {
        POST("account/profile");
        progressBar.show();
    }
}

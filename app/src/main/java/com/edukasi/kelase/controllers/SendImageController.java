package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.SendImageParser;

import org.json.JSONException;

import java.io.File;

/**
 * Created by ebizu-rizky on 11/23/15.
 */
public class SendImageController extends BaseAPIController {

    private FileAttachment fileAttachment;
    protected SendImageParser sendImageParser;

    public SendImageController(Context context, File file) {
        super(context);
        fileAttachment = new FileAttachment();
        fileAttachment.setFile(file);
        fileAttachment.setKey("files");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        sendImageParser = new SendImageParser(context);
        errorMessage = sendImageParser.getParseMessage();
        return sendImageParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {
    }

    @Override
    public void executeAPI() {
        POST_WITH_IMAGE_WO_PARAMS("status/send_image", fileAttachment);
    }
}

package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.App;
import com.edukasi.kelase.entities.KelasKu;
import com.edukasi.kelase.models.KelasKuModel;

import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 11/29/15.
 */
public class ListKelasKuParser extends BaseParser<KelasKu> {

    KelasKuModel kelasKuModel;

    public ListKelasKuParser(Context context) {
        super(context);
        kelasKuModel = new KelasKuModel(context);
    }

    @Override
    public KelasKu parseJsonObject(JSONObject jObj) {
        KelasKu item = new KelasKu();
        item.setId(jObj.optString("ID_KELAS"));
        item.setRole_id(App.session.getUserDetails().getRole_id());
        kelasKuModel.update(item);
        return item;
    }
}

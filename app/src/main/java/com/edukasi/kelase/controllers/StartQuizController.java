package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.StartQuizParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 1/4/16.
 */
public class StartQuizController extends BaseAPIController {

    protected StartQuizParser startQuizParser;

    public StartQuizController(Context context, String id_kelas, String id_sesi, String id_sub_sesi, String id_pengguna, String id_role) {
        super(context);
        addParameter("id_kelas", id_kelas);
        addParameter("id_sesi", id_sesi);
        addParameter("id_sub_sesi", id_sub_sesi);
        addParameter("id_pengguna", id_pengguna);
        addParameter("id_role", id_role);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        startQuizParser = new StartQuizParser(context);
        errorMessage = startQuizParser.getParseMessage();
        return startQuizParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {
        startQuizParser.getResultSingle();
    }

    @Override
    public void executeAPI() {
        POST("kelas/quiz/start_quiz");
    }
}

package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.SendImageParser;

import org.json.JSONException;

import java.io.File;

/**
 * Created by ebizu-rizky on 11/24/15.
 */
public class ChangeProfpictController extends BaseAPIController {
    
    private FileAttachment fileAttachment;
    protected SendImageParser sendImageParser;
    
    public ChangeProfpictController(Context context, String userId, File file, String height, String width) {
        super(context);
        fileAttachment = new FileAttachment();
        fileAttachment.setKey("files");
        fileAttachment.setFile(file);
        addParameter("userid", userId);
        addParameter("height", height);
        addParameter("width", width);
        addParameter("x1", "0");
        addParameter("y1", "0");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        sendImageParser = new SendImageParser(context);
        errorMessage = sendImageParser.getParseMessage();
        return sendImageParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST_WITH_IMAGE("account/profile/image", fileAttachment);
    }
}

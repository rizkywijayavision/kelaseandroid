package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.BaseParser;
import com.edukasi.kelase.entities.SubSesiTopik;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class TopikParser extends BaseParser<SubSesiTopik> {

    public TopikParser(Context context) {
        super(context);
    }

    @Override
    public SubSesiTopik parseJsonObject(JSONObject jObj) {
        SubSesiTopik subSesiTopik = new SubSesiTopik();
        subSesiTopik.setID_TOPIK(jObj.optString("ID_TOPIK"));
        subSesiTopik.setID_SESI(jObj.optString("ID_SESI"));
        subSesiTopik.setID_SUB_SESI(jObj.optString("ID_SUB_SESI"));
        subSesiTopik.setJUDUL_TOPIK(jObj.optString("JUDUL_TOPIK"));
        subSesiTopik.setDESKRIPSI_TOPIK(jObj.optString("DESKRIPSI_TOPIK"));
        subSesiTopik.setNAMA_PENGGUNA(jObj.optString("NAMA_PENGGUNA"));
        subSesiTopik.setDATE_CREATED(jObj.optString("DATE_CREATED"));
        return subSesiTopik;
    }

    @Override
    public boolean parse(String json){
        JSONObject resObj;
        try {
            resObj = new JSONObject(json);
            if (resObj.optString(JSON_STATUS).equals("success")) {
                resObj = resObj.optJSONObject(JSON_ARRAY_ROOT);
                JSONArray jsonArr = resObj.optJSONArray("LIST_TOPIK");
                result = parseJsonArray(jsonArr);
                return true;
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}

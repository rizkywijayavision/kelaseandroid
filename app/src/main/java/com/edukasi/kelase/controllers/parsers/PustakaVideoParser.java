package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.PustakaVideo;
import com.edukasi.kelase.models.PustakaVideoModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaVideoParser extends BaseParser<PustakaVideo> {

    private PustakaVideoModel pustakaVideoModel;

    public PustakaVideoParser(Context context) {
        super(context);
        pustakaVideoModel = new PustakaVideoModel(context);
    }

    @Override
    public PustakaVideo parseJsonObject(JSONObject jObj) {
        PustakaVideo pustakaVideo = new PustakaVideo();
        pustakaVideo.setId(jObj.optString("ID_VIDEO"));
        pustakaVideo.setNAMA_VIDEO(jObj.optString("NAMA_VIDEO"));
        pustakaVideo.setLINK_VIDEO(jObj.optString("LINK_VIDEO"));
        pustakaVideo.setPRIVASI(jObj.optString("PRIVASI"));
        pustakaVideo.setDATE_CREATED(jObj.optString("DATE_CREATED"));
        pustakaVideo.setID_PENGGUNA(jObj.optString("ID_PENGGUNA"));
        pustakaVideo.setSOURCE(jObj.optString("SOURCE"));
        pustakaVideoModel.update(pustakaVideo);
        return pustakaVideo;
    }

    @Override
    public boolean parse(String json){
        JSONObject resObj;
        try {
            resObj = new JSONObject(json);
            if (resObj.optString(JSON_STATUS).equals("success")) {
                resObj = resObj.optJSONObject(JSON_ARRAY_ROOT);
                JSONArray jsonArr = resObj.optJSONArray("videos");
                result = parseJsonArray(jsonArr);
                return true;
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}

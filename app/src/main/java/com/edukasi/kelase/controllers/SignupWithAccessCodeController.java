package com.edukasi.kelase.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/19/15.
 */
public class SignupWithAccessCodeController extends BaseAPIController {

    private ProgressDialog progressBar;

    public SignupWithAccessCodeController(Context context, ProgressDialog progressBar, String firstname, String lastname, String username,
                                          String password, String email, String code_access, String role) {
        super(context);
        this.progressBar = progressBar;
        addParameter("firstname", firstname);
        addParameter("lastname", lastname);
        addParameter("username", username);
        addParameter("password", password);
        addParameter("email", email);
        addParameter("code_access", code_access);
        addParameter("role", role);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return true;
    }

    @Override
    public void onAPIsuccess() {
        progressBar.dismiss();
        Intent intent = new Intent("user_broadcast");
        intent.putExtra("message", "with_code_access");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onAPIFailed(String errorMessage) {
        super.onAPIFailed(errorMessage);
        progressBar.dismiss();
    }

    @Override
    public void executeAPI() {
        POST("account/register/member");
        progressBar.show();
    }
}

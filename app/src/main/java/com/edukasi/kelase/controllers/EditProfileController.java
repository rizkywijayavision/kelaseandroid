package com.edukasi.kelase.controllers;

import android.content.Context;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/24/15.
 */
public class EditProfileController extends BaseAPIController {

    public EditProfileController(Context context, String userId, String peran, String name, String alamat, String negara, String provinsi,
                                 String phone, String birthplace, String birtdate) {
        super(context);
        addParameter("userid", userId);
        addParameter("role_id", peran);
        addParameter("fullname",name);
        addParameter("address", alamat);
        addParameter("country", negara);
        addParameter("province", provinsi);
        addParameter("phone", phone);
        addParameter("birthday_place", birthplace);
        addParameter("birthday_date", birtdate);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return true;
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("account/profile/update");
    }
}

package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.LinimasaParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/20/15.
 */
public class LinimasaController extends BaseAPIController {

    protected LinimasaParser linimasaParser;

    public LinimasaController(Context context, String idUser, String limit, String offset) {
        super(context);
        addParameter("userid", idUser);
        addParameter("limit", limit);
        addParameter("offset", offset);
        addParameter("type", "all");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        linimasaParser = new LinimasaParser(context);
        errorMessage = linimasaParser.getParseMessage();
        return linimasaParser.parse(json);
    }

    @Override
    public void onAPIsuccess() {
    }

    @Override
    public void onAPIFailed(String errorMessage) {
        super.onAPIFailed(errorMessage);
    }

    @Override
    public void executeAPI() {
        POST("timeline/list");
    }
}

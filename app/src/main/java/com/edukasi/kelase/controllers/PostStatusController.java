package com.edukasi.kelase.controllers;

import android.content.Context;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/22/15.
 */
public class PostStatusController extends BaseAPIController{

    private boolean withImage;
    
    public PostStatusController(Context context, String UserID, String statusContent) {
        super(context);
        addParameter("status_type", "share");
        addParameter("userid", UserID);
        addParameter("status_msg", statusContent);
    }

    public PostStatusController(Context context, String UserID, String statusContent, String Image) {
        super(context);
        addParameter("status_type", "photo");
        addParameter("userid", UserID);
        addParameter("status_msg", statusContent);
        addParameter("img", Image);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return true;
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("status/send");
    }
}
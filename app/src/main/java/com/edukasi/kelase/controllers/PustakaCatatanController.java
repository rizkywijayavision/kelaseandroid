package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.App;
import com.edukasi.kelase.controllers.parsers.PustakaCatatanParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaCatatanController extends BaseAPIController {

    private PustakaCatatanParser pustakaCatatanParser;

    public PustakaCatatanController(Context context) {
        super(context);
        addParameter("userid", App.session.getUserDetails().getId());
        addParameter("is_user_login", "true");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        pustakaCatatanParser = new PustakaCatatanParser(context);
        errorMessage = pustakaCatatanParser.getParseMessage();
        return pustakaCatatanParser.parse(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("pustaka/notes");
    }
}

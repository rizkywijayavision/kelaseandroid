package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.DetailKelasParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/29/15.
 */
public class DetailKelasController extends BaseAPIController {

    private String idKelas;
    private DetailKelasParser detailKelasParser;

    public DetailKelasController(Context context, String idUser, String roleId, String idKelas) {
        super(context);
        this.idKelas = idKelas;
        addParameter("id_pengguna", idUser);
        addParameter("id_role", roleId);
        addParameter("id_kelas", idKelas);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        detailKelasParser = new DetailKelasParser(context, idKelas);
        errorMessage = detailKelasParser.getParseMessage();
        return detailKelasParser.parseDetailKelas(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("kelas/detail");
    }
}

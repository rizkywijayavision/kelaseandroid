package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.App;
import com.edukasi.kelase.controllers.parsers.PustakaVideoParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaVideoController extends BaseAPIController {

    private PustakaVideoParser pustakaVideoParser;

    public PustakaVideoController(Context context) {
        super(context);
        addParameter("userid", App.session.getUserDetails().getId());
        addParameter("is_user_login", "true");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        pustakaVideoParser = new PustakaVideoParser(context);
        errorMessage = pustakaVideoParser.getParseMessage();
        return pustakaVideoParser.parse(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("pustaka/videos");
    }
}

package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.App;

import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by ebizu-rizky on 1/5/16.
 */
public class SubmitQuizController extends BaseAPIController {

    JSONArray jawaban_soal;

    public SubmitQuizController(Context context, String id_kelas, String id_sesi, String id_sub_sesi, String id_pengguna, String id_role, JSONArray jawaban_soal) {
        super(context);
        addParameter("id_kelas", id_kelas);
        addParameter("id_sesi", id_sesi);
        addParameter("id_sub_sesi", id_sub_sesi);
        addParameter("id_pengguna", id_pengguna);
        addParameter("id_role", id_role);
        this.jawaban_soal = jawaban_soal;
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return true;
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("kelas/quiz/submit_quiz");
    }

    @Override
    public void POST(String url,String endpoint) {
        url = url + endpoint;
        App.log(url);
        JSONObject jsonParams;
        if (paramMap.size() > 0) {
            jsonParams = new JSONObject(paramMap);
            try {
                jsonParams.put("jawaban_soal", jawaban_soal);
                App.log(jsonParams.toString());
                ByteArrayEntity entity = new ByteArrayEntity(jsonParams.toString().getBytes("UTF-8"));
                client.post(context, url, entity, "application/json", responseHandler);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            client.post(url, responseHandler);
        }
    }
}

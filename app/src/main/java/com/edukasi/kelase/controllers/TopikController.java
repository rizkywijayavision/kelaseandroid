package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.TopikParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class TopikController extends BaseAPIController {

    protected TopikParser topikParser;

    public TopikController(Context context, String id_kelas, String id_sesi, String id_sub_sesi, String id_pengguna, String id_role) {
        super(context);
        addParameter("id_kelas", id_kelas);
        addParameter("id_sesi", id_sesi);
        addParameter("id_sub_sesi", id_sub_sesi);
        addParameter("id_pengguna", id_pengguna);
        addParameter("id_role", id_role);
        addParameter( "with_topik_list", "true");
        addParameter("topik_option", "{\"limit\":10,\"offset\":0,\"with_count\":true}");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        topikParser = new TopikParser(context);
        return topikParser.parse(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("kelas/forum/detail");
    }
}

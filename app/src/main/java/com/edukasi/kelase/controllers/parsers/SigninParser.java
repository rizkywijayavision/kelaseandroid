package com.edukasi.kelase.controllers.parsers;

import android.content.Context;
import com.edukasi.kelase.sessions.LoginSession;

import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 11/19/15.
 */
public class SigninParser extends BaseParser<LoginSession>{

    public SigninParser(Context context) {
        super(context);
    }

    @Override
    public LoginSession parseJsonObject(JSONObject jObj) {
        LoginSession loginSession = new LoginSession();
        loginSession.setName(jObj.optString("name"));
        loginSession.setId(jObj.optString("id"));
        loginSession.setRole_id(jObj.optString("role_id"));
        loginSession.setUsername(jObj.optString("username"));
        return loginSession;
    }
}

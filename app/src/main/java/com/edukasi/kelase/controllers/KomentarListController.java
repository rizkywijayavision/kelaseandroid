package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.KomentarParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/22/15.
 */
public class KomentarListController extends BaseAPIController {

    protected KomentarParser komentarParser;

    public KomentarListController(Context context, String ID_STATUS, String ID_USER) {
        super(context);
        addParameter("statusid",ID_STATUS);
        addParameter("userid",ID_USER);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        komentarParser = new KomentarParser(context);
        errorMessage = komentarParser.getParseMessage();
        return komentarParser.parse(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("comment/list");
    }
}

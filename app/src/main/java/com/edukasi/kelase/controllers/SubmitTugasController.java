package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.SendImageParser;

import org.json.JSONException;

import java.io.File;

/**
 * Created by ebizu-rizky on 1/4/16.
 */
public class SubmitTugasController extends BaseAPIController{

    private FileAttachment fileAttachment;
    protected SendImageParser sendImageParser;

    public SubmitTugasController(Context context, String id_kelas, String id_sesi, String id_sub_sesi, String id_pengguna, String id_role, File file_tugas) {
        super(context);
        addParameter("id_kelas", id_kelas);
        addParameter("id_sesi", id_sesi);
        addParameter("id_sub_sesi", id_sub_sesi);
        addParameter("id_pengguna", id_pengguna);
        addParameter("id_role", id_role);
        fileAttachment = new FileAttachment();
        fileAttachment.setFile(file_tugas);
        fileAttachment.setKey("file_tugas");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        sendImageParser = new SendImageParser(context);
        errorMessage = sendImageParser.getParseMessage();
        return sendImageParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST_WITH_IMAGE("kelas/tugas/submit_tugas", fileAttachment);
    }
}

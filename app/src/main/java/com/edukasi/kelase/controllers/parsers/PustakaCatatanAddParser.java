package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.App;
import com.edukasi.kelase.entities.PustakaCatatan;
import com.edukasi.kelase.models.PustakaCatatanModel;

import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 1/25/16.
 */
public class PustakaCatatanAddParser extends BaseParser<PustakaCatatan> {

    private PustakaCatatanModel pustakaCatatanModel;

    public PustakaCatatanAddParser(Context context) {
        super(context);
        pustakaCatatanModel = new PustakaCatatanModel(context);
    }

    @Override
    public PustakaCatatan parseJsonObject(JSONObject jObj) {
        PustakaCatatan pustakaCatatan = new PustakaCatatan();
        pustakaCatatan.setId(jObj.optString("ID_CATATAN"));
        pustakaCatatan.setJUDUL_CATATAN(jObj.optString("judul_catatan"));
        pustakaCatatan.setKONTEN_CATATAN(jObj.optString("konten_catatan"));
        pustakaCatatan.setPRIVASI(jObj.optString("privasi"));
        pustakaCatatan.setDATE_CREATED(jObj.optString("DATE_CREATED"));
        pustakaCatatan.setID_PENGGUNA(jObj.optString("id_pengguna"));
        pustakaCatatan.setTAG(jObj.optString("tag"));
        pustakaCatatan.setNAMA_DEPAN(jObj.optString("NAMA_DEPAN"));
        pustakaCatatan.setUSERNAME(App.session.getUserDetails().getUsername());
        pustakaCatatan.setIMAGE(App.session.getProfileData().getProfile_image());
        pustakaCatatan.setID_ROLE(App.session.getUserDetails().getRole_id());
        pustakaCatatanModel.update(pustakaCatatan);
        return pustakaCatatan;
    }
}

package com.edukasi.kelase.controllers;

import android.app.ProgressDialog;
import android.content.Context;

import com.edukasi.kelase.App;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 1/25/16.
 */
public class PustakaDeleteController extends BaseAPIController {

    private int type;
    private ProgressDialog progress;

    public PustakaDeleteController(Context context, int type, String id) {
        super(context);
        this.type = type;
        switch (type){
            case 0:
                addParameter("photo_id", id);
                break;
            case 1:
                addParameter("note_id", id);
                break;
            case 2:
                addParameter("video_id", id);
                break;
            case 3:
                addParameter("userid", App.session.getUserDetails().getId());
                addParameter("id_tautan", id);
                break;
        }
        progress = new ProgressDialog(context);
        progress.setTitle("Delete Pustaka");
        progress.setMessage("Mohon Tunggu Sebentar...");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return true;
    }

    @Override
    public void onAPIsuccess() {
        progress.dismiss();
    }

    @Override
    public void onAPIFailed(String errorMessage) {
        super.onAPIFailed(errorMessage);
        progress.dismiss();
    }

    @Override
    public void executeAPI() {
        progress.show();
        switch (type){
            case 0:
                POST("pustaka/delete_photo");
                break;
            case 1:
                POST("pustaka/delete_notes");
                break;
            case 2:
                POST("pustaka/delete_videos");
                break;
            case 3:
                POST("pustaka/delete_links");
                break;
        }
    }
}

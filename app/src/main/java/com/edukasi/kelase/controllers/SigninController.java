package com.edukasi.kelase.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.edukasi.kelase.App;
import com.edukasi.kelase.controllers.parsers.SigninParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/16/15.
 */


public class SigninController extends BaseAPIController {

    private ProgressDialog progressBar;
    private SigninParser signinParser;
    private String username, password;

    public SigninController(Context mContext, ProgressDialog progressBar, String username, String password){
        super(mContext);
        this.progressBar = progressBar;
        this.username = username;
        this.password = password;
        addParameter("username", username);
        addParameter("password", password);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        signinParser = new SigninParser(context);
        errorMessage = signinParser.getParseMessage();
        return signinParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {
        progressBar.dismiss();
        App.session.createLoginSession(signinParser.getResultSingle());
        Intent intent = new Intent("user_broadcast");
        intent.putExtra("message", "login");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onAPIFailed(String errorMessage) {
        super.onAPIFailed(errorMessage);
        progressBar.dismiss();
    }

    @Override
    public void executeAPI() {
        POST("account/login");
        progressBar.show();
    }
}

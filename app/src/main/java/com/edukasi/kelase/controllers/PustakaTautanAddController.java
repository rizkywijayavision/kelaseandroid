package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.App;
import com.edukasi.kelase.controllers.parsers.PustakaTautanParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 1/25/16.
 */
public class PustakaTautanAddController extends BaseAPIController {

    private PustakaTautanParser pustakaTautanParser;

    public PustakaTautanAddController(Context context, String url) {
        super(context);
        addParameter("userid", App.session.getUserDetails().getId());
        addParameter("url", url);
        addParameter("deskripsi_tautan", "deskripsi_tautan");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        pustakaTautanParser = new PustakaTautanParser(context);
        errorMessage = pustakaTautanParser.getParseMessage();
        return pustakaTautanParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("pustaka/add_links");
    }
}

package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.KelasSesi;
import com.edukasi.kelase.entities.KelasSubSesi;
import com.edukasi.kelase.models.KelasSesiModel;
import com.edukasi.kelase.models.KelasSubSesiModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 11/29/15.
 */

public class DetailKelasParser extends BaseParser<KelasSesi> {

    String idKelas;

    public DetailKelasParser(Context context, String idKelas) {
        super(context);
        this.idKelas = idKelas;
    }

    @Override
    public KelasSesi parseJsonObject(JSONObject jObj) {
        KelasSesi kelasSesi = new KelasSesi();
        kelasSesi.setID_KELAS(idKelas);
        kelasSesi.setId(jObj.optString("ID_SESI"));
        kelasSesi.setJUDUL_SESI(jObj.optString("JUDUL_SESI"));
        kelasSesi.setDESKRIPSI_SESI(jObj.optString("DESKRIPSI_SESI"));
        KelasSesiModel kelasSesiModel = new KelasSesiModel(context);
        kelasSesiModel.update(kelasSesi);
        JSONArray listSubSesi = jObj.optJSONArray("sub_sesi");
        for(int i = 0 ; i < listSubSesi.length(); i++){
            try {
                JSONObject subSesi = listSubSesi.getJSONObject(i);
                KelasSubSesi kelasSubSesi = new KelasSubSesi();
                kelasSubSesi.setId(subSesi.optString("ID_SUB_SESI"));
                kelasSubSesi.setID_KELAS(idKelas);
                kelasSubSesi.setID_SESI(jObj.optString("ID_SESI"));
                kelasSubSesi.setURUTAN(subSesi.optString("URUTAN"));
                kelasSubSesi.setIS_DONE(subSesi.optString("IS_DONE"));
                kelasSubSesi.setID_TYPE_SUB_SESI(subSesi.optString("ID_TYPE_SUB_SESI"));
                kelasSubSesi.setJUDUL_SUB_SESI(subSesi.optString("JUDUL_SUB_SESI"));
                kelasSubSesi.setDESKRIPSI_SUB_SESI(subSesi.optString("DESKRIPSI_SUB_SESI"));
                kelasSubSesi.setIS_ACTIVE(subSesi.optString("IS_ACTIVE"));
                kelasSubSesi.setTYPE_SUB_SESI(subSesi.optString("TYPE_SUB_SESI"));
                KelasSubSesiModel kelasSubSesiModel = new KelasSubSesiModel(context);
                kelasSubSesiModel.update(kelasSubSesi);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return kelasSesi;
    }
}
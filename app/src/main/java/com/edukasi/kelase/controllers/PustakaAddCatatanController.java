package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.PustakaCatatanAddParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 1/20/16.
 */
public class PustakaAddCatatanController extends BaseAPIController {

    private PustakaCatatanAddParser pustakaCatatanAddParser;

    public PustakaAddCatatanController(Context context, String userid, String note_title, String note_content) {
        super(context);
        addParameter("userid", userid);
        addParameter("note_title", note_title);
        addParameter("note_content", note_content);
        addParameter("privasi", "1");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        pustakaCatatanAddParser = new PustakaCatatanAddParser(context);
        errorMessage = pustakaCatatanAddParser.getParseMessage();
        return pustakaCatatanAddParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("pustaka/add_notes");
    }
}

package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 11/24/15.
 */
public class SendImageParser extends BaseParser<String> {

    public SendImageParser(Context context) {
        super(context);
    }

    @Override
    public String parseJsonObject(JSONObject jObj) {
        return jObj.optString("name");
    }
}

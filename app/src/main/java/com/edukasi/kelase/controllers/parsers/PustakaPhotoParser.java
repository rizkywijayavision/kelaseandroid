package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.PustakaPhoto;
import com.edukasi.kelase.models.PustakaPhotoModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaPhotoParser extends BaseParser<PustakaPhoto> {

    private PustakaPhotoModel pustakaPhotoModel;

    public PustakaPhotoParser(Context context) {
        super(context);
        pustakaPhotoModel = new PustakaPhotoModel(context);
    }

    @Override
    public PustakaPhoto parseJsonObject(JSONObject jObj) {
        PustakaPhoto pustakaPhoto = new PustakaPhoto();
        pustakaPhoto.setId(jObj.optString("ID_FOTO"));
        pustakaPhoto.setNAMA_FOTO(jObj.optString("NAMA_FOTO"));
        pustakaPhoto.setFILE_FOTO(jObj.optString("FILE_FOTO"));
        pustakaPhoto.setDATE_CREATED(jObj.optString("DATE_CREATED"));
        pustakaPhoto.setID_ALBUM_FOTO(jObj.optString("ID_ALBUM_FOTO"));
        pustakaPhoto.setID_PENGGUNA(jObj.optString("ID_PENGGUNA"));
        pustakaPhoto.setPATH(jObj.optString("PATH"));
        pustakaPhotoModel.update(pustakaPhoto);
        return pustakaPhoto;
    }

    @Override
    public boolean parse(String json){
        JSONObject resObj;
        try {
            resObj = new JSONObject(json);
            if (resObj.optString(JSON_STATUS).equals("success")) {
                resObj = resObj.optJSONObject(JSON_ARRAY_ROOT);
                JSONArray jsonArr = resObj.optJSONArray("photos");
                result = parseJsonArray(jsonArr);
                return true;
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}

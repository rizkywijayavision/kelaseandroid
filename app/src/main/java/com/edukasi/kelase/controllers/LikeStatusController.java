package com.edukasi.kelase.controllers;

import android.content.Context;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/20/15.
 */
public class LikeStatusController extends BaseAPIController {

    public LikeStatusController(Context context, String idUser, String idStatus, String flag) {
        super(context);
        addParameter("userid", idUser);
        addParameter("status_id", idStatus);
        addParameter("flag", flag);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return true;
    }

    @Override
    public void onAPIsuccess() {
    }

    @Override
    public void executeAPI() {
        POST("status/favorite");
    }
}

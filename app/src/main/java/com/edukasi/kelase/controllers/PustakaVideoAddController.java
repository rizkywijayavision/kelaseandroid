package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.PustakaVideoParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 1/25/16.
 */

public class PustakaVideoAddController extends BaseAPIController {

    private PustakaVideoParser pustakaVideoParser;

    public PustakaVideoAddController(Context context, String userid, String video_name, String video_link) {
        super(context);
        addParameter("userid", userid);
        addParameter("video_name", video_name);
        addParameter("video_link", video_link);
        addParameter("privasi", "1");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        pustakaVideoParser = new PustakaVideoParser(context);
        errorMessage = pustakaVideoParser.getParseMessage();
        return pustakaVideoParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("pustaka/add_videos");
    }
}

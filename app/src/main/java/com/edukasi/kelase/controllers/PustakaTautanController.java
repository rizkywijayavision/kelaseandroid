package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.App;
import com.edukasi.kelase.controllers.parsers.PustakaTautanParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 1/25/16.
 */
public class PustakaTautanController extends BaseAPIController {

    private PustakaTautanParser pustakaTautanParser;

    public PustakaTautanController(Context context) {
        super(context);
        addParameter("userid", App.session.getUserDetails().getId());
    }

    @Override
    public boolean parse(String json) throws JSONException {
        pustakaTautanParser = new PustakaTautanParser(context);
        errorMessage = pustakaTautanParser.getParseMessage();
        return pustakaTautanParser.parse(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("pustaka/links");
    }
}

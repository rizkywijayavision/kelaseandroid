package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.callbacks.OnParseJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseParser<T> implements OnParseJson<T> {
    public static final String JSON_STATUS = "status";
    public static final String JSON_ARRAY_ROOT = "data";
    public static final String JSON_MORE_KEY = "more";
    public static final String JSON_MESSAGE = "message";
    private String parseMessage = "";
    protected List<T> result;
    private T resultSingle;
    protected Context context;

    public BaseParser(Context context) {
        result = new ArrayList<T>();
        this.context = context;
    }

    public boolean parse(String json){
        JSONObject resObj;
        try {
            resObj = new JSONObject(json);
            if (resObj.optString(JSON_STATUS).equals("success")) {
                JSONArray jsonArr = resObj.optJSONArray(JSON_ARRAY_ROOT);
//                parseMessage = resObj.optString(JSON_MESSAGE);
                result = parseJsonArray(jsonArr);
                return true;
            } else {
//                parseMessage = resObj.optString(JSON_MESSAGE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean parseDetailKelas(String json){
        JSONObject resObj;
        try {
            resObj = new JSONObject(json);
            if (resObj.optString(JSON_STATUS).equals("success")) {
                resObj = resObj.optJSONObject(JSON_ARRAY_ROOT);
                JSONArray jsonArr = resObj.optJSONArray("sesi");
//                parseMessage = resObj.optString(JSON_MESSAGE);
                result = parseJsonArray(jsonArr);
                return true;
            } else {
//                parseMessage = resObj.optString(JSON_MESSAGE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean parseSingle(String json) {
        JSONObject resObj;
        try {
            resObj = new JSONObject(json);
            if (resObj.optString(JSON_STATUS).equals("success")) {
                parseMessage = resObj.optString(JSON_MESSAGE);
                resultSingle = parseJsonObject(resObj.optJSONObject(JSON_ARRAY_ROOT));
                return true;
            } else {
                parseMessage = resObj.optString(JSON_MESSAGE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<T> parseJsonArray(JSONArray jArr) {
        List<T> list = new ArrayList<T>();
        if (jArr != null) {
            for (int i = 0; i < jArr.length(); i++) {
                JSONObject jsonObject = jArr.optJSONObject(i);
                if (jsonObject != null) {
                    list.add(parseJsonObject(jsonObject));
                }
            }
        }
        return list;
    }

    public String getParseMessage() {
        return parseMessage;
    }

    public List<T> getResult() {
        return result;
    }

    public T getResultSingle() {return resultSingle;}
}
package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.ProfileData;

import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 11/19/15.
 */

public class UserProfileParser extends BaseParser<ProfileData>{

    public UserProfileParser(Context context) {
        super(context);
    }

    @Override
    public ProfileData parseJsonObject(JSONObject jObj) {
        jObj = jObj.optJSONObject("detail");
        ProfileData profile = new ProfileData();
        profile.setNama_depan(jObj.optString("nama_depan"));
        profile.setNama_belakang(jObj.optString("nama_belakang"));
        profile.setAlamat(jObj.optString("alamat"));
        profile.setNegara(jObj.optString("negara"));
        profile.setProvinsi(jObj.optString("provinsi"));
        profile.setNama_negara(jObj.optString("nama_negara"));
        profile.setNama_provinsi(jObj.optString("nama_provinsi"));
        profile.setTempat_lahir(jObj.optString("tempat_lahir"));
        profile.setTanggal_lahir(jObj.optString("tanggal_lahir"));
        profile.setTelepon(jObj.optString("telepon"));
        profile.setFacebook(jObj.optString("facebook"));
        profile.setTwitter(jObj.optString("twitter"));
        profile.setProfile_image(jObj.optString("profile_image"));
        profile.setCover_image(jObj.optString("cover_image"));
        return profile;
    }
}

package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.PustakaPhotoParser;

import org.json.JSONException;

import java.io.File;

/**
 * Created by ebizu-rizky on 1/25/16.
 */
public class PustakaPhotoAddController extends BaseAPIController {

    private FileAttachment fileAttachment;
    private PustakaPhotoParser pustakaPhotoParser;

    public PustakaPhotoAddController(Context context, String userId, File file) {
        super(context);
        fileAttachment = new FileAttachment();
        fileAttachment.setKey("upload_file");
        fileAttachment.setFile(file);
        addParameter("userid", userId);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        pustakaPhotoParser = new PustakaPhotoParser(context);
        errorMessage = pustakaPhotoParser.getParseMessage();
        return pustakaPhotoParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST_WITH_IMAGE("pustaka/add_photo", fileAttachment);
    }
}

package com.edukasi.kelase.controllers;

import android.content.Context;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 12/28/15.
 */
public class TampahKomentarTopikController extends BaseAPIController {

    public TampahKomentarTopikController(Context context, String id_kelas, String id_sesi,
                                         String id_sub_sesi, String id_pengguna, String id_role,
                                         String id_topik, String komentar) {
        super(context);
        addParameter("id_kelas", id_kelas);
        addParameter("id_sesi", id_sesi);
        addParameter("id_sub_sesi", id_sub_sesi);
        addParameter("id_pengguna", id_pengguna);
        addParameter("id_role", id_role);
        addParameter("id_topik", id_topik);
        addParameter("komentar", komentar);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return true;
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("kelas/forum/send_komentar");
    }
}

package com.edukasi.kelase.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/19/15.
 */
public class SignupWithoutAccessCodeController extends BaseAPIController {

    private ProgressDialog progressBar;

    public SignupWithoutAccessCodeController(Context context, ProgressDialog progressBar, String firstname, String lastname,
                                             String username, String password, String email, String institusi,
                                             String namalembaga, String alamatlembaga, String negara, String provinsi) {
        super(context);
        this.progressBar = progressBar;
        addParameter("firstname", firstname);
        addParameter("lastname", lastname);
        addParameter("username", username);
        addParameter("password", password);
        addParameter("email", email);
        addParameter("type", institusi);
        addParameter("name", namalembaga);
        addParameter("address", alamatlembaga);
        addParameter("country", negara);
        addParameter("province", provinsi);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return true;
    }

    @Override
    public void onAPIsuccess() {
        progressBar.dismiss();
        Intent intent = new Intent("user_broadcast");
        intent.putExtra("message", "without_code_access");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onAPIFailed(String errorMessage) {
        super.onAPIFailed(errorMessage);
        progressBar.dismiss();
    }

    @Override
    public void executeAPI() {
        POST("account/register/instansi");
        progressBar.show();
    }
}

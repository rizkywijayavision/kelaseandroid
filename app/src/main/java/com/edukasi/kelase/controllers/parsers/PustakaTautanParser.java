package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.PustakaTautan;
import com.edukasi.kelase.models.PustakaTautanModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 1/25/16.
 */
public class PustakaTautanParser extends BaseParser<PustakaTautan> {

    private PustakaTautanModel pustakaTautanModel;

    public PustakaTautanParser(Context context) {
        super(context);
        pustakaTautanModel = new PustakaTautanModel(context);
    }

    @Override
    public PustakaTautan parseJsonObject(JSONObject jObj) {
        PustakaTautan pustakaTautan = new PustakaTautan();
        pustakaTautan.setId(jObj.optString("ID_TAUTAN"));
        pustakaTautan.setMETA_TITLE(jObj.optString("META_TITLE"));
        pustakaTautan.setMETA_LINK(jObj.optString("META_LINK"));
        pustakaTautan.setMETA_IMAGE(jObj.optString("META_IMAGE"));
        pustakaTautan.setMETA_DESCRIPTION(jObj.optString("META_DESCRIPTION"));
        pustakaTautan.setDESKRIPSI_TAUTAN(jObj.optString("DESKRIPSI_TAUTAN"));
        pustakaTautan.setDATE_CREATED(jObj.optString("DATE_CREATED"));
        pustakaTautan.setID_PENGGUNA(jObj.optString("ID_PENGGUNA"));
        pustakaTautanModel.update(pustakaTautan);
        return pustakaTautan;
    }

    @Override
    public boolean parse(String json){
        JSONObject resObj;
        try {
            resObj = new JSONObject(json);
            if (resObj.optString(JSON_STATUS).equals("success")) {
                resObj = resObj.optJSONObject(JSON_ARRAY_ROOT);
                JSONArray jsonArr = resObj.optJSONArray("list");
                result = parseJsonArray(jsonArr);
                return true;
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}

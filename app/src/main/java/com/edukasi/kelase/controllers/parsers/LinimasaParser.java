package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.MomentListitem;

import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 11/20/15.
 */
public class LinimasaParser extends BaseParser<MomentListitem> {

    public LinimasaParser(Context context) {
        super(context);
    }

    @Override
    public MomentListitem parseJsonObject(JSONObject jObj) {
        MomentListitem items= new MomentListitem();
        items.setID_STATUS_PENGGUNA(jObj.optString("ID_STATUS_PENGGUNA").toString());
        items.setID_PENGGUNA(jObj.optString("ID_PENGGUNA").toString());
        items.setUSERNAME(jObj.optString("USERNAME").toString());
        items.setNAMA_DEPAN(jObj.optString("NAMA_DEPAN").toString());
        items.setNAMA_BELAKANG(jObj.optString("NAMA_BELAKANG").toString());
        items.setDATE_CREATED(jObj.optString("DATE_CREATED").toString());
        items.setCount_favorit(jObj.optString("count_favorit").toString());
        items.setSTATUS(jObj.optString("STATUS").toString());
        items.setMETA_TITLE(jObj.optString("META_TITLE").toString());
        items.setMETA_DESCRIPTION(jObj.optString("META_DESCRIPTION").toString());
        items.setMETA_URL(jObj.optString("META_URL").toString());
        items.setCount_favorit(jObj.optString("count_favorit").toString());
        items.setIMAGE(jObj.optString("IMAGE").toString());
        items.setIMAGE_STATUS(jObj.optString("IMAGE_STATUS").toString());
        items.setTYPE_STATUS(jObj.optString("TYPE_STATUS").toString());
        items.setMETA_IMAGE(jObj.optString("META_IMAGE").toString());
        items.setLINK(jObj.optString("LINK").toString());
        items.setLINK_NAME(jObj.optString("LINK_NAME").toString());
        items.setIs_rate(jObj.optString("is_rate").toString());
        items.setCount_favorit(jObj.optString("count_favorit").toString());
        items.setCOUNT_COMMENT(jObj.optString("COUNT_COMMENT").toString());
        return items;
    }
}

package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.App;
import com.edukasi.kelase.controllers.parsers.PustakaPhotoParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaPhotoController extends BaseAPIController {

    private PustakaPhotoParser pustakaPhotoParser;

    public PustakaPhotoController(Context context) {
        super(context);
        addParameter("userid", App.session.getUserDetails().getId());
        addParameter("is_user_login", "true");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        pustakaPhotoParser = new PustakaPhotoParser(context);
        errorMessage = pustakaPhotoParser.getParseMessage();
        return pustakaPhotoParser.parse(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("pustaka/photo");
    }
}

package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.SoalQuiz;
import com.edukasi.kelase.entities.SoalQuizEssay;
import com.edukasi.kelase.entities.SoalQuizPG;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ebizu-rizky on 1/4/16.
 */
public class StartQuizParser extends BaseParser<SoalQuiz> {

    public StartQuizParser(Context context) {
        super(context);
    }

    @Override
    public SoalQuiz parseJsonObject(JSONObject jObj) {
        SoalQuiz soalQuiz = new SoalQuiz();
        soalQuiz.setJUDUL_QUIZ(jObj.optString("JUDUL_QUIZ"));
        soalQuiz.setDESKRIPSI_QUIZ(jObj.optString("DESKRIPSI_QUIZ"));
        soalQuiz.setTANGGAL_MULAI_QUIZ(jObj.optString("TANGGAL_MULAI_QUIZ"));
        soalQuiz.setTANGGAL_SELESAI_QUIZ(jObj.optString("TANGGAL_SELESAI_QUIZ"));
        soalQuiz.setDURASI(jObj.optString("DURASI"));
        soalQuiz.setDATE_CREATED(jObj.optString("DATE_CREATED"));
        soalQuiz.setTANGGAL_START_QUIZ(jObj.optString("TANGGAL_START_QUIZ"));
        soalQuiz.setTANGGAL_EXPIRED_QUIZ(jObj.optString("TANGGAL_EXPIRED_QUIZ"));
        JSONArray jsonArraySoalPG = jObj.optJSONArray("SOAL_PG");
        List<SoalQuizPG> soalQuizListPG = new ArrayList<>();
        for (int i = 0; i < jsonArraySoalPG.length(); i++){
            try {
                JSONObject jsonObject = (JSONObject) jsonArraySoalPG.get(i);
                SoalQuizPG soalQuizPG = new SoalQuizPG();
                soalQuizPG.setSOAL(jsonObject.optString("SOAL"));
                soalQuizPG.setOPSI_A(jsonObject.optString("OPSI_A"));
                soalQuizPG.setOPSI_B(jsonObject.optString("OPSI_B"));
                soalQuizPG.setOPSI_C(jsonObject.optString("OPSI_C"));
                soalQuizPG.setOPSI_D(jsonObject.optString("OPSI_D"));
                soalQuizPG.setID_SOAL(jsonObject.optString("ID_SOAL"));
                soalQuizPG.setBOBOT_SOAL(jsonObject.optString("BOBOT_SOAL"));
                soalQuizListPG.add(soalQuizPG);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Collections.reverse(soalQuizListPG);
        soalQuiz.setSoalQuizPGList(soalQuizListPG);
        JSONArray jsonArraySoalEssay = jObj.optJSONArray("SOAL_ESSAY");
        List<SoalQuizEssay> soalQuizListEssay = new ArrayList<>();
        for (int i = 0; i < jsonArraySoalEssay.length(); i++){
            try {
                JSONObject jsonObject = (JSONObject) jsonArraySoalEssay.get(i);
                SoalQuizEssay soalQuizEssay = new SoalQuizEssay();
                soalQuizEssay.setSOAL(jsonObject.optString("SOAL"));
                soalQuizEssay.setID_SOAL(jsonObject.optString("ID_SOAL"));
                soalQuizEssay.setBOBOT_SOAL(jsonObject.optString("BOBOT_SOAL"));
                soalQuizListEssay.add(soalQuizEssay);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Collections.reverse(soalQuizListEssay);
        soalQuiz.setSoalQuizEssayList(soalQuizListEssay);
        return soalQuiz;
    }
}

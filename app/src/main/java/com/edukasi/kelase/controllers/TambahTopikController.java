package com.edukasi.kelase.controllers;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 12/5/15.
 */
public class TambahTopikController extends BaseAPIController {

    protected String idTopik;
    protected String dateCreated;

    public TambahTopikController(Context context, String idUser, String roleId, String id_kelas,  String idSesi,  String idSubSesi, String judulTopik,  String descTopik) {
        super(context);
        addParameter("id_kelas", id_kelas);
        addParameter("id_sesi", idSesi);
        addParameter("id_sub_sesi", idSubSesi);
        addParameter("id_pengguna",roleId);
        addParameter("id_role", idUser);
        addParameter("judul_topik", judulTopik);
        addParameter("deskripsi_topik", descTopik);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("kelas/forum/add_topik");
    }

    public boolean parseSingle(String json) {
        JSONObject resObj;
        try {
            resObj = new JSONObject(json);
            if (resObj.optString("status").equals("success")) {
                idTopik = resObj.optJSONObject("data").optString("ID_TOPIK");
                dateCreated = resObj.optJSONObject("data").optString("DATE_CREATED");
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}

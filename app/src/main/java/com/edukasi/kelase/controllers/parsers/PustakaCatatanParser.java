package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.PustakaCatatan;
import com.edukasi.kelase.models.PustakaCatatanModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaCatatanParser extends BaseParser<PustakaCatatan> {

    private PustakaCatatanModel pustakaCatatanModel;

    public PustakaCatatanParser(Context context) {
        super(context);
        pustakaCatatanModel = new PustakaCatatanModel(context);
    }

    @Override
    public PustakaCatatan parseJsonObject(JSONObject jObj) {
        PustakaCatatan pustakaCatatan = new PustakaCatatan();
        pustakaCatatan.setId(jObj.optString("ID_CATATAN"));
        pustakaCatatan.setJUDUL_CATATAN(jObj.optString("JUDUL_CATATAN"));
        pustakaCatatan.setKONTEN_CATATAN(jObj.optString("KONTEN_CATATAN"));
        pustakaCatatan.setPRIVASI(jObj.optString("PRIVASI"));
        pustakaCatatan.setDATE_CREATED(jObj.optString("DATE_CREATED"));
        pustakaCatatan.setID_PENGGUNA(jObj.optString("ID_PENGGUNA"));
        pustakaCatatan.setTAG(jObj.optString("TAG"));
        pustakaCatatan.setNAMA_DEPAN(jObj.optString("NAMA_DEPAN"));
        pustakaCatatan.setUSERNAME(jObj.optString("USERNAME"));
        pustakaCatatan.setIMAGE(jObj.optString("setIMAGE"));
        pustakaCatatan.setID_ROLE(jObj.optString("ID_ROLE"));
        pustakaCatatanModel.update(pustakaCatatan);
        return pustakaCatatan;
    }

    @Override
    public boolean parse(String json){
        JSONObject resObj;
        try {
            resObj = new JSONObject(json);
            if (resObj.optString(JSON_STATUS).equals("success")) {
                resObj = resObj.optJSONObject(JSON_ARRAY_ROOT);
                JSONArray jsonArr = resObj.optJSONArray("notes");
                result = parseJsonArray(jsonArr);
                return true;
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}

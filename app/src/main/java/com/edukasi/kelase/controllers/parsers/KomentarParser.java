package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.KomentarListitem;

import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 11/22/15.
 */
public class KomentarParser extends BaseParser<KomentarListitem> {

    public KomentarParser(Context context) {
        super(context);
    }

    @Override
    public KomentarListitem parseJsonObject(JSONObject jObj) {
        KomentarListitem komentarListitem = new KomentarListitem();
        komentarListitem.setNAMA_DEPAN(jObj.optString("NAMA_DEPAN").toString());
        komentarListitem.setNAMA_BELAKANG(jObj.optString("NAMA_BELAKANG").toString());
        komentarListitem.setDATE_CREATED(jObj.optString("DATE_CREATED"));
        komentarListitem.setSTATUS(jObj.optString("STATUS"));
        komentarListitem.setIMAGE(jObj.optString("IMAGE"));
        return komentarListitem;
    }
}

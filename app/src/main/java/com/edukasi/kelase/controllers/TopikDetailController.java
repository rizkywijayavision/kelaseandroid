package com.edukasi.kelase.controllers;

import android.content.Context;

import com.edukasi.kelase.controllers.parsers.TopikKomentarParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class TopikDetailController extends BaseAPIController {

    protected TopikKomentarParser topikKomentarParser;

    public TopikDetailController(Context context, String id_kelas, String id_sesi, String id_sub_sesi,
                                 String id_pengguna, String id_role, String id_topik) {
        super(context);
        addParameter("id_kelas", id_kelas);
        addParameter("id_sesi", id_sesi);
        addParameter("id_sub_sesi", id_sub_sesi);
        addParameter("id_pengguna", id_pengguna);
        addParameter("id_role", id_role);
        addParameter( "id_topik", id_topik);
        addParameter("with_komentar_list","true");
    }

    @Override
    public boolean parse(String json) throws JSONException {
        topikKomentarParser = new TopikKomentarParser(context);
        return topikKomentarParser.parseSingle(json);
    }

    @Override
    public void onAPIsuccess() {

    }

    @Override
    public void executeAPI() {
        POST("kelas/forum/detail_topik");
    }
}

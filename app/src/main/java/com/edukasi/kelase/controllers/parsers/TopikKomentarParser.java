package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.TopikDetail;
import com.edukasi.kelase.entities.TopikKomentar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class TopikKomentarParser extends BaseParser<TopikDetail> {

    public TopikKomentarParser(Context context) {
        super(context);
    }

    @Override
    public TopikDetail parseJsonObject(JSONObject jObj) {
        TopikDetail topikDetail = new TopikDetail();
        topikDetail.setJUDUL_TOPIK(jObj.optString("JUDUL_TOPIK"));
        topikDetail.setDESKRIPSI_TOPIK(jObj.optString("DESKRIPSI_TOPIK"));
        topikDetail.setDATE_CREATED(jObj.optString("DATE_CREATED"));
        topikDetail.setNAMA_PENGGUNA(jObj.optString("NAMA_PENGGUNA"));
        topikDetail.setIMAGE_PENGGUNA(jObj.optString("IMAGE_PENGGUNA"));
        topikDetail.setID_PENGGUNA(jObj.optString("ID_PENGGUNA"));
        JSONArray jsonKomentar = jObj.optJSONArray("LIST_KOMENTAR");
        for (int i = 0; i < jsonKomentar.length(); i++){
            TopikKomentar list_komentar = new TopikKomentar();
            try {
                list_komentar.setID_KOMENTAR(jsonKomentar.getJSONObject(i).optString("ID_KOMENTAR"));
                list_komentar.setKOMENTAR(jsonKomentar.getJSONObject(i).optString("KOMENTAR"));
                list_komentar.setDATE_CREATED(jsonKomentar.getJSONObject(i).optString("DATE_CREATED"));
                list_komentar.setID_PENGGUNA(jsonKomentar.getJSONObject(i).optString("ID_PENGGUNA"));
                list_komentar.setNAMA_PENGGUNA(jsonKomentar.getJSONObject(i).optString("NAMA_PENGGUNA"));
                list_komentar.setIMAGE_PENGGUNA(jsonKomentar.getJSONObject(i).optString("IMAGE_PENGGUNA"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            topikDetail.addKomentar(list_komentar);
        }
        return  topikDetail;
    }
}

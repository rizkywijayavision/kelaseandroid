package com.edukasi.kelase.controllers.parsers;

import android.content.Context;

import com.edukasi.kelase.entities.Kelas;
import com.edukasi.kelase.models.KelasModel;

import org.json.JSONObject;

/**
 * Created by ebizu-rizky on 11/24/15.
 */

public class ListKelasParser extends BaseParser<Kelas> {

    private int typeKelas;
    private KelasModel kelasModel;

    public ListKelasParser(Context context, int typeKelas) {
        super(context);
        this.typeKelas = typeKelas;
        kelasModel = new KelasModel(context);
    }

    @Override
    public Kelas parseJsonObject(JSONObject jObj) {
        Kelas item = new Kelas();
        item.setId(jObj.optString("ID_KELAS"));
        item.setNAMA_KELAS(jObj.optString("NAMA_KELAS"));
        item.setINFO_KELAS(jObj.optString("INFO_KELAS"));
        if ( jObj.optString("ICON_KELAS") == "null"){
            item.setICON_KELAS("");
        } else item.setICON_KELAS(jObj.optString("ICON_KELAS"));
        item.setNAMA_GURU_ADMIN(jObj.optString("NAMA_ADMIN_GURU"));
        item.setID_GURU_ADMIN(jObj.optString("ID_GURU_ADMIN"));
        item.setJML_PESERTA(jObj.optString("JUMLAH_PESERTA"));
        item.setSTATUS(jObj.optString("STATUS"));
        item.setID_INSTITUSI(jObj.optString("ID_INSTITUSI"));
        item.setNAMA_INSTITUSI(jObj.optString("NAMA_INSTITUSI"));
        item.setPath_image(jObj.optString("path_image"));
        item.setNull_image(jObj.optString("null_image"));
        item.setJOIN_STATUS(jObj.optString("JOIN_STATUS"));
        item.setCAN_JOIN(jObj.optString("CAN_JOIN"));
        switch (typeKelas){
            case 1 :
                item.setTYPE_KELAS("Kelas Privat");
                break;
            case 2 :
                item.setTYPE_KELAS("Kelas Publik");
                break;
        }
        kelasModel.update(item);
        return item;
    }
}

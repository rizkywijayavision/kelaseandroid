package com.edukasi.kelase.controllers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.edukasi.kelase.controllers.parsers.ListKelasKuParser;
import com.edukasi.kelase.controllers.parsers.ListKelasParser;

import org.json.JSONException;

/**
 * Created by ebizu-rizky on 11/24/15.
 */
public class GetListKelasController extends BaseAPIController {

    protected ListKelasParser listKelasParser;
    protected ListKelasKuParser listKelasKuParser;
    private int typeKelas;

    public GetListKelasController(Context context, String userId, String roleId, int typeKelas) {
        super(context);
        this.typeKelas = typeKelas;
        addParameter("id_pengguna", userId);
        addParameter("id_role", roleId);
    }

    @Override
    public boolean parse(String json) throws JSONException {
        if(typeKelas == 3){
            listKelasKuParser = new ListKelasKuParser(context);
            errorMessage = listKelasKuParser.getParseMessage();
            return listKelasKuParser.parse(json);
        } else {
            listKelasParser = new ListKelasParser(context, typeKelas);
            errorMessage = listKelasParser.getParseMessage();
            return listKelasParser.parse(json);
        }
    }

    @Override
    public void onAPIsuccess() {
        Intent intent;
        switch (typeKelas){
            case 1 :
                intent = new Intent("Kelas Privat");
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                break;
            case 2 :
                intent = new Intent("Kelas Publik");
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                break;
            case 3 :
                intent = new Intent("Kelasku");
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                break;
        }
    }

    @Override
    public void executeAPI() {
        switch (typeKelas){
            case 1 :
                POST("kelas/list");
                break;
            case 2 :
                POST("kelas/list/public");
                break;
            case 3 :
                POST("kelas/list/me");
                break;
        }

    }
}

package com.edukasi.kelase.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.KelasDetailActivity;
import com.edukasi.kelase.adapters.KelasListAdapter;
import com.edukasi.kelase.entities.Kelas;
import com.edukasi.kelase.models.KelasModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu-rizky on 11/28/15.
 */

public class KelasTypeFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    private Context mContext;
    private ListView list;
    private List<Kelas> listKelas;
    private KelasListAdapter kelasAdapter;
    private int typeKelas;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity().getApplicationContext();
        Bundle args = getArguments();
        if (args != null) {
            typeKelas = args.getInt("typeKelas");
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void initView(View view) {
        list = (ListView) view.findViewById(R.id.list_kelas);
        listKelas = new ArrayList<>();
        KelasModel kelasModel = new KelasModel(mContext);
        switch (typeKelas){
            case 1 :
                listKelas.addAll(kelasModel.byType("Kelas Privat"));
                break;
            case 2 :
                listKelas.addAll(kelasModel.byType("Kelas Publik"));
                break;
            case 3 :
                listKelas.addAll(kelasModel.allKelasKu());
                break;
        }
        kelasAdapter = new KelasListAdapter(mContext);
        kelasAdapter.setData(listKelas);
        list.setAdapter(kelasAdapter);

    }

    @Override
    public void setUICallbacks() {
        list.setOnItemClickListener(this);
    }

    @Override
    public void updateUI() {
//        List<Kelas> lisKelasNew = new ArrayList<>();
//        KelasModel kelasModel = new KelasModel(mContext);
//        switch (typeKelas){
//            case 1 :
//                lisKelasNew.addAll(kelasModel.byType("Kelas Privat"));
//                break;
//            case 2 :
//                lisKelasNew.addAll(kelasModel.byType("Kelas Publik"));
//                break;
//            case 3 :
//                lisKelasNew.addAll(kelasModel.allKelasKu());
//                break;
//        }
//        Set<Kelas> fooSet = new LinkedHashSet<>(listKelas);
//        fooSet.addAll(lisKelasNew);
//        listKelas = new ArrayList<>(fooSet);
//        kelasAdapter.notifyDataSetChanged();
    }


    @Override
    public String getPageTitle() {
        return null;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_kelasku;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(mContext, KelasDetailActivity.class);
        intent.putExtra("ID", listKelas.get(i).getId());
        intent.putExtra("TYPE_KELAS", String.valueOf(typeKelas));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
//        switch (typeKelas){
//            case 1 :
//                LocalBroadcastManager.getInstance(mContext).registerReceiver(kelasBroadcast,
//                        new IntentFilter("Kelas Privat"));
//                break;
//            case 2 :
//                LocalBroadcastManager.getInstance(mContext).registerReceiver(kelasBroadcast,
//                        new IntentFilter("Kelas Publik"));
//                break;
//            case 3 :
//                LocalBroadcastManager.getInstance(mContext).registerReceiver(kelasBroadcast,
//                        new IntentFilter("Kelasku"));
//                break;
//        }
    }

    @Override
    public void onPause() {
        super.onPause();
//        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(kelasBroadcast);
    }

//    private BroadcastReceiver kelasBroadcast = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            updateUI();
//        }
//    };
}
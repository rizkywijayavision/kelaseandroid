package com.edukasi.kelase.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.pustaka.CatatanAddActivity;
import com.edukasi.kelase.activities.pustaka.PhotoAddActivity;
import com.edukasi.kelase.activities.pustaka.TautanAddActivity;
import com.edukasi.kelase.activities.pustaka.VideoAddActivity;
import com.edukasi.kelase.libs.parallaxscroll.widget.SlidingTabLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.ksoichiro.android.observablescrollview.CacheFragmentStatePagerAdapter;

/**
 * Created by ebizu-rizky on 12/6/15.
 */
public class PustakaFragment extends BaseFragment{

    private Context mContext;
    private int actionBarHeight;
    private int mBaseTranslationY;
    private View mHeaderView;
    private NavigationAdapter mPagerAdapter;
    private ViewPager mPager;
    private FloatingActionButton postStatus, postImageGallery, postVideo, postLink;
    private FloatingActionMenu menu1;
    private final static int REQUEST_CODE = 111;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity().getApplicationContext();
        TypedValue tv = new TypedValue();
        if (mContext.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void initView(View view) {
        menu1 = (FloatingActionMenu) view.findViewById(R.id.menu1);
        menu1.setMenuButtonShowAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.show_from_bottom));
        menu1.setMenuButtonHideAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.hide_to_bottom));
        menu1.setClosedOnTouchOutside(true);
        postStatus = (FloatingActionButton) view.findViewById(R.id.fab1);
        postImageGallery = (FloatingActionButton) view.findViewById(R.id.fab2);
        postVideo = (FloatingActionButton) view.findViewById(R.id.fab3);
        postLink = (FloatingActionButton) view.findViewById(R.id.fab4);

        mHeaderView = view.findViewById(R.id.header);
        mPagerAdapter = newViewPagerAdapter();
        mPager = (ViewPager) view.findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mPager.setOffscreenPageLimit(3);

        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        slidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.putih));
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(mPager);

        // When the page is selected, other fragments' scrollY should be adjusted
        // according to the toolbar status(shown/hidden)
        slidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

    }

    @Override
    public void setUICallbacks() {
        menu1.setOnMenuButtonClickListener(fabMenuListener);
        postStatus.setOnClickListener(clickListener);
        postImageGallery.setOnClickListener(clickListener);
        postVideo.setOnClickListener(clickListener);
        postLink.setOnClickListener(clickListener);
    }

    private View.OnClickListener fabMenuListener =  new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (menu1.isOpened()) {
            }
            menu1.toggle(true);
        }
    };

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.fab1:
                    menu1.toggle(true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(mContext, CatatanAddActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }, 300);

                    break;
                case R.id.fab2:
                    menu1.toggle(true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(mContext, PhotoAddActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }, 300);

                    break;
                case R.id.fab3:
                    menu1.toggle(true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(mContext, VideoAddActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }, 300);

                    break;
                case R.id.fab4:
                    menu1.toggle(true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(mContext, TautanAddActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }, 300);
                    break;
            }
        }
    };

    @Override
    public void updateUI() {
    }

    @Override
    public String getPageTitle() {
        return null;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_pustaka;
    }

    private Fragment getCurrentFragment() {
        return mPagerAdapter.getItemAt(mPager.getCurrentItem());
    }

    protected NavigationAdapter newViewPagerAdapter() {
        return new NavigationAdapter(getFragmentManager());
    }

    protected static class NavigationAdapter extends CacheFragmentStatePagerAdapter {

        private static final String[] TITLES = new String[]{"Gambar", "Catatan", "Video", "Tautan"};

        private int mScrollY;

        public NavigationAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setScrollY(int scrollY) {
            mScrollY = scrollY;
        }

        protected Fragment newFragment() {
            return new PustakaPhotoFragment();
        }

        @Override
        protected Fragment createItem(int position) {
            Fragment f;
            switch (position) {
                case 0:
                    f = new PustakaPhotoFragment();
                    if (0 <= mScrollY) {
                        Bundle args = new Bundle();
//                        args.putInt("typeKelas", 3);
                        f.setArguments(args);
                    }
                    break;
                case 1:
                    f = new PustakaCatatanFragment();
                    if (0 <= mScrollY) {
                        Bundle args = new Bundle();
//                        args.putInt("typeKelas", 2);
                        f.setArguments(args);
                    }
                    break;
                case 2:
                    f = new PustakaVideoFragment();
                    if (0 <= mScrollY) {
                        Bundle args = new Bundle();
//                        args.putInt("typeKelas", 1);
                        f.setArguments(args);
                    }
                    break;
                case 3:
                    f = new PustakaTautanFragment();
                    if (0 <= mScrollY) {
                        Bundle args = new Bundle();
//                        args.putInt("typeKelas", 1);
                        f.setArguments(args);
                    }
                    break;
                default:
                    f = new PustakaPhotoFragment();
                    if (0 <= mScrollY) {
                        Bundle args = new Bundle();
//                        args.putInt("typeKelas", 3);
                        f.setArguments(args);
                    }
                    break;
            }
            return f;
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }
    }

}

package com.edukasi.kelase.fragments;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.KelasDetailActivity;
import com.edukasi.kelase.activities.subsesiactivities.BacaanActivity;
import com.edukasi.kelase.activities.subsesiactivities.ForumActivity;
import com.edukasi.kelase.activities.subsesiactivities.QuizActivity;
import com.edukasi.kelase.activities.subsesiactivities.TugasActivity;
import com.edukasi.kelase.adapters.SesiListAdapter;
import com.edukasi.kelase.entities.KelasSesi;
import com.edukasi.kelase.entities.KelasSubSesi;
import com.edukasi.kelase.libs.expandablelayout.ExpandableLayoutListener;
import com.edukasi.kelase.libs.expandablelayout.ExpandableRelativeLayout;
import com.edukasi.kelase.libs.expandablelayout.Utils;
import com.edukasi.kelase.libs.parallaxscroll.FlexibleSpaceWithImageBaseFragment;
import com.edukasi.kelase.models.KelasSesiModel;
import com.edukasi.kelase.models.KelasSubSesiModel;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.github.ksoichiro.android.observablescrollview.Scrollable;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu-rizky on 11/30/15.
 */
public class KelasSesiFragment extends FlexibleSpaceWithImageBaseFragment<ObservableScrollView> {

    private String id_kelas, type_kelas;
    private ObservableScrollView scrollView;
    private ListView sesiListView;
    private List<KelasSesi> listSesi;
    private SesiListAdapter sesiListAdapter;
    private LinearLayout parentView;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        context = getContext();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView(View view) {
        scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        scrollView.setTouchInterceptionViewGroup((ViewGroup) view.findViewById(R.id.fragment_root));
        Bundle args = getArguments();
        if (args != null) {
            if(args.containsKey(ARG_SCROLL_Y)){
                final int scrollY = args.getInt(ARG_SCROLL_Y, 0);
                ScrollUtils.addOnGlobalLayoutListener(scrollView, new Runnable() {
                    @Override
                    public void run() {
                        scrollView.scrollTo(0, scrollY);
                    }
                });
                updateFlexibleSpace(scrollY, view);
            }
            id_kelas = args.getString("ID");
            type_kelas = args.getString("TYPE_KELAS");
        } else {
            updateFlexibleSpace(0, view);
        }

        parentView = (LinearLayout) view.findViewById(R.id.parentView);
//        sesiListView = (ListView) view.findViewById(R.id.list_view_sesi);
        listSesi = new ArrayList<>();
        KelasSesiModel kelasSesiModel = new KelasSesiModel(context);
        listSesi.addAll(kelasSesiModel.byIdKelas(id_kelas));
        int i = 0;
        for (KelasSesi kelasSesi : listSesi){
            i++;
            View convertView = LayoutInflater.from(context).inflate(R.layout.adapter_list_sesi, null);
            TextView titleSesi = (TextView) convertView.findViewById(R.id.title_sesi);
            final RelativeLayout expandButton = (RelativeLayout) convertView.findViewById(R.id.expand_button);
            HtmlTextView descSesi = (HtmlTextView) convertView.findViewById(R.id.desc_sesi);
            LinearLayout parentSubSesi = (LinearLayout) convertView.findViewById(R.id.parent_sub_sesi);
            final View sesiTriangle = convertView.findViewById(R.id.sesi_triangle);
            final ExpandableRelativeLayout descExpandLayout = (ExpandableRelativeLayout) convertView.findViewById(R.id.expandableLayout);

            titleSesi.setText(kelasSesi.getJUDUL_SESI());
            descSesi.setHtmlFromString(kelasSesi.getDESKRIPSI_SESI(), new HtmlTextView.RemoteImageGetter());
            descExpandLayout.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
            descExpandLayout.setListener(new ExpandableLayoutListener() {
                @Override
                public void onAnimationStart() {
                }

                @Override
                public void onAnimationEnd() {

                }

                @Override
                public void onPreOpen() {
                    createRotateAnimator(sesiTriangle, 0f, 180f).start();
                }

                @Override
                public void onPreClose() {
                    createRotateAnimator(sesiTriangle, 180f, 0f).start();
                }

                @Override
                public void onOpened() {

                }

                @Override
                public void onClosed() {

                }
            });
            expandButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    if (view == expandButton) {
                        descExpandLayout.toggle();
                    }
                }
            });

            List<KelasSubSesi> listSubSesi = new KelasSubSesiModel(context).byIdSesi(kelasSesi.getID_KELAS(),kelasSesi.getId());
            for (final KelasSubSesi subSesi : listSubSesi){
                Button subSesiButton = new Button(context);
                subSesiButton.setText(subSesi.getJUDUL_SUB_SESI());
                subSesiButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                subSesiButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = null;
                        if (subSesi.getTYPE_SUB_SESI().equals("Bacaan")){
                            i = new Intent(context, BacaanActivity.class);
                        }
                        if (subSesi.getTYPE_SUB_SESI().equals("Tugas")){
                            i = new Intent(context, TugasActivity.class);
                        }
                        if (subSesi.getTYPE_SUB_SESI().equals("Forum Diskusi")){
                            i = new Intent(context, ForumActivity.class);
                        }
                        if (subSesi.getTYPE_SUB_SESI().equals("Quis")){
                            i = new Intent(context, QuizActivity.class);
                        }
                        i.putExtra("ID_KELAS", subSesi.getID_KELAS());
                        i.putExtra("ID_SESI", subSesi.getID_SESI());
                        i.putExtra("ID_SUBSESI", subSesi.getId());
                        startActivity(i);
                    }
                });
                parentSubSesi.addView(subSesiButton);
            }
            parentView.addView(convertView);
        }
    }

    @Override
    public void setUICallbacks() {
        scrollView.setScrollViewCallbacks(this);
    }

    @Override
    public void updateUI() {
    }

    @Override
    public String getPageTitle() {
        return null;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_kelas_sesi;
    }

    @Override
    public void updateFlexibleSpace(int scrollY) {
        // Sometimes scrollable.getCurrentScrollY() and the real scrollY has different values.
        // As a workaround, we should call scrollVerticallyTo() to make sure that they match.
        Scrollable s = getScrollable();
        s.scrollVerticallyTo(scrollY);

        // If scrollable.getCurrentScrollY() and the real scrollY has the same values,
        // calling scrollVerticallyTo() won't invoke scroll (or onScrollChanged()), so we call it here.
        // Calling this twice is not a problem as long as updateFlexibleSpace(int, View) has idempotence.
        updateFlexibleSpace(scrollY, getView());
    }

    @Override
    protected void updateFlexibleSpace(int scrollY, View view) {
        ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);

        // Also pass this event to parent Activity
        KelasDetailActivity parentActivity =
                (KelasDetailActivity) getActivity();
        if (parentActivity != null) {
            parentActivity.onScrollChanged(scrollY, scrollView);
        }
    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }
}

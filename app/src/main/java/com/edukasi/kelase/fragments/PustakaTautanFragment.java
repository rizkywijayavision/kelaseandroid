package com.edukasi.kelase.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.adapters.PustakaTautanAdapter;
import com.edukasi.kelase.controllers.PustakaDeleteController;
import com.edukasi.kelase.entities.PustakaTautan;
import com.edukasi.kelase.models.PustakaTautanModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu-rizky on 12/6/15.
 */
public class PustakaTautanFragment extends BaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener  {

    private Context mContext;
    private ListView list;
    private List<PustakaTautan> pustakaTautanList;
    private PustakaTautanAdapter pustakaTautanAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity().getApplicationContext();
        Bundle args = getArguments();
        if (args != null) {
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void initView(View view) {
        list = (ListView) view.findViewById(R.id.list_pustaka);
        pustakaTautanList = new ArrayList<>();
        PustakaTautanModel pustakaTautanModel = new PustakaTautanModel(mContext);
        pustakaTautanList.addAll(pustakaTautanModel.all());
        pustakaTautanAdapter = new PustakaTautanAdapter(mContext);
        pustakaTautanAdapter.setData(pustakaTautanList);
        list.setAdapter(pustakaTautanAdapter);
    }

    @Override
    public void setUICallbacks() {
        list.setOnItemClickListener(this);
        list.setOnItemLongClickListener(this);
    }

    @Override
    public void updateUI() {

    }


    @Override
    public String getPageTitle() {
        return null;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_pustaka_item;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(pustakaTautanList.get(i).getMETA_LINK()));
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long l) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Apakah anda yakin ingin menghapus pustaka ini ?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                new PustakaDeleteController(getContext(), 3, pustakaTautanList.get(i).getId()){
                    @Override
                    public void onAPIsuccess() {
                        super.onAPIsuccess();
                        PustakaTautanModel pustakaTautanModel = new PustakaTautanModel(context);
                        pustakaTautanModel.delete(pustakaTautanList.get(i));
                        pustakaTautanList.remove(i);
                        pustakaTautanAdapter.notifyDataSetChanged();
                    }
                }.executeAPI();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });



        AlertDialog dialog = builder.create();
        dialog.show();
        return true;
    }
}

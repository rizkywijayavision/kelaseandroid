/**
 * Base Fragment
 * @author egiadtya
 * 27 October 2014
 */
package com.edukasi.kelase.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.BaseActivity;
import com.edukasi.kelase.base.interfaces.FragmentInteface;
import com.edukasi.kelase.helpers.PictureHelper;
import com.nostra13.universalimageloader.core.assist.ImageSize;

public abstract class BaseFragment extends Fragment implements FragmentInteface {
    private View view;
    protected Activity activity;
    protected boolean hasFetchDataFromAPI;
    protected LayoutInflater inflater;
    protected PictureHelper pictureHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pictureHelper = PictureHelper.getInstance(activity, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        view = inflater.inflate(getFragmentLayout(), container, false);
        initView(view);
//        TextHelper.getInstance(activity).setFont((ViewGroup) view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateUI();
        setUICallbacks();
//        if (getBaseActivity() != null)
//            getBaseActivity().setActionBarTitle(getPageTitle());
    }

    @Override
    public void onAttach(Activity activity) {
        this.activity = activity;
        super.onAttach(activity);
    }

    public BaseActivity getBaseActivity() {
        if (activity instanceof BaseActivity) {
            return ((BaseActivity) activity);
        } else {
            return null;
        }
    }

    public void replaceFragment(int container, BaseFragment fragment, boolean addBackToStack) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (addBackToStack) {
            ft.addToBackStack(fragment.getPageTitle());
        }
        //ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(container, fragment, fragment.getPageTitle());
        ft.commit();
    }

    public void addFragment(int container, BaseFragment fragment, boolean addBackToStack) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (addBackToStack) {
            ft.addToBackStack(fragment.getPageTitle());
        }
        ft.add(container, fragment);
        ft.commit();
    }

    public BaseFragment findFragment(String tag) {
        Fragment fragment = getFragmentManager().findFragmentByTag(tag);
        if (fragment != null) {
            return (BaseFragment) fragment;
        }
        return null;
    }

    public void addChildFragment(int container, BaseFragment fragment, boolean addBackToStack) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        if (addBackToStack) {
            ft.addToBackStack(fragment.getPageTitle());
        }
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.add(container, fragment);
        ft.commitAllowingStateLoss();
    }

    public void loadImage(String url, ImageView imageView, int stubImage) {
        pictureHelper.loadImage(url, imageView, stubImage, null);
    }

    public void loadImage(String url, ImageView imageView, int stubImage, ImageSize imageSize) {
        pictureHelper.loadImage(url, imageView, stubImage, imageSize);
    }

    public void displayImage(String url, ImageView view){
        pictureHelper.displayImage(url,view);
    }

    public String checkNullString(String string) {
        return (string == null) ? "" : string;
    }

    @Override
    public void onStart() {
        super.onStart();
//        if (getPageTitle() != null)
//            GoogleAnalyticsHelper.screenTracker(activity, getPageTitle());
    }
}

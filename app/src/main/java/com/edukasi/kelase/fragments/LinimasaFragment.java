package com.edukasi.kelase.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.PostImageActivity;
import com.edukasi.kelase.activities.PostStatusActivity;
import com.edukasi.kelase.adapters.LinimasaListAdapter;
import com.edukasi.kelase.controllers.LinimasaController;
import com.edukasi.kelase.entities.KelaseUtil;
import com.edukasi.kelase.entities.MomentListitem;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

public class LinimasaFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    private Context mContext;
    private FloatingActionButton postStatus, postImageGallery;
    private FloatingActionMenu menu1;
    private ListView list;
    private ProgressBar linimasaProgressbar;
    private SwipeRefreshLayout linimasaPullRefresh;
    private int mPreviousVisibleItem;
    private LinimasaListAdapter liniAdapter;
    private List<MomentListitem> MomentList;
    private LinimasaController linimasaController;
    private int offsetItemLinimasa;
    private int firstID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity().getApplicationContext();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void initView(View view) {
        menu1 = (FloatingActionMenu) view.findViewById(R.id.menu1);
        menu1.setMenuButtonShowAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.show_from_bottom));
        menu1.setMenuButtonHideAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.hide_to_bottom));
        menu1.setClosedOnTouchOutside(true);
        postStatus = (FloatingActionButton) view.findViewById(R.id.fab3);
        postImageGallery = (FloatingActionButton) view.findViewById(R.id.fab4);

        list = (ListView) view.findViewById(R.id.list_moment);
        View headerView = inflater.inflate(R.layout.header_linimasa_listview,null);
        ImageView imageCover = (ImageView) headerView.findViewById(R.id.header_linimasa_listview_img);
        displayImage(KelaseUtil.KELASE_SERVICE_IMAGE_URL + App.session.getProfileData().cover_image, imageCover);
        list.addHeaderView(headerView);

        MomentList = new ArrayList<>();
        liniAdapter = new LinimasaListAdapter(activity);
        liniAdapter.setData(MomentList);

        linimasaProgressbar = (ProgressBar) view.findViewById(R.id.linimasa_progressbar);
        linimasaProgressbar.setVisibility(View.VISIBLE);
        linimasaPullRefresh = (SwipeRefreshLayout) view.findViewById(R.id.linimasa_pullrefresh);
    }

    @Override
    public void setUICallbacks() {
        menu1.setOnMenuButtonClickListener(fabMenuListener);
        postStatus.setOnClickListener(clickListener);
        postImageGallery.setOnClickListener(clickListener);
        linimasaPullRefresh.setOnRefreshListener(this);
        list.setOnScrollListener(absListViewScrollListener);
    }

    @Override
    public void updateUI() {
        list.setAdapter(liniAdapter);
        getDataLinimasa();
    }

    @Override
    public String getPageTitle() {
        return null;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_linimasa;
    }

    public void getDataLinimasa(){
        linimasaController = new LinimasaController(mContext, App.session.getUserDetails().getId(), "10", "0"){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                offsetItemLinimasa = linimasaParser.getResult().size();
                MomentList.addAll(linimasaParser.getResult());
                liniAdapter.notifyDataSetChanged();
                linimasaProgressbar.setVisibility(View.GONE);
                firstID = Integer.valueOf(MomentList.get(0).getID_STATUS_PENGGUNA());
            }

            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                linimasaProgressbar.setVisibility(View.GONE);
            }
        };
        linimasaController.executeAPI();
    }

    private View.OnClickListener fabMenuListener =  new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        if (menu1.isOpened()) {
        }
        menu1.toggle(true);
        }
    };

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fab3:
                menu1.toggle(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent statusIntent = new Intent(mContext, PostStatusActivity.class);
                        startActivity(statusIntent);
                    }
                }, 300);

                break;
            case R.id.fab4:
                menu1.toggle(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent photoIntent = new Intent(mContext, PostImageActivity.class);
                        photoIntent.putExtra("type", "gallery");
                        startActivity(photoIntent);
                    }
                }, 300);
                break;
        }
        }
    };


    @Override
    public void onRefresh() {
        linimasaController = new LinimasaController(mContext, App.session.getUserDetails().getId(), "10", "0"){
            @Override
            public void onAPIsuccess() {
                super.onAPIsuccess();
                for (MomentListitem  momentListitem : linimasaParser.getResult()){
                   if (Integer.valueOf(momentListitem.getID_STATUS_PENGGUNA()) > firstID){
                       MomentList.add(0, momentListitem);
                       offsetItemLinimasa =+ 1;
                       liniAdapter.notifyDataSetChanged();
                       firstID = Integer.valueOf(momentListitem.getID_STATUS_PENGGUNA());
                   }
                }
                App.log("size arraylist : " + MomentList.size());
                linimasaPullRefresh.setRefreshing(false);
            }

            @Override
            public void onAPIFailed(String errorMessage) {
                super.onAPIFailed(errorMessage);
                linimasaPullRefresh.setRefreshing(false);
            }
        };
        linimasaController.executeAPI();
    }

    public AbsListView.OnScrollListener absListViewScrollListener =  new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int topRowVerticalPosition = (list == null || list.getChildCount() == 0) ? 0 : list.getChildAt(0).getTop();
            if (firstVisibleItem > mPreviousVisibleItem) {
                menu1.hideMenu(true);
            } else if (firstVisibleItem < mPreviousVisibleItem) {
                menu1.showMenu(true);
            }
            mPreviousVisibleItem = firstVisibleItem;

            linimasaPullRefresh.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            if (firstVisibleItem + 2 > MomentList.size() && MomentList.size() > 0){

                linimasaController = new LinimasaController(mContext, App.session.getUserDetails().getId(), "10", String.valueOf(offsetItemLinimasa)){
                    @Override
                    public void onAPIsuccess() {
                        super.onAPIsuccess();
                        offsetItemLinimasa =+ linimasaParser.getResult().size();
                        MomentList.addAll(linimasaParser.getResult());
                        liniAdapter.notifyDataSetChanged();
                    }
                };
                linimasaController.executeAPI();
            }
        }
    };
}

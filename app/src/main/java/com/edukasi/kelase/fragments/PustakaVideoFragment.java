package com.edukasi.kelase.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.adapters.PustakaVideoAdapter;
import com.edukasi.kelase.controllers.PustakaDeleteController;
import com.edukasi.kelase.entities.PustakaVideo;
import com.edukasi.kelase.libs.youtube.YouTubeActivity;
import com.edukasi.kelase.models.PustakaVideoModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu-rizky on 12/6/15.
 */

public class PustakaVideoFragment extends BaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener  {

    private Context mContext;
    private ListView list;
    private List<PustakaVideo> pustakaVideoList;
    private PustakaVideoAdapter pustakaVideoAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity().getApplicationContext();
        Bundle args = getArguments();
        if (args != null) {
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void initView(View view) {
        list = (ListView) view.findViewById(R.id.list_pustaka);
        pustakaVideoList = new ArrayList<>();
        PustakaVideoModel pustakaVideoModel = new PustakaVideoModel(mContext);
        pustakaVideoList.addAll(pustakaVideoModel.all());
        pustakaVideoAdapter = new PustakaVideoAdapter(mContext);
        pustakaVideoAdapter.setData(pustakaVideoList);
        list.setAdapter(pustakaVideoAdapter);
    }

    @Override
    public void setUICallbacks() {
        list.setOnItemClickListener(this);
        list.setOnItemLongClickListener(this);
    }

    @Override
    public void updateUI() {

    }


    @Override
    public String getPageTitle() {
        return null;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_pustaka_item;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent actIntent = new Intent(mContext, YouTubeActivity.class);
        actIntent.putExtra(YouTubeActivity.KEY_VIDEO_ID, pustakaVideoList.get(i).getLINK_VIDEO());
        startActivity(actIntent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long l) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Apakah anda yakin ingin menghapus pustaka ini ?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                new PustakaDeleteController(getContext(), 2, pustakaVideoList.get(i).getId()){
                    @Override
                    public void onAPIsuccess() {
                        super.onAPIsuccess();
                        PustakaVideoModel pustakaVideoModel = new PustakaVideoModel(context);
                        pustakaVideoModel.delete(pustakaVideoList.get(i));
                        pustakaVideoList.remove(i);
                        pustakaVideoAdapter.notifyDataSetChanged();
                    }
                }.executeAPI();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });



        AlertDialog dialog = builder.create();
        dialog.show();
        return true;
    }
}
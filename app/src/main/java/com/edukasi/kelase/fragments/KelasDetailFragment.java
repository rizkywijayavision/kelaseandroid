package com.edukasi.kelase.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.KelasDetailActivity;
import com.edukasi.kelase.entities.Kelas;
import com.edukasi.kelase.libs.parallaxscroll.FlexibleSpaceWithImageBaseFragment;
import com.edukasi.kelase.models.KelasModel;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.github.ksoichiro.android.observablescrollview.Scrollable;

import org.sufficientlysecure.htmltextview.HtmlTextView;

/**
 * Created by ebizu-rizky on 11/28/15.
 */

public class KelasDetailFragment extends FlexibleSpaceWithImageBaseFragment<ObservableScrollView> {

    private TextView titleKelas;
    private Kelas kelasDetail;
    private ObservableScrollView scrollView;
    private RelativeLayout descExpandButton;
    private HtmlTextView descText;
    private String id_kelas, type_kelas;
    private Context mContext;
    private TextView namaPengajar, jumlahPeserta, namaInstitusi, statusKelas;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mContext = getContext();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView(View view) {
        scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        scrollView.setTouchInterceptionViewGroup((ViewGroup) view.findViewById(R.id.fragment_root));
        Bundle args = getArguments();
        if (args != null) {
            if(args.containsKey(ARG_SCROLL_Y)){
                final int scrollY = args.getInt(ARG_SCROLL_Y, 0);
                ScrollUtils.addOnGlobalLayoutListener(scrollView, new Runnable() {
                    @Override
                    public void run() {
                        scrollView.scrollTo(0, scrollY);
                    }
                });
                updateFlexibleSpace(scrollY, view);
            }
            id_kelas = args.getString("ID");
            type_kelas = args.getString("TYPE_KELAS");
        } else {
            updateFlexibleSpace(0, view);
        }

        KelasModel kelasModel = new KelasModel(mContext);
        kelasDetail = (Kelas) kelasModel.find(id_kelas);

        titleKelas = (TextView) view.findViewById(R.id.title_kelas);
        titleKelas.setText(kelasDetail.getNAMA_KELAS());
        namaPengajar = (TextView) view.findViewById(R.id.nama_pengajar);
        namaPengajar.setText(kelasDetail.getNAMA_GURU_ADMIN());
        jumlahPeserta = (TextView) view.findViewById(R.id.jumlah_peserta);
        jumlahPeserta.setText(kelasDetail.getJML_PESERTA());
        namaInstitusi = (TextView) view.findViewById(R.id.nama_institusi);
        namaInstitusi.setText(kelasDetail.getNAMA_INSTITUSI());
        statusKelas = (TextView) view.findViewById(R.id.status_kelas);
        if (kelasDetail.getSTATUS().equals("0")){
            statusKelas.setText("Open");
        } else statusKelas.setText("Closed");
        descText = (HtmlTextView) view.findViewById(R.id.html_text);
        descText.setHtmlFromString(kelasDetail.getINFO_KELAS(), new HtmlTextView.RemoteImageGetter());
    }

    @Override
    public void setUICallbacks() {
        scrollView.setScrollViewCallbacks(this);
    }

    @Override
    public void updateUI() {
//        descExpandLayout.toggle();
    }

    @Override
    public String getPageTitle() {
        return null;
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_kelas_detail;
    }

    @Override
    public void updateFlexibleSpace(int scrollY) {
        // Sometimes scrollable.getCurrentScrollY() and the real scrollY has different values.
        // As a workaround, we should call scrollVerticallyTo() to make sure that they match.
        Scrollable s = getScrollable();
        s.scrollVerticallyTo(scrollY);

        // If scrollable.getCurrentScrollY() and the real scrollY has the same values,
        // calling scrollVerticallyTo() won't invoke scroll (or onScrollChanged()), so we call it here.
        // Calling this twice is not a problem as long as updateFlexibleSpace(int, View) has idempotence.
        updateFlexibleSpace(scrollY, getView());
    }

    @Override
    protected void updateFlexibleSpace(int scrollY, View view) {
        ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);

        // Also pass this event to parent Activity
        KelasDetailActivity parentActivity =
                (KelasDetailActivity) getActivity();
        if (parentActivity != null) {
            parentActivity.onScrollChanged(scrollY, scrollView);
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
//            switch (view.getId()) {
//                case R.id.detail_button:
//                    descExpandLayout.toggle();
//                    break;
//            }
        }
    };

}

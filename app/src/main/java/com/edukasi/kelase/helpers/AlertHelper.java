/**
 * @author egiadtya
 * 27 October 2014
 */
package com.edukasi.kelase.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.widget.Toast;

import com.edukasi.kelase.activities.BaseActivity;

public class AlertHelper {

	private static AlertHelper alertHelper;
	
	public static AlertHelper getInstance(){
		if (alertHelper == null) {
			alertHelper = new AlertHelper();
		}
		
		return alertHelper;
	}
	
	public void showAlert(Context context, String message){
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	
	public void showAlertDialog(Context context, String title, String message) {
		new AlertDialog.Builder(context).setTitle(title).setMessage(message).setPositiveButton("Ok", new OnClickListener() {
			@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			}).show();
	}

	public AlertDialog.Builder showAlertWithoutListener(Context context,String title, String message) {
		AlertDialog.Builder adb = new AlertDialog.Builder(context);
		adb.setTitle(title);
		adb.setMessage(message);
		return adb;
	}
	
}

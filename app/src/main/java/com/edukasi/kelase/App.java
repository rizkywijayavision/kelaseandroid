package com.edukasi.kelase;

import android.app.Application;
import android.util.Log;

import com.edukasi.kelase.sessions.KelaseSessionManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by ebizu-rizky on 11/4/15.
 */

public class App extends Application {

    public static KelaseSessionManager session;

    @Override
    public void onCreate() {
        super.onCreate();
        // Create global configuration and initialize ImageLoader with this configuration
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCacheSize(2 * 1024 * 1024)
                .build();
        ImageLoader.getInstance().init(config);

        Config.setMode(Config.MODE.DEVELOPMENT);
    }

    public static void log(String message) {
        if (Config.isDevelopment) {
            Log.i(Config.APP_NAME, message);
        }
    }

    public static void log(String message, String type) {
        if (Config.isDevelopment) {
            switch (type) {
                case "i":
                    Log.i(Config.APP_NAME, message);
                    break;
                case "e":
                    Log.i(Config.APP_NAME, message);
                    break;
                case "d":
                    Log.i(Config.APP_NAME, message);
                    break;
                default:
                    break;
            }
            Log.i(Config.APP_NAME, message);
        }
    }
}

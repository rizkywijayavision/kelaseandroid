package com.edukasi.kelase.sessions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.edukasi.kelase.App;
import com.edukasi.kelase.activities.SplashActivity;
import com.edukasi.kelase.entities.ProfileData;


public class KelaseSessionManager {


	// Shared Preferences
	SharedPreferences pref;

	// Editor for Shared preferences
	Editor editor;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "KelasePref";

	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";

	private static final String KEY_USERID = "kelaseid";
	private static final String KEY_USERNAME = "username";
	private static final String KEY_ROLE_ID = "roleid";
	private static final String KEY_NAME = "name";

	private static final String KEY_PROFILE_NAMADEPAN = "namadepan";
	private static final String KEY_PROFILE_NAMABELAKANG = "namabelakang";
	private static final String KEY_PROFILE_ALAMAT = "alamat";
	private static final String KEY_PROFILE_NEGARA = "idnegara";
	private static final String KEY_PROFILE_PROVINSI = "idprovinsi";
	private static final String KEY_PROFILE_NAMANEGARA = "namanegara";
	private static final String KEY_PROFILE_NAMAPROVINSI = "namaprovinsi";
	private static final String KEY_PROFILE_TEMPATLAHIR = "tempatlahir";
	private static final String KEY_PROFILE_TANGGALLAHIR = "tanggallahir";
	private static final String KEY_PROFILE_TELEPON = "telepon";
	private static final String KEY_PROFILE_FACEBOOK = "facebook";
	private static final String KEY_PROFILE_TWITTER = "twitter";
	private static final String KEY_PROFILE_IMAGE = "propic";
	private static final String KEY_PROFILE_COVERIMAGE = "coverpic";
	private static final String PREFS_SYS_FIRST_RUN = "_FIRSTRUN_";
	private static final String PREF_DRAWER_HOMEACTIVITY = "drawer_state_homeactivity";
	private static final String PREF_TAB_STATE_KELAS_FRAGMENT = "tab_state_kelas_fragment";
	private static final String PREF_TAB_STATE_PUSTAKA_FRAGMENT = "tab_state_pustaka_fragment";

	// Constructor
	@SuppressLint("CommitPrefEdits")
	public KelaseSessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	/**
	 * Create login session
	 * */
	public void createLoginSession(LoginSession session) {
		// Storing login value as TRUE
		editor.putBoolean(IS_LOGIN, true);
		editor.putString(KEY_USERNAME, session.getUsername());
		editor.putString(KEY_ROLE_ID, session.getRole_id());
		editor.putString(KEY_USERID, session.getId());
		editor.putString(KEY_NAME, session.getName());
		editor.putString(KEY_PROFILE_NAMADEPAN, "");
		editor.putString(KEY_PROFILE_NAMABELAKANG, "");
		editor.putString(KEY_PROFILE_ALAMAT, "");
		editor.putString(KEY_PROFILE_NEGARA, "");
		editor.putString(KEY_PROFILE_PROVINSI, "");
		editor.putString(KEY_PROFILE_NAMANEGARA, "");
		editor.putString(KEY_PROFILE_NAMAPROVINSI, "");
		editor.putString(KEY_PROFILE_TEMPATLAHIR, "");
		editor.putString(KEY_PROFILE_TANGGALLAHIR, "");
		editor.putString(KEY_PROFILE_TELEPON, "");
		editor.putString(KEY_PROFILE_FACEBOOK, "");
		editor.putString(KEY_PROFILE_TWITTER, "");
		editor.putString(KEY_PROFILE_IMAGE, "");
		editor.putString(KEY_PROFILE_COVERIMAGE, "");
		// commit changes
		editor.commit();

	}

	public void saveProfileSession(ProfileData profileData) {
		// Storing login value as TRUE
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
		editor.putBoolean(IS_LOGIN, true);
		editor.putString(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
		editor.putString(KEY_ROLE_ID, pref.getString(KEY_ROLE_ID, null));
		editor.putString(KEY_USERID, pref.getString(KEY_USERID, null));
		editor.putString(KEY_NAME, pref.getString(KEY_NAME, null));
		editor.putString(KEY_PROFILE_NAMADEPAN, profileData.nama_depan);
		editor.putString(KEY_PROFILE_NAMABELAKANG, profileData.nama_belakang);
		editor.putString(KEY_PROFILE_ALAMAT, profileData.alamat);
		editor.putString(KEY_PROFILE_NEGARA, profileData.negara);
		editor.putString(KEY_PROFILE_PROVINSI, profileData.provinsi);
		editor.putString(KEY_PROFILE_NAMANEGARA, profileData.nama_negara);
		editor.putString(KEY_PROFILE_NAMAPROVINSI, profileData.nama_provinsi);
		editor.putString(KEY_PROFILE_TEMPATLAHIR, profileData.tempat_lahir);
		editor.putString(KEY_PROFILE_TANGGALLAHIR, profileData.tanggal_lahir);
		editor.putString(KEY_PROFILE_TELEPON, profileData.telepon);
		editor.putString(KEY_PROFILE_FACEBOOK, profileData.facebook);
		editor.putString(KEY_PROFILE_TWITTER, profileData.twitter);
		editor.putString(KEY_PROFILE_IMAGE, profileData.profile_image);
		editor.putString(KEY_PROFILE_COVERIMAGE, profileData.cover_image);
		// commit changes
		editor.commit();
	}

	public void saveStateDrawerHomeActivity(int state){
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
		editor.putInt(PREF_DRAWER_HOMEACTIVITY, state);
		editor.commit();
	}
	public int getDrawerStateHomeActivity(){
		return pref.getInt(PREF_DRAWER_HOMEACTIVITY, 1);
	}

	public void saveTabStateKelasFragment(int state){
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
		editor.putInt(PREF_TAB_STATE_KELAS_FRAGMENT, state);
		editor.commit();
	}

	public int getStateTabKelasFragment(){
		return pref.getInt(PREF_TAB_STATE_KELAS_FRAGMENT, 0);
	}

	public void saveTabStatePustakaFragment(int state){
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
		editor.putInt(PREF_TAB_STATE_PUSTAKA_FRAGMENT, state);
		editor.commit();
	}

	public int getStateTabPustakaFragment(){
		return pref.getInt(PREF_TAB_STATE_PUSTAKA_FRAGMENT, 0);
	}
	/**
	 * Check login method wil check user login status If false it will redirect
	 * user to login page Else won't do anything
	 * */
	public boolean checkLogin() {
//		 Check login status
		if (this.isLoggedIn()) {
			// user is not logged in redirect him to Login Activity
			App.session = this;
			return true;
		}
		return false;
	}

	public boolean firstOpen(){
		return pref.getBoolean(PREFS_SYS_FIRST_RUN, true);
	}

	public void setFirstOpen(){
		editor.putBoolean(PREFS_SYS_FIRST_RUN, false);
		editor.commit();
	}

	/**
	 * Get stored session data
	 * */
	public LoginSession getUserDetails() {
		LoginSession user = new LoginSession(
				pref.getString(KEY_USERNAME, null), pref.getString(KEY_USERID,
						null), pref.getString(KEY_NAME, null), pref.getString(
						KEY_ROLE_ID, null));

		return user;
	}

	public ProfileData getProfileData() {
		ProfileData profileData = new ProfileData(pref.getString(
				KEY_PROFILE_NAMADEPAN, null), pref.getString(
				KEY_PROFILE_NAMABELAKANG, null), pref.getString(
				KEY_PROFILE_ALAMAT, null), pref.getString(KEY_PROFILE_NEGARA,
				null), pref.getString(KEY_PROFILE_PROVINSI, null),
				pref.getString(KEY_PROFILE_NAMANEGARA, null), pref.getString(
						KEY_PROFILE_NAMAPROVINSI, null), pref.getString(
						KEY_PROFILE_TEMPATLAHIR, null), pref.getString(
						KEY_PROFILE_TANGGALLAHIR, null), pref.getString(
						KEY_PROFILE_TELEPON, null), pref.getString(
						KEY_PROFILE_FACEBOOK, null), pref.getString(
						KEY_PROFILE_TWITTER, null), pref.getString(
						KEY_PROFILE_IMAGE, null), pref.getString(
						KEY_PROFILE_COVERIMAGE, null));
		return profileData;
	}

	/**
	 * Clear session details
	 * */
	public void logoutUser() {
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();

		// After logout redirect user to Login Activity
		Intent i = new Intent(_context, SplashActivity.class);
		// Closing all the Activities
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		// Add new Flag to start new Activity
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);

		// Staring Login Activity
		_context.startActivity(i);
	}

	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn() {
		return pref.getBoolean(IS_LOGIN, false);
	}
}

package com.edukasi.kelase.sessions;

public class LoginSession {

	public String username;
	public String id;
	public String name;
	public String role_id;

	public LoginSession() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoginSession(String username, String id, String name, String role_id) {
		super();
		this.username = username;
		this.id = id;
		this.name = name;
		this.role_id = role_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

}

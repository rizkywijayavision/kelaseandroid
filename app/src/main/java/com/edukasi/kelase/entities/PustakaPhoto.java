package com.edukasi.kelase.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaPhoto extends BaseEntity {

    @DatabaseField(columnName = "NAMA_FOTO")
    private String NAMA_FOTO;
    @DatabaseField(columnName = "FILE_FOTO")
    private String FILE_FOTO;
    @DatabaseField(columnName = "DATE_CREATED")
    private String DATE_CREATED;
    @DatabaseField(columnName = "ID_ALBUM_FOTO")
    private String ID_ALBUM_FOTO;
    @DatabaseField(columnName = "ID_PENGGUNA")
    private String ID_PENGGUNA;
    @DatabaseField(columnName = "PATH")
    private String PATH;

    public String getNAMA_FOTO() {
        return NAMA_FOTO;
    }

    public void setNAMA_FOTO(String NAMA_FOTO) {
        this.NAMA_FOTO = NAMA_FOTO;
    }

    public String getFILE_FOTO() {
        return FILE_FOTO;
    }

    public void setFILE_FOTO(String FILE_FOTO) {
        this.FILE_FOTO = FILE_FOTO;
    }

    public String getDATE_CREATED() {
        return DATE_CREATED;
    }

    public void setDATE_CREATED(String DATE_CREATED) {
        this.DATE_CREATED = DATE_CREATED;
    }

    public String getID_ALBUM_FOTO() {
        return ID_ALBUM_FOTO;
    }

    public void setID_ALBUM_FOTO(String ID_ALBUM_FOTO) {
        this.ID_ALBUM_FOTO = ID_ALBUM_FOTO;
    }

    public String getID_PENGGUNA() {
        return ID_PENGGUNA;
    }

    public void setID_PENGGUNA(String ID_PENGGUNA) {
        this.ID_PENGGUNA = ID_PENGGUNA;
    }

    public String getPATH() {
        return PATH;
    }

    public void setPATH(String PATH) {
        this.PATH = PATH;
    }
}

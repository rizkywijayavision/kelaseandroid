package com.edukasi.kelase.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaCatatan extends BaseEntity {

    @DatabaseField(columnName = "JUDUL_CATATAN")
    private String JUDUL_CATATAN;
    @DatabaseField(columnName = "KONTEN_CATATAN")
    private String KONTEN_CATATAN;
    @DatabaseField(columnName = "PRIVASI")
    private String PRIVASI;
    @DatabaseField(columnName = "DATE_CREATED")
    private String DATE_CREATED;
    @DatabaseField(columnName = "ID_PENGGUNA")
    private String ID_PENGGUNA;
    @DatabaseField(columnName = "TAG")
    private String TAG;
    @DatabaseField(columnName = "NAMA_DEPAN")
    private String NAMA_DEPAN;
    @DatabaseField(columnName = "USERNAME")
    private String USERNAME;
    @DatabaseField(columnName = "IMAGE")
    private String IMAGE;
    @DatabaseField(columnName = "ID_ROLE")
    private String ID_ROLE;

    public String getJUDUL_CATATAN() {
        return JUDUL_CATATAN;
    }

    public void setJUDUL_CATATAN(String JUDUL_CATATAN) {
        this.JUDUL_CATATAN = JUDUL_CATATAN;
    }

    public String getKONTEN_CATATAN() {
        return KONTEN_CATATAN;
    }

    public void setKONTEN_CATATAN(String KONTEN_CATATAN) {
        this.KONTEN_CATATAN = KONTEN_CATATAN;
    }

    public String getPRIVASI() {
        return PRIVASI;
    }

    public void setPRIVASI(String PRIVASI) {
        this.PRIVASI = PRIVASI;
    }

    public String getDATE_CREATED() {
        return DATE_CREATED;
    }

    public void setDATE_CREATED(String DATE_CREATED) {
        this.DATE_CREATED = DATE_CREATED;
    }

    public String getID_PENGGUNA() {
        return ID_PENGGUNA;
    }

    public void setID_PENGGUNA(String ID_PENGGUNA) {
        this.ID_PENGGUNA = ID_PENGGUNA;
    }

    public String getTAG() {
        return TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    public String getNAMA_DEPAN() {
        return NAMA_DEPAN;
    }

    public void setNAMA_DEPAN(String NAMA_DEPAN) {
        this.NAMA_DEPAN = NAMA_DEPAN;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public String getID_ROLE() {
        return ID_ROLE;
    }

    public void setID_ROLE(String ID_ROLE) {
        this.ID_ROLE = ID_ROLE;
    }
}

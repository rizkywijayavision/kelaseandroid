package com.edukasi.kelase.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ebizu-rizky on 11/29/15.
 */

public class Kelas extends BaseEntity {

    @DatabaseField(columnName = "NAMA_KELAS")
    private String NAMA_KELAS;
    @DatabaseField(columnName = "TYPE_KELAS")
    private String TYPE_KELAS;
    @DatabaseField(columnName = "INFO_KELAS")
    private String INFO_KELAS;
    @DatabaseField(columnName = "ICON_KELAS")
    private String ICON_KELAS;
    @DatabaseField(columnName = "NAMA_GURU_ADMIN")
    private String NAMA_GURU_ADMIN;
    @DatabaseField(columnName = "ID_GURU_ADMIN")
    private String ID_GURU_ADMIN;
    @DatabaseField(columnName = "ID_PENGGUNA")
    private String ID_PENGGUNA;
    @DatabaseField(columnName = "JML_PESERTA")
    private String JML_PESERTA;
    @DatabaseField(columnName = "STATUS")
    private String STATUS;
    @DatabaseField(columnName = "ID_INSTITUSI")
    private String ID_INSTITUSI;
    @DatabaseField(columnName = "NAMA_INSTITUSI")
    private String NAMA_INSTITUSI;
    @DatabaseField(columnName = "path_image")
    private String path_image;
    @DatabaseField(columnName = "null_image")
    private String null_image;
    @DatabaseField(columnName = "JOIN_STATUS")
    private String JOIN_STATUS;
    @DatabaseField(columnName = "CAN_JOIN")
    private String CAN_JOIN;
    @DatabaseField(columnName = "COVER")
    private String COVER;

    public String getNAMA_KELAS() {
        return NAMA_KELAS;
    }

    public void setNAMA_KELAS(String NAMA_KELAS) {
        this.NAMA_KELAS = NAMA_KELAS;
    }

    public String getINFO_KELAS() {
        return INFO_KELAS;
    }

    public void setINFO_KELAS(String INFO_KELAS) {
        this.INFO_KELAS = INFO_KELAS;
    }

    public String getICON_KELAS() {
        return ICON_KELAS;
    }

    public void setICON_KELAS(String ICON_KELAS) {
        this.ICON_KELAS = ICON_KELAS;
    }

    public String getNAMA_GURU_ADMIN() {
        return NAMA_GURU_ADMIN;
    }

    public void setNAMA_GURU_ADMIN(String NAMA_GURU_ADMIN) {
        this.NAMA_GURU_ADMIN = NAMA_GURU_ADMIN;
    }

    public String getID_GURU_ADMIN() {
        return ID_GURU_ADMIN;
    }

    public void setID_GURU_ADMIN(String ID_GURU_ADMIN) {
        this.ID_GURU_ADMIN = ID_GURU_ADMIN;
    }

    public String getID_PENGGUNA() {
        return ID_PENGGUNA;
    }

    public void setID_PENGGUNA(String ID_PENGGUNA) {
        this.ID_PENGGUNA = ID_PENGGUNA;
    }

    public String getJML_PESERTA() {
        return JML_PESERTA;
    }

    public void setJML_PESERTA(String JML_PESERTA) {
        this.JML_PESERTA = JML_PESERTA;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getID_INSTITUSI() {
        return ID_INSTITUSI;
    }

    public void setID_INSTITUSI(String ID_INSTITUSI) {
        this.ID_INSTITUSI = ID_INSTITUSI;
    }

    public String getNAMA_INSTITUSI() {
        return NAMA_INSTITUSI;
    }

    public void setNAMA_INSTITUSI(String NAMA_INSTITUSI) {
        this.NAMA_INSTITUSI = NAMA_INSTITUSI;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getNull_image() {
        return null_image;
    }

    public void setNull_image(String null_image) {
        this.null_image = null_image;
    }

    public String getJOIN_STATUS() {
        return JOIN_STATUS;
    }

    public void setJOIN_STATUS(String JOIN_STATUS) {
        this.JOIN_STATUS = JOIN_STATUS;
    }

    public String getCAN_JOIN() {
        return CAN_JOIN;
    }

    public void setCAN_JOIN(String CAN_JOIN) {
        this.CAN_JOIN = CAN_JOIN;
    }

    public String getCOVER() {
        return COVER;
    }

    public void setCOVER(String COVER) {
        this.COVER = COVER;
    }

    public String getTYPE_KELAS() {
        return TYPE_KELAS;
    }

    public void setTYPE_KELAS(String TYPE_KELAS) {
        this.TYPE_KELAS = TYPE_KELAS;
    }
}

package com.edukasi.kelase.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ebizu-rizky on 11/29/15.
 */
public class KelasKu extends BaseEntity {

    @DatabaseField(columnName = "ROLE_ID")
    String role_id;

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }
}

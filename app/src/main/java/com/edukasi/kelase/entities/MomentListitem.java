package com.edukasi.kelase.entities;

/**
 * 
 * @author rezkyatinnov
 *
 */
public class MomentListitem {

	public String ID_STATUS_PENGGUNA;
	public String ID_PENGGUNA;
	public String STATUS;
	public String DELETED;
	public String META_TITLE;
	public String META_DESCRIPTION;
	public String META_IMAGE;
	public String DATE_CREATED;
	public String COMMENT_ON_STATUS_ID;
	public String META_URL;
	public String IMAGE;
	public String IMAGE_STATUS;
	public String USERNAME;
	public String NAMA_BELAKANG;
	public String NAMA_DEPAN;
	public String LINK;
	public String LINK_NAME;
	public String count_favorit;
	public String is_rate;
	public String role;
	public String TYPE_STATUS;
	public String COUNT_COMMENT;

	public MomentListitem() {
		super();
		ID_STATUS_PENGGUNA = "";
		ID_PENGGUNA = "";
		STATUS = "";
		DELETED = "";
		META_TITLE = "";
		META_DESCRIPTION = "";
		META_IMAGE = "";
		DATE_CREATED = "";
		COMMENT_ON_STATUS_ID = "";
		META_URL = "";
		IMAGE = "";
		IMAGE_STATUS = "";
		USERNAME = "";
		NAMA_BELAKANG = "";
		NAMA_DEPAN = "";
		LINK = "";
		LINK_NAME = "";
		TYPE_STATUS ="";
		this.count_favorit = "";
		this.is_rate = "";
		this.role = "";
	}

	public MomentListitem(String iD_STATUS_PENGGUNA, String iD_PENGGUNA,
						  String sTATUS, String dELETED, String mETA_TITLE,
						  String mETA_DESCRIPTION, String mETA_IMAGE, String dATE_CREATED,
						  String cOMMENT_ON_STATUS_ID, String mETA_URL, String iMAGE,
						  String iMAGE_STATUS, String uSERNAME, String nAMA_BELAKANG,
						  String nAMA_DEPAN, String lINK, String lINK_NAME,
						  String count_favorit, String is_rate, String role, String tYPE_STATUS) {
		super();
		ID_STATUS_PENGGUNA = iD_STATUS_PENGGUNA;
		ID_PENGGUNA = iD_PENGGUNA;
		STATUS = sTATUS;
		DELETED = dELETED;
		META_TITLE = mETA_TITLE;
		META_DESCRIPTION = mETA_DESCRIPTION;
		META_IMAGE = mETA_IMAGE;
		DATE_CREATED = dATE_CREATED;
		COMMENT_ON_STATUS_ID = cOMMENT_ON_STATUS_ID;
		META_URL = mETA_URL;
		IMAGE = iMAGE;
		IMAGE_STATUS = iMAGE_STATUS;
		USERNAME = uSERNAME;
		NAMA_BELAKANG = nAMA_BELAKANG;
		NAMA_DEPAN = nAMA_DEPAN;
		LINK = lINK;
		LINK_NAME = lINK_NAME;
		this.count_favorit = count_favorit;
		this.is_rate = is_rate;
		this.role = role;
		TYPE_STATUS = tYPE_STATUS;
	}

	public String getID_STATUS_PENGGUNA() {
		return ID_STATUS_PENGGUNA;
	}

	public void setID_STATUS_PENGGUNA(String iD_STATUS_PENGGUNA) {
		ID_STATUS_PENGGUNA = iD_STATUS_PENGGUNA;
	}

	public String getID_PENGGUNA() {
		return ID_PENGGUNA;
	}

	public void setID_PENGGUNA(String iD_PENGGUNA) {
		ID_PENGGUNA = iD_PENGGUNA;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getDELETED() {
		return DELETED;
	}

	public void setDELETED(String dELETED) {
		DELETED = dELETED;
	}

	public String getMETA_TITLE() {
		return META_TITLE;
	}

	public void setMETA_TITLE(String mETA_TITLE) {
		META_TITLE = mETA_TITLE;
	}

	public String getMETA_DESCRIPTION() {
		return META_DESCRIPTION;
	}

	public void setMETA_DESCRIPTION(String mETA_DESCRIPTION) {
		META_DESCRIPTION = mETA_DESCRIPTION;
	}

	public String getMETA_IMAGE() {
		return META_IMAGE;
	}

	public void setMETA_IMAGE(String mETA_IMAGE) {
		META_IMAGE = mETA_IMAGE;
	}

	public String getDATE_CREATED() {
		return DATE_CREATED;
	}

	public void setDATE_CREATED(String dATE_CREATED) {
		DATE_CREATED = dATE_CREATED;
	}

	public String getCOMMENT_ON_STATUS_ID() {
		return COMMENT_ON_STATUS_ID;
	}

	public void setCOMMENT_ON_STATUS_ID(String cOMMENT_ON_STATUS_ID) {
		COMMENT_ON_STATUS_ID = cOMMENT_ON_STATUS_ID;
	}

	public String getMETA_URL() {
		return META_URL;
	}

	public void setMETA_URL(String mETA_URL) {
		META_URL = mETA_URL;
	}

	public String getIMAGE() {
		return IMAGE;
	}

	public void setIMAGE(String iMAGE) {
		IMAGE = iMAGE;
	}

	public String getIMAGE_STATUS() {
		return IMAGE_STATUS;
	}

	public void setIMAGE_STATUS(String iMAGE_STATUS) {
		IMAGE_STATUS = iMAGE_STATUS;
	}

	public String getUSERNAME() {
		return USERNAME;
	}

	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}

	public String getNAMA_BELAKANG() {
		return NAMA_BELAKANG;
	}

	public void setNAMA_BELAKANG(String nAMA_BELAKANG) {
		NAMA_BELAKANG = nAMA_BELAKANG;
	}

	public String getNAMA_DEPAN() {
		return NAMA_DEPAN;
	}

	public void setNAMA_DEPAN(String nAMA_DEPAN) {
		NAMA_DEPAN = nAMA_DEPAN;
	}

	public String getLINK() {
		return LINK;
	}

	public void setLINK(String lINK) {
		LINK = lINK;
	}

	public String getLINK_NAME() {
		return LINK_NAME;
	}

	public void setLINK_NAME(String lINK_NAME) {
		LINK_NAME = lINK_NAME;
	}

	public String getCount_favorit() {
		return count_favorit;
	}

	public void setCount_favorit(String count_favorit) {
		this.count_favorit = count_favorit;
	}

	public String getIs_rate() {
		return is_rate;
	}

	public void setIs_rate(String is_rate) {
		this.is_rate = is_rate;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getTYPE_STATUS() {
		return TYPE_STATUS;
	}

	public void setTYPE_STATUS(String TYPE_STATUS) {
		this.TYPE_STATUS = TYPE_STATUS;
	}

	public String getCOUNT_COMMENT() {
		return COUNT_COMMENT;
	}

	public void setCOUNT_COMMENT(String COUNT_COMMENT) {
		this.COUNT_COMMENT = COUNT_COMMENT;
	}
}

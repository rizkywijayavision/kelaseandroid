package com.edukasi.kelase.entities;


public class ProfileData {

	public String nama_depan;
	public String nama_belakang;
	public String alamat;
	public String negara;
	public String provinsi;
	public String nama_negara;
	public String nama_provinsi;
	public String tempat_lahir;
	public String tanggal_lahir;
	public String telepon;
	public String facebook;
	public String twitter;
	public String profile_image;
	public String cover_image;

	public ProfileData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfileData(String nama_depan, String nama_belakang, String alamat,
					   String negara, String provinsi, String nama_negara,
					   String nama_provinsi, String tempat_lahir, String tanggal_lahir,
					   String telepon, String facebook, String twitter,
					   String profile_image, String cover_image) {
		super();
		this.nama_depan = nama_depan;
		this.nama_belakang = nama_belakang;
		this.alamat = alamat;
		this.negara = negara;
		this.provinsi = provinsi;
		this.nama_negara = nama_negara;
		this.nama_provinsi = nama_provinsi;
		this.tempat_lahir = tempat_lahir;
		this.tanggal_lahir = tanggal_lahir;
		this.telepon = telepon;
		this.facebook = facebook;
		this.twitter = twitter;
		this.profile_image = profile_image;
		this.cover_image = cover_image;
	}

	public String getNama_depan() {
		return nama_depan;
	}

	public void setNama_depan(String nama_depan) {
		this.nama_depan = nama_depan;
	}

	public String getNama_belakang() {
		return nama_belakang;
	}

	public void setNama_belakang(String nama_belakang) {
		this.nama_belakang = nama_belakang;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getNegara() {
		return negara;
	}

	public void setNegara(String negara) {
		this.negara = negara;
	}

	public String getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}

	public String getNama_negara() {
		return nama_negara;
	}

	public void setNama_negara(String nama_negara) {
		this.nama_negara = nama_negara;
	}

	public String getNama_provinsi() {
		return nama_provinsi;
	}

	public void setNama_provinsi(String nama_provinsi) {
		this.nama_provinsi = nama_provinsi;
	}

	public String getTempat_lahir() {
		return tempat_lahir;
	}

	public void setTempat_lahir(String tempat_lahir) {
		this.tempat_lahir = tempat_lahir;
	}

	public String getTanggal_lahir() {
		return tanggal_lahir;
	}

	public void setTanggal_lahir(String tanggal_lahir) {
		this.tanggal_lahir = tanggal_lahir;
	}

	public String getTelepon() {
		return telepon;
	}

	public void setTelepon(String telepon) {
		this.telepon = telepon;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getProfile_image() {
		return profile_image;
	}

	public void setProfile_image(String profile_image) {
		this.profile_image = profile_image;
	}

	public String getCover_image() {
		return cover_image;
	}

	public void setCover_image(String cover_image) {
		this.cover_image = cover_image;
	}


}

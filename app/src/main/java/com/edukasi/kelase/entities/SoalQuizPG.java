package com.edukasi.kelase.entities;

/**
 * Created by ebizu-rizky on 1/4/16.
 */
public class SoalQuizPG {

    private String SOAL;
    private String OPSI_A;
    private String OPSI_B;
    private String OPSI_C;
    private String OPSI_D;
    private String ID_SOAL;
    private String BOBOT_SOAL;

    public String getSOAL() {
        return SOAL;
    }

    public void setSOAL(String SOAL) {
        this.SOAL = SOAL;
    }

    public String getOPSI_A() {
        return OPSI_A;
    }

    public void setOPSI_A(String OPSI_A) {
        this.OPSI_A = OPSI_A;
    }

    public String getOPSI_B() {
        return OPSI_B;
    }

    public void setOPSI_B(String OPSI_B) {
        this.OPSI_B = OPSI_B;
    }

    public String getOPSI_C() {
        return OPSI_C;
    }

    public void setOPSI_C(String OPSI_C) {
        this.OPSI_C = OPSI_C;
    }

    public String getOPSI_D() {
        return OPSI_D;
    }

    public void setOPSI_D(String OPSI_D) {
        this.OPSI_D = OPSI_D;
    }

    public String getID_SOAL() {
        return ID_SOAL;
    }

    public void setID_SOAL(String ID_SOAL) {
        this.ID_SOAL = ID_SOAL;
    }

    public String getBOBOT_SOAL() {
        return BOBOT_SOAL;
    }

    public void setBOBOT_SOAL(String BOBOT_SOAL) {
        this.BOBOT_SOAL = BOBOT_SOAL;
    }
}

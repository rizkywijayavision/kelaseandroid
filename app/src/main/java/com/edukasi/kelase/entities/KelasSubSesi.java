package com.edukasi.kelase.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ebizu-rizky on 11/29/15.
 */
public class KelasSubSesi extends BaseEntity {

    @DatabaseField(columnName = "ID_KELAS")
    private String ID_KELAS;
    @DatabaseField(columnName = "ID_SESI")
    private String ID_SESI;
    @DatabaseField(columnName = "URUTAN")
    private String URUTAN;
    @DatabaseField(columnName = "IS_DONE")
    private String IS_DONE;
    @DatabaseField(columnName = "ID_TYPE_SUB_SESI")
    private String ID_TYPE_SUB_SESI;
    @DatabaseField(columnName = "JUDUL_SUB_SESI")
    private String JUDUL_SUB_SESI;
    @DatabaseField(columnName = "DESKRIPSI_SUB_SESI")
    private String DESKRIPSI_SUB_SESI;
    @DatabaseField(columnName = "IS_ACTIVE")
    private String IS_ACTIVE;
    @DatabaseField(columnName = "TYPE_SUB_SESI")
    private String TYPE_SUB_SESI;

    public String getID_KELAS() {
        return ID_KELAS;
    }

    public void setID_KELAS(String ID_KELAS) {
        this.ID_KELAS = ID_KELAS;
    }

    public String getID_SESI() {
        return ID_SESI;
    }

    public void setID_SESI(String ID_SESI) {
        this.ID_SESI = ID_SESI;
    }


    public String getURUTAN() {
        return URUTAN;
    }

    public void setURUTAN(String URUTAN) {
        this.URUTAN = URUTAN;
    }

    public String getIS_DONE() {
        return IS_DONE;
    }

    public void setIS_DONE(String IS_DONE) {
        this.IS_DONE = IS_DONE;
    }

    public String getID_TYPE_SUB_SESI() {
        return ID_TYPE_SUB_SESI;
    }

    public void setID_TYPE_SUB_SESI(String ID_TYPE_SUB_SESI) {
        this.ID_TYPE_SUB_SESI = ID_TYPE_SUB_SESI;
    }

    public String getJUDUL_SUB_SESI() {
        return JUDUL_SUB_SESI;
    }

    public void setJUDUL_SUB_SESI(String JUDUL_SUB_SESI) {
        this.JUDUL_SUB_SESI = JUDUL_SUB_SESI;
    }

    public String getDESKRIPSI_SUB_SESI() {
        return DESKRIPSI_SUB_SESI;
    }

    public void setDESKRIPSI_SUB_SESI(String DESKRIPSI_SUB_SESI) {
        this.DESKRIPSI_SUB_SESI = DESKRIPSI_SUB_SESI;
    }

    public String getIS_ACTIVE() {
        return IS_ACTIVE;
    }

    public void setIS_ACTIVE(String IS_ACTIVE) {
        this.IS_ACTIVE = IS_ACTIVE;
    }

    public String getTYPE_SUB_SESI() {
        return TYPE_SUB_SESI;
    }

    public void setTYPE_SUB_SESI(String TYPE_SUB_SESI) {
        this.TYPE_SUB_SESI = TYPE_SUB_SESI;
    }
}

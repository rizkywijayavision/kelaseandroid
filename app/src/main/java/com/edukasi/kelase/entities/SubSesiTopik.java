package com.edukasi.kelase.entities;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class SubSesiTopik {

    private String ID_TOPIK;
    private String ID_SESI;
    private String ID_SUB_SESI;
    private String JUDUL_TOPIK;
    private String DESKRIPSI_TOPIK;
    private String NAMA_PENGGUNA;
    private String DATE_CREATED;

    public String getID_TOPIK() {
        return ID_TOPIK;
    }

    public void setID_TOPIK(String ID_TOPIK) {
        this.ID_TOPIK = ID_TOPIK;
    }

    public String getID_SESI() {
        return ID_SESI;
    }

    public void setID_SESI(String ID_SESI) {
        this.ID_SESI = ID_SESI;
    }

    public String getID_SUB_SESI() {
        return ID_SUB_SESI;
    }

    public void setID_SUB_SESI(String ID_SUB_SESI) {
        this.ID_SUB_SESI = ID_SUB_SESI;
    }

    public String getJUDUL_TOPIK() {
        return JUDUL_TOPIK;
    }

    public void setJUDUL_TOPIK(String JUDUL_TOPIK) {
        this.JUDUL_TOPIK = JUDUL_TOPIK;
    }

    public String getDESKRIPSI_TOPIK() {
        return DESKRIPSI_TOPIK;
    }

    public void setDESKRIPSI_TOPIK(String DESKRIPSI_TOPIK) {
        this.DESKRIPSI_TOPIK = DESKRIPSI_TOPIK;
    }

    public String getNAMA_PENGGUNA() {
        return NAMA_PENGGUNA;
    }

    public void setNAMA_PENGGUNA(String NAMA_PENGGUNA) {
        this.NAMA_PENGGUNA = NAMA_PENGGUNA;
    }

    public String getDATE_CREATED() {
        return DATE_CREATED;
    }

    public void setDATE_CREATED(String DATE_CREATED) {
        this.DATE_CREATED = DATE_CREATED;
    }
}

package com.edukasi.kelase.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ebizu-rizky on 11/29/15.
 */
public class KelasSesi extends BaseEntity{

    @DatabaseField(columnName = "ID_KELAS")
    private String ID_KELAS;
    @DatabaseField(columnName = "JUDUL_SESI")
    private String JUDUL_SESI;
    @DatabaseField(columnName = "DESKRIPSI_SESI")
    private String DESKRIPSI_SESI;

    public String getID_KELAS() {
        return ID_KELAS;
    }

    public void setID_KELAS(String ID_KELAS) {
        this.ID_KELAS = ID_KELAS;
    }

    public String getJUDUL_SESI() {
        return JUDUL_SESI;
    }

    public void setJUDUL_SESI(String JUDUL_SESI) {
        this.JUDUL_SESI = JUDUL_SESI;
    }

    public String getDESKRIPSI_SESI() {
        return DESKRIPSI_SESI;
    }

    public void setDESKRIPSI_SESI(String DESKRIPSI_SESI) {
        this.DESKRIPSI_SESI = DESKRIPSI_SESI;
    }
}

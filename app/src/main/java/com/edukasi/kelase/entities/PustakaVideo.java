package com.edukasi.kelase.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaVideo extends BaseEntity {

    @DatabaseField(columnName = "NAMA_VIDEO")
    private String NAMA_VIDEO;
    @DatabaseField(columnName = "LINK_VIDEO")
    private String LINK_VIDEO;
    @DatabaseField(columnName = "PRIVASI")
    private String PRIVASI;
    @DatabaseField(columnName = "DATE_CREATED")
    private String DATE_CREATED;
    @DatabaseField(columnName = "ID_PENGGUNA")
    private String ID_PENGGUNA;
    @DatabaseField(columnName = "SOURCE")
    private String SOURCE;

    public String getNAMA_VIDEO() {
        return NAMA_VIDEO;
    }

    public void setNAMA_VIDEO(String NAMA_VIDEO) {
        this.NAMA_VIDEO = NAMA_VIDEO;
    }

    public String getLINK_VIDEO() {
        return LINK_VIDEO;
    }

    public void setLINK_VIDEO(String LINK_VIDEO) {
        this.LINK_VIDEO = LINK_VIDEO;
    }

    public String getPRIVASI() {
        return PRIVASI;
    }

    public void setPRIVASI(String PRIVASI) {
        this.PRIVASI = PRIVASI;
    }

    public String getDATE_CREATED() {
        return DATE_CREATED;
    }

    public void setDATE_CREATED(String DATE_CREATED) {
        this.DATE_CREATED = DATE_CREATED;
    }

    public String getID_PENGGUNA() {
        return ID_PENGGUNA;
    }

    public void setID_PENGGUNA(String ID_PENGGUNA) {
        this.ID_PENGGUNA = ID_PENGGUNA;
    }

    public String getSOURCE() {
        return SOURCE;
    }

    public void setSOURCE(String SOURCE) {
        this.SOURCE = SOURCE;
    }
}

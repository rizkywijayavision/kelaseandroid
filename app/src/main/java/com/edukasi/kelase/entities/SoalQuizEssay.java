package com.edukasi.kelase.entities;

/**
 * Created by ebizu-rizky on 1/4/16.
 */
public class SoalQuizEssay {

    private String SOAL;
    private String ID_SOAL;
    private String BOBOT_SOAL;

    public String getSOAL() {
        return SOAL;
    }

    public void setSOAL(String SOAL) {
        this.SOAL = SOAL;
    }

    public String getID_SOAL() {
        return ID_SOAL;
    }

    public void setID_SOAL(String ID_SOAL) {
        this.ID_SOAL = ID_SOAL;
    }

    public String getBOBOT_SOAL() {
        return BOBOT_SOAL;
    }

    public void setBOBOT_SOAL(String BOBOT_SOAL) {
        this.BOBOT_SOAL = BOBOT_SOAL;
    }
}

package com.edukasi.kelase.entities;

import java.util.List;

/**
 * Created by ebizu-rizky on 1/4/16.
 */
public class SoalQuiz {

    private String JUDUL_QUIZ;
    private String DESKRIPSI_QUIZ;
    private String TANGGAL_MULAI_QUIZ;
    private String TANGGAL_SELESAI_QUIZ;
    private String DURASI;
    private String DATE_CREATED;
    private String TANGGAL_START_QUIZ;
    private String TANGGAL_EXPIRED_QUIZ;
    private List<SoalQuizPG> soalQuizPGList;
    private List<SoalQuizEssay> soalQuizEssayList;

    public String getJUDUL_QUIZ() {
        return JUDUL_QUIZ;
    }

    public void setJUDUL_QUIZ(String JUDUL_QUIZ) {
        this.JUDUL_QUIZ = JUDUL_QUIZ;
    }

    public String getDESKRIPSI_QUIZ() {
        return DESKRIPSI_QUIZ;
    }

    public void setDESKRIPSI_QUIZ(String DESKRIPSI_QUIZ) {
        this.DESKRIPSI_QUIZ = DESKRIPSI_QUIZ;
    }

    public String getTANGGAL_MULAI_QUIZ() {
        return TANGGAL_MULAI_QUIZ;
    }

    public void setTANGGAL_MULAI_QUIZ(String TANGGAL_MULAI_QUIZ) {
        this.TANGGAL_MULAI_QUIZ = TANGGAL_MULAI_QUIZ;
    }

    public String getTANGGAL_SELESAI_QUIZ() {
        return TANGGAL_SELESAI_QUIZ;
    }

    public void setTANGGAL_SELESAI_QUIZ(String TANGGAL_SELESAI_QUIZ) {
        this.TANGGAL_SELESAI_QUIZ = TANGGAL_SELESAI_QUIZ;
    }

    public String getDURASI() {
        return DURASI;
    }

    public void setDURASI(String DURASI) {
        this.DURASI = DURASI;
    }

    public String getDATE_CREATED() {
        return DATE_CREATED;
    }

    public void setDATE_CREATED(String DATE_CREATED) {
        this.DATE_CREATED = DATE_CREATED;
    }

    public String getTANGGAL_START_QUIZ() {
        return TANGGAL_START_QUIZ;
    }

    public void setTANGGAL_START_QUIZ(String TANGGAL_START_QUIZ) {
        this.TANGGAL_START_QUIZ = TANGGAL_START_QUIZ;
    }

    public String getTANGGAL_EXPIRED_QUIZ() {
        return TANGGAL_EXPIRED_QUIZ;
    }

    public void setTANGGAL_EXPIRED_QUIZ(String TANGGAL_EXPIRED_QUIZ) {
        this.TANGGAL_EXPIRED_QUIZ = TANGGAL_EXPIRED_QUIZ;
    }

    public List<SoalQuizPG> getSoalQuizPGList() {
        return soalQuizPGList;
    }

    public void setSoalQuizPGList(List<SoalQuizPG> soalQuizPGList) {
        this.soalQuizPGList = soalQuizPGList;
    }

    public List<SoalQuizEssay> getSoalQuizEssayList() {
        return soalQuizEssayList;
    }

    public void setSoalQuizEssayList(List<SoalQuizEssay> soalQuizEssayList) {
        this.soalQuizEssayList = soalQuizEssayList;
    }
}

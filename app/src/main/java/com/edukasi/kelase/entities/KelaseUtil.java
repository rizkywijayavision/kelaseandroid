package com.edukasi.kelase.entities;

import com.edukasi.kelase.Config;

/**
 * Created by ebizu-rizky on 9/18/15.
 */
public class KelaseUtil {
    public static final String KELASE_SERVICE_URL = Config.getURL()+"api/v1/";
    public static final String KELASE_SERVICE_IMAGE_URL = Config.getURL()+"files/upload/member/";
    public static final String KELASE_SERVICE_IMAGE_STATUS = Config.getURL()+"files/upload/status/original/";
    public static final String KELASE_SERVICE_KELASE_IMAGE_URL = Config.getURL()+"files/upload/kelas/";
}

package com.edukasi.kelase.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ebizu-rizky on 1/25/16.
 */
public class PustakaTautan extends BaseEntity {

    @DatabaseField(columnName = "META_TITLE")
    private String META_TITLE;
    @DatabaseField(columnName = "META_LINK")
    private String META_LINK;
    @DatabaseField(columnName = "META_IMAGE")
    private String META_IMAGE;
    @DatabaseField(columnName = "META_DESCRIPTION")
    private String META_DESCRIPTION;
    @DatabaseField(columnName = "DESKRIPSI_TAUTAN")
    private String DESKRIPSI_TAUTAN;
    @DatabaseField(columnName = "DATE_CREATED")
    private String DATE_CREATED;
    @DatabaseField(columnName = "ID_PENGGUNA")
    private String ID_PENGGUNA;

    public String getMETA_TITLE() {
        return META_TITLE;
    }

    public void setMETA_TITLE(String META_TITLE) {
        this.META_TITLE = META_TITLE;
    }

    public String getMETA_LINK() {
        return META_LINK;
    }

    public void setMETA_LINK(String META_LINK) {
        this.META_LINK = META_LINK;
    }

    public String getMETA_IMAGE() {
        return META_IMAGE;
    }

    public void setMETA_IMAGE(String META_IMAGE) {
        this.META_IMAGE = META_IMAGE;
    }

    public String getMETA_DESCRIPTION() {
        return META_DESCRIPTION;
    }

    public void setMETA_DESCRIPTION(String META_DESCRIPTION) {
        this.META_DESCRIPTION = META_DESCRIPTION;
    }

    public String getDESKRIPSI_TAUTAN() {
        return DESKRIPSI_TAUTAN;
    }

    public void setDESKRIPSI_TAUTAN(String DESKRIPSI_TAUTAN) {
        this.DESKRIPSI_TAUTAN = DESKRIPSI_TAUTAN;
    }

    public String getDATE_CREATED() {
        return DATE_CREATED;
    }

    public void setDATE_CREATED(String DATE_CREATED) {
        this.DATE_CREATED = DATE_CREATED;
    }

    public String getID_PENGGUNA() {
        return ID_PENGGUNA;
    }

    public void setID_PENGGUNA(String ID_PENGGUNA) {
        this.ID_PENGGUNA = ID_PENGGUNA;
    }
}

package com.edukasi.kelase.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 
 * @author rezkyatinnov
 *
 */
public class KelasListitem implements Parcelable {

	public String ID_KELAS;
	public String NAMA_KELAS;
	public String INFO_KELAS;
	public String ID_GURU;
	public String NAMA_GURU;
	public String JML_PESERTA;
	public String ENROLLMENT_STATUS;
	public String AVAILABILITY_DATE;
	public String ICON_KELAS;
	public String KATA_KUNCI_KELAS;
	public String ID_KATEGORI;
	public String ID_INSTITUSI;
	public String DATE_START;
	public String DATE_END;
	public String STATUS;
	public String TAG;
	public String COVER;
	public String NAMA_INSTITUSI;
	public String PUBLIC_STATUS;
	public String PERMALINK_INSTITUSI;

	public KelasListitem() {
		super();
		ID_KELAS = "";
		NAMA_KELAS = "";
		INFO_KELAS = "";
		ID_GURU = "";
		NAMA_GURU = "";
		JML_PESERTA = "";
		ENROLLMENT_STATUS = "";
		AVAILABILITY_DATE = "";
		ICON_KELAS = "";
		KATA_KUNCI_KELAS = "";
		ID_KATEGORI = "";
		ID_INSTITUSI = "";
		DATE_START = "";
		DATE_END = "";
		STATUS = "";
		TAG = "";
		COVER = "";
		NAMA_INSTITUSI = "";
		PUBLIC_STATUS = "";
		PERMALINK_INSTITUSI = "";
	}

	public KelasListitem(String iD_KELAS, String nAMA_KELAS, String iNFO_KELAS,
						 String iD_GURU, String nAMA_GURU, String jML_PESERTA, String eNROLLMENT_STATUS,
						 String aVAILABILITY_DATE, String iCON_KELAS,
						 String kATA_KUNCI_KELAS, String iD_KATEGORI, String iD_INSTITUSI,
						 String dATE_START, String dATE_END, String sTATUS, String tAG,
						 String cOVER, String nAMA_INSTITUSI, String pUBLIC_STATUS, String pERMALINK_INSTITUSI) {
		super();
		ID_KELAS = iD_KELAS;
		NAMA_KELAS = nAMA_KELAS;
		INFO_KELAS = iNFO_KELAS;
		ID_GURU = iD_GURU;
		NAMA_GURU = nAMA_GURU;
		JML_PESERTA = jML_PESERTA;
		ENROLLMENT_STATUS = eNROLLMENT_STATUS;
		AVAILABILITY_DATE = aVAILABILITY_DATE;
		ICON_KELAS = iCON_KELAS;
		KATA_KUNCI_KELAS = kATA_KUNCI_KELAS;
		ID_KATEGORI = iD_KATEGORI;
		ID_INSTITUSI = iD_INSTITUSI;
		DATE_START = dATE_START;
		DATE_END = dATE_END;
		STATUS = sTATUS;
		TAG = tAG;
		COVER = cOVER;
		NAMA_INSTITUSI = nAMA_INSTITUSI;
		PUBLIC_STATUS = pUBLIC_STATUS;
		PERMALINK_INSTITUSI = pERMALINK_INSTITUSI;
	}

	protected KelasListitem(Parcel in) {
		ID_KELAS = in.readString();
		NAMA_KELAS = in.readString();
		INFO_KELAS = in.readString();
		ID_GURU = in.readString();
		NAMA_GURU = in.readString();
		JML_PESERTA = in.readString();
		ENROLLMENT_STATUS = in.readString();
		AVAILABILITY_DATE = in.readString();
		ICON_KELAS = in.readString();
		KATA_KUNCI_KELAS = in.readString();
		ID_KATEGORI = in.readString();
		ID_INSTITUSI = in.readString();
		DATE_START = in.readString();
		DATE_END = in.readString();
		STATUS = in.readString();
		TAG = in.readString();
		COVER = in.readString();
		NAMA_INSTITUSI = in.readString();
		PUBLIC_STATUS = in.readString();
		PERMALINK_INSTITUSI = in.readString();
	}

	public static final Creator<KelasListitem> CREATOR = new Creator<KelasListitem>() {
		@Override
		public KelasListitem createFromParcel(Parcel in) {
			return new KelasListitem(in);
		}

		@Override
		public KelasListitem[] newArray(int size) {
			return new KelasListitem[size];
		}
	};

	public String getID_KELAS() {
		return ID_KELAS;
	}

	public void setID_KELAS(String iD_KELAS) {
		ID_KELAS = iD_KELAS;
	}

	public String getNAMA_KELAS() {
		return NAMA_KELAS;
	}

	public void setNAMA_KELAS(String nAMA_KELAS) {
		NAMA_KELAS = nAMA_KELAS;
	}

	public String getINFO_KELAS() {
		return INFO_KELAS;
	}

	public void setINFO_KELAS(String iNFO_KELAS) {
		INFO_KELAS = iNFO_KELAS;
	}

	public String getID_GURU() {
		return ID_GURU;
	}

	public void setID_GURU(String iD_GURU) {
		ID_GURU = iD_GURU;
	}

	public String getNAMA_GURU() {
		return NAMA_GURU;
	}

	public void setNAMA_GURU(String nAMA_GURU) {
		NAMA_GURU = nAMA_GURU;
	}

	public String getJML_PESERTA() {
		return JML_PESERTA;
	}

	public void setJML_PESERTA(String jML_PESERTA) {
		JML_PESERTA = jML_PESERTA;
	}

	public String getENROLLMENT_STATUS() {
		return ENROLLMENT_STATUS;
	}

	public void setENROLLMENT_STATUS(String eNROLLMENT_STATUS) {
		ENROLLMENT_STATUS = eNROLLMENT_STATUS;
	}

	public String getAVAILABILITY_DATE() {
		return AVAILABILITY_DATE;
	}

	public void setAVAILABILITY_DATE(String aVAILABILITY_DATE) {
		AVAILABILITY_DATE = aVAILABILITY_DATE;
	}

	public String getICON_KELAS() {
		return ICON_KELAS;
	}

	public void setICON_KELAS(String iCON_KELAS) {
		ICON_KELAS = iCON_KELAS;
	}

	public String getKATA_KUNCI_KELAS() {
		return KATA_KUNCI_KELAS;
	}

	public void setKATA_KUNCI_KELAS(String kATA_KUNCI_KELAS) {
		KATA_KUNCI_KELAS = kATA_KUNCI_KELAS;
	}

	public String getID_KATEGORI() {
		return ID_KATEGORI;
	}

	public void setID_KATEGORI(String iD_KATEGORI) {
		ID_KATEGORI = iD_KATEGORI;
	}

	public String getID_INSTITUSI() {
		return ID_INSTITUSI;
	}

	public void setID_INSTITUSI(String iD_INSTITUSI) {
		ID_INSTITUSI = iD_INSTITUSI;
	}

	public String getDATE_START() {
		return DATE_START;
	}

	public void setDATE_START(String dATE_START) {
		DATE_START = dATE_START;
	}

	public String getDATE_END() {
		return DATE_END;
	}

	public void setDATE_END(String dATE_END) {
		DATE_END = dATE_END;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getTAG() {
		return TAG;
	}

	public void setTAG(String tAG) {
		TAG = tAG;
	}

	public String getCOVER() {
		return COVER;
	}

	public void setCOVER(String cOVER) {
		COVER = cOVER;
	}

	public String getNAMA_INSTITUSI() {
		return NAMA_INSTITUSI;
	}

	public void setNAMA_INSTITUSI(String nAMA_INSTITUSI) {
		NAMA_INSTITUSI = nAMA_INSTITUSI;
	}

	public String getPUBLIC_STATUS(){
		return PUBLIC_STATUS;
	}

	public void setPUBLIC_STATUS(String pUBLIC_STATUS){
		PUBLIC_STATUS = pUBLIC_STATUS;
	}

	public String getPERMALINK_INSTITUSI() {
		return PERMALINK_INSTITUSI;
	}

	public void setPERMALINK_INSTITUSI(String pERMALINK_INSTITUSI) {
		PERMALINK_INSTITUSI = pERMALINK_INSTITUSI;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(ID_KELAS);
		parcel.writeString(NAMA_KELAS);
		parcel.writeString(INFO_KELAS);
		parcel.writeString(ID_GURU);
		parcel.writeString(NAMA_GURU);
		parcel.writeString(JML_PESERTA);
		parcel.writeString(ENROLLMENT_STATUS);
		parcel.writeString(AVAILABILITY_DATE);
		parcel.writeString(ICON_KELAS);
		parcel.writeString(KATA_KUNCI_KELAS);
		parcel.writeString(ID_KATEGORI);
		parcel.writeString(ID_INSTITUSI);
		parcel.writeString(DATE_START);
		parcel.writeString(DATE_END);
		parcel.writeString(STATUS);
		parcel.writeString(TAG);
		parcel.writeString(COVER);
		parcel.writeString(NAMA_INSTITUSI);
		parcel.writeString(PUBLIC_STATUS);
		parcel.writeString(PERMALINK_INSTITUSI);
	}
	// Parcelling part


	@Override
	public int describeContents(){
		return 0;
	}
}

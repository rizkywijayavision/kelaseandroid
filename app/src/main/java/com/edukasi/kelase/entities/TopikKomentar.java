package com.edukasi.kelase.entities;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class TopikKomentar {
    private String ID_KOMENTAR;
    private String KOMENTAR;
    private String DATE_CREATED;
    private String ID_PENGGUNA;
    private String NAMA_PENGGUNA;
    private String IMAGE_PENGGUNA;

    public String getID_KOMENTAR() {
        return ID_KOMENTAR;
    }

    public void setID_KOMENTAR(String ID_KOMENTAR) {
        this.ID_KOMENTAR = ID_KOMENTAR;
    }

    public String getKOMENTAR() {
        return KOMENTAR;
    }

    public void setKOMENTAR(String KOMENTAR) {
        this.KOMENTAR = KOMENTAR;
    }

    public String getDATE_CREATED() {
        return DATE_CREATED;
    }

    public void setDATE_CREATED(String DATE_CREATED) {
        this.DATE_CREATED = DATE_CREATED;
    }

    public String getID_PENGGUNA() {
        return ID_PENGGUNA;
    }

    public void setID_PENGGUNA(String ID_PENGGUNA) {
        this.ID_PENGGUNA = ID_PENGGUNA;
    }

    public String getNAMA_PENGGUNA() {
        return NAMA_PENGGUNA;
    }

    public void setNAMA_PENGGUNA(String NAMA_PENGGUNA) {
        this.NAMA_PENGGUNA = NAMA_PENGGUNA;
    }

    public String getIMAGE_PENGGUNA() {
        return IMAGE_PENGGUNA;
    }

    public void setIMAGE_PENGGUNA(String IMAGE_PENGGUNA) {
        this.IMAGE_PENGGUNA = IMAGE_PENGGUNA;
    }
}

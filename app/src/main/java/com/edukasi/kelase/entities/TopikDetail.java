package com.edukasi.kelase.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class TopikDetail {
    private String JUDUL_TOPIK;
    private String DESKRIPSI_TOPIK;
    private String DATE_CREATED;
    private String NAMA_PENGGUNA;
    private String IMAGE_PENGGUNA;
    private String ID_PENGGUNA;
    private List<TopikKomentar> LIST_KOMENTAR;

    public TopikDetail(){
        LIST_KOMENTAR = new ArrayList<>();
    }

    public void addKomentar(TopikKomentar list_komentar){
        LIST_KOMENTAR.add(list_komentar);
    }

    public List<TopikKomentar> getLIST_KOMENTAR(){
        return LIST_KOMENTAR;
    }

    public String getJUDUL_TOPIK() {
        return JUDUL_TOPIK;
    }

    public void setJUDUL_TOPIK(String JUDUL_TOPIK) {
        this.JUDUL_TOPIK = JUDUL_TOPIK;
    }

    public String getDESKRIPSI_TOPIK() {
        return DESKRIPSI_TOPIK;
    }

    public void setDESKRIPSI_TOPIK(String DESKRIPSI_TOPIK) {
        this.DESKRIPSI_TOPIK = DESKRIPSI_TOPIK;
    }

    public String getDATE_CREATED() {
        return DATE_CREATED;
    }

    public void setDATE_CREATED(String DATE_CREATED) {
        this.DATE_CREATED = DATE_CREATED;
    }

    public String getNAMA_PENGGUNA() {
        return NAMA_PENGGUNA;
    }

    public void setNAMA_PENGGUNA(String NAMA_PENGGUNA) {
        this.NAMA_PENGGUNA = NAMA_PENGGUNA;
    }

    public String getIMAGE_PENGGUNA() {
        return IMAGE_PENGGUNA;
    }

    public void setIMAGE_PENGGUNA(String IMAGE_PENGGUNA) {
        this.IMAGE_PENGGUNA = IMAGE_PENGGUNA;
    }

    public String getID_PENGGUNA() {
        return ID_PENGGUNA;
    }

    public void setID_PENGGUNA(String ID_PENGGUNA) {
        this.ID_PENGGUNA = ID_PENGGUNA;
    }
}

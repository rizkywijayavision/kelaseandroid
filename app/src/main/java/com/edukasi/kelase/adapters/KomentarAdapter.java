package com.edukasi.kelase.adapters;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.edukasi.kelase.R;
import com.edukasi.kelase.entities.KelaseUtil;
import com.edukasi.kelase.entities.KomentarListitem;
import com.edukasi.kelase.helpers.PictureHelper;
import com.pkmmte.view.CircularImageView;

/**
 * Created by ebizu-rizky on 11/22/15.
 */

public class KomentarAdapter extends BaseAdapter<KomentarListitem> {

    private PictureHelper pictureHelper;

    public KomentarAdapter(Context context) {
        super(context);
        pictureHelper = PictureHelper.getInstance(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final KomentarListitem komentarListitem = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_komentar, parent, false);
            holder = new ViewHolder();
            holder.textName = (TextView) convertView.findViewById(R.id.komentaritem_namalbl);
            holder.textDate = (TextView) convertView.findViewById(R.id.komentaritem_timestamplbl);
            holder.textComment = (TextView) convertView.findViewById(R.id.komentaritem_komentarlbl);
            holder.imageViewProfpic = (CircularImageView) convertView.findViewById(R.id.komentaritem_propic);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (komentarListitem != null){
            holder.textName.setText(komentarListitem.getNAMA_DEPAN() + " " + komentarListitem.getNAMA_BELAKANG());
            holder.textDate.setText(komentarListitem.getDATE_CREATED());
            holder.textComment.setText(komentarListitem.getSTATUS());
            pictureHelper.displayImage(KelaseUtil.KELASE_SERVICE_IMAGE_URL + komentarListitem.getIMAGE(), holder.imageViewProfpic);
        }
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        TextView textName, textDate, textComment;
        CircularImageView imageViewProfpic;
    }

}

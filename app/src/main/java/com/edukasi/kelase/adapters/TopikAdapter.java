package com.edukasi.kelase.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.entities.SubSesiTopik;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class TopikAdapter extends BaseAdapter<SubSesiTopik> {

    public TopikAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final SubSesiTopik subSesiTopik = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_topik, parent, false);
            holder = new ViewHolder();
            holder.textJudul = (TextView) convertView.findViewById(R.id.judul_topik);
            holder.textUser = (TextView) convertView.findViewById(R.id.nama_pengguna);
            holder.textDate = (TextView) convertView.findViewById(R.id.date_created);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (subSesiTopik != null){
            holder.textJudul.setText(subSesiTopik.getJUDUL_TOPIK());
            holder.textUser.setText(subSesiTopik.getNAMA_PENGGUNA());
            PrettyTime p = new PrettyTime();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
            try {
                Date date = format.parse(subSesiTopik.getDATE_CREATED());
                holder.textDate.setText(p.format(date));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        TextView textJudul, textDate, textUser;
    }
}

package com.edukasi.kelase.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.entities.Kelas;
import com.edukasi.kelase.entities.KelaseUtil;
import com.edukasi.kelase.helpers.PictureHelper;
import com.github.siyamed.shapeimageview.RoundedImageView;

/**
 * Created by rizky on 28/08/15.
 */
public class KelasListAdapter extends BaseAdapter<Kelas> {

    private PictureHelper pictureHelper;

    public KelasListAdapter(Context context) {
        super(context);
        pictureHelper = PictureHelper.getInstance(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final Kelas kelasListitem = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.kelas_listitem, parent, false);
            holder = new ViewHolder();
            holder.textViewKelas = (TextView) convertView.findViewById(R.id.title_kelas);
            holder.textViewPeserta = (TextView) convertView.findViewById(R.id.text_peserta);
            holder.textViewStatus = (TextView) convertView.findViewById(R.id.text_statusKelas);
            holder.textViewGuru = (TextView) convertView.findViewById(R.id.text_guru);
            holder.kelasIcon = (RoundedImageView) convertView.findViewById(R.id.kelas_icon);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (kelasListitem != null){
            holder.textViewKelas.setText(kelasListitem.getNAMA_KELAS());
            if (kelasListitem.getJML_PESERTA()== "null" || kelasListitem.getJML_PESERTA()== "")
                holder.textViewPeserta.setText("0 orang"); else holder.textViewPeserta.setText(kelasListitem.getJML_PESERTA() + " orang");
            if (kelasListitem.getSTATUS() == "1")
                holder.textViewStatus.setText("Dibuka"); else holder.textViewStatus.setText("Ditutup");
            String iconKelas = kelasListitem.getICON_KELAS();
            if (iconKelas == "null" || iconKelas == "" || iconKelas == null || iconKelas.isEmpty()){
                holder.kelasIcon.setImageResource(R.drawable.default_kelas);
            } else {
                pictureHelper.displayImage(KelaseUtil.KELASE_SERVICE_KELASE_IMAGE_URL + kelasListitem.getICON_KELAS(), holder.kelasIcon);
            }
            holder.textViewGuru.setText(kelasListitem.getNAMA_GURU_ADMIN());
        }
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        TextView textViewKelas, textViewPeserta, textViewStatus, textViewGuru;
        RoundedImageView kelasIcon;
    }
}

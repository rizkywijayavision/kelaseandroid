package com.edukasi.kelase.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.Util;
import com.edukasi.kelase.entities.PustakaTautan;
import com.edukasi.kelase.helpers.PictureHelper;

/**
 * Created by ebizu-rizky on 1/25/16.
 */
public class PustakaTautanAdapter extends BaseAdapter<PustakaTautan> {

    private PictureHelper pictureHelper;

    public PustakaTautanAdapter(Context context) {
        super(context);
        pictureHelper = PictureHelper.getInstance(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final PustakaTautan pustakaTautan = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_pustaka_tautan, parent, false);
            holder = new ViewHolder();
            holder.metaTitle = (TextView) convertView.findViewById(R.id.meta_title);
            holder.metaDesc = (TextView) convertView.findViewById(R.id.meta_description);
            holder.link = (TextView) convertView.findViewById(R.id.meta_link);
            holder.dateCreated = (TextView) convertView.findViewById(R.id.date_created);
            holder.metaImage = (ImageView) convertView.findViewById(R.id.meta_image);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (pustakaTautan != null){
            holder.link.setText(pustakaTautan.getMETA_LINK());
            holder.metaTitle.setText(pustakaTautan.getMETA_TITLE());
            holder.metaDesc.setText(pustakaTautan.getDESKRIPSI_TAUTAN());
            holder.dateCreated.setText(Util.prettyDate(pustakaTautan.getDATE_CREATED()));
            if (!pustakaTautan.getMETA_IMAGE().equals("") && !pustakaTautan.getMETA_IMAGE().equals("null")){
                pictureHelper.displayImage(pustakaTautan.getMETA_LINK() +"/"+ pustakaTautan.getMETA_IMAGE(), holder.metaImage);
            } else {
                holder.metaImage.setImageDrawable(null);
            }
        }
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        TextView metaTitle, link, metaDesc, dateCreated;
        ImageView metaImage;
    }
}

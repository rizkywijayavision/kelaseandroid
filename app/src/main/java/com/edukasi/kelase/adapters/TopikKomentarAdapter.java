package com.edukasi.kelase.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.Util;
import com.edukasi.kelase.entities.KelaseUtil;
import com.edukasi.kelase.entities.TopikKomentar;
import com.edukasi.kelase.helpers.PictureHelper;
import com.pkmmte.view.CircularImageView;

/**
 * Created by ebizu-rizky on 12/3/15.
 */
public class TopikKomentarAdapter extends BaseAdapter<TopikKomentar>{

    private PictureHelper pictureHelper;

    public TopikKomentarAdapter(Context context) {
        super(context);
        pictureHelper = PictureHelper.getInstance(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final TopikKomentar topikKomentar = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_komentar, parent, false);
            holder = new ViewHolder();
            holder.textName = (TextView) convertView.findViewById(R.id.komentaritem_namalbl);
            holder.textDate = (TextView) convertView.findViewById(R.id.komentaritem_timestamplbl);
            holder.textComment = (TextView) convertView.findViewById(R.id.komentaritem_komentarlbl);
            holder.imageViewProfpic = (CircularImageView) convertView.findViewById(R.id.komentaritem_propic);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (topikKomentar != null){
            holder.textName.setText(topikKomentar.getNAMA_PENGGUNA());
            holder.textDate.setText(Util.prettyDate(topikKomentar.getDATE_CREATED()));
            holder.textComment.setText(topikKomentar.getKOMENTAR());
            pictureHelper.displayImage(KelaseUtil.KELASE_SERVICE_IMAGE_URL + topikKomentar.getIMAGE_PENGGUNA(), holder.imageViewProfpic);
        }
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        TextView textName, textDate, textComment;
        CircularImageView imageViewProfpic;
    }
}

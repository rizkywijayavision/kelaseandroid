/**
 * Base Adapter
 * @author egiadtya
 * 27 October 2014
 */
package com.edukasi.kelase.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

public abstract class BaseAdapter<T> extends android.widget.BaseAdapter {
    protected List<T> Data;
    protected Context context = null;
    protected LayoutInflater inflater = null;
    protected List<T> originalList;
    private ImageLoader imageLoader;

    public BaseAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getCount() {
        return this.Data.size();
    }

    @Override
    public T getItem(int postion) {
        return Data.get(postion);
    }

    public List<T> getData() {
        return Data;
    }

    public void setData(List<T> data) {
        Data = data;
        originalList = data;
    }

    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);

    @Override
    public abstract long getItemId(int position);

    public void loadImage(String uri, final ImageView imageView, ImageSize imageSize) {
        imageLoader.loadImage(uri, imageSize, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (imageView != null) {
                    imageView.setImageBitmap(loadedImage);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                // TODO Auto-generated method stub

            }
        });
    }
}

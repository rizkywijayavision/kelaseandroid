package com.edukasi.kelase.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.entities.KelasSesi;
import com.edukasi.kelase.libs.expandablelayout.ExpandableLayoutListener;
import com.edukasi.kelase.libs.expandablelayout.ExpandableRelativeLayout;
import com.edukasi.kelase.libs.expandablelayout.Utils;

import org.sufficientlysecure.htmltextview.HtmlTextView;

/**
 * Created by ebizu-rizky on 11/30/15.
 */
public class SesiListAdapter extends BaseAdapter<KelasSesi> {

    public SesiListAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final KelasSesi kelasSesi = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_list_sesi, parent, false);
            holder = new ViewHolder();
            holder.titleSesi = (TextView) convertView.findViewById(R.id.title_sesi);
            holder.expandButton = (RelativeLayout) convertView.findViewById(R.id.expand_button);
            holder.descSesi = (HtmlTextView) convertView.findViewById(R.id.desc_sesi);
            holder.parentSubSesi = (LinearLayout) convertView.findViewById(R.id.parent_sub_sesi);
            holder.sesiTriangle = convertView.findViewById(R.id.sesi_triangle);
            holder.descExpandLayout = (ExpandableRelativeLayout) convertView.findViewById(R.id.expandableLayout);
            holder.descExpandLayout.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
            holder.descExpandLayout.setListener(new ExpandableLayoutListener(){
                @Override
                public void onAnimationStart() {
                }

                @Override
                public void onAnimationEnd() {

                }

                @Override
                public void onPreOpen() {
                    createRotateAnimator(holder.sesiTriangle, 0f, 180f).start();
                }

                @Override
                public void onPreClose() {
                    createRotateAnimator(holder.sesiTriangle, 180f, 0f).start();
                }

                @Override
                public void onOpened() {

                }

                @Override
                public void onClosed() {

                }
            });
            holder.expandButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    if (view == holder.expandButton) {
                        holder.descExpandLayout.toggle();
                    }
                }
            });
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (kelasSesi != null){
            holder.titleSesi.setText("Sesi - " + kelasSesi.getId());
            holder.descSesi.setHtmlFromString(kelasSesi.getDESKRIPSI_SESI(), new HtmlTextView.RemoteImageGetter());
        }
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        TextView titleSesi;
        RelativeLayout expandButton;
        HtmlTextView descSesi;
        LinearLayout parentSubSesi;
        View sesiTriangle;
        ExpandableRelativeLayout descExpandLayout;
    }

    private View.OnClickListener onClickListener;
    private ExpandableLayoutListener expandableLayoutListener;

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }
}

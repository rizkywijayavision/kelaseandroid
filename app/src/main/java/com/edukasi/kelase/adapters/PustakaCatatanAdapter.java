package com.edukasi.kelase.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.edukasi.kelase.R;
import com.edukasi.kelase.Util;
import com.edukasi.kelase.entities.PustakaCatatan;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaCatatanAdapter extends BaseAdapter<PustakaCatatan> {

    public PustakaCatatanAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final PustakaCatatan pustakaCatatan = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_pustaka_catatan, parent, false);
            holder = new ViewHolder();
            holder.judulCatatan = (TextView) convertView.findViewById(R.id.judul_catatan);
            holder.date = (TextView) convertView.findViewById(R.id.date_created);
            holder.konten = (TextView) convertView.findViewById(R.id.content);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (pustakaCatatan != null){
            holder.judulCatatan.setText(pustakaCatatan.getJUDUL_CATATAN());
            holder.date.setText(Util.prettyDate(pustakaCatatan.getDATE_CREATED()));
            holder.konten.setText(pustakaCatatan.getKONTEN_CATATAN());
        }
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        TextView judulCatatan, date, konten;
    }
}
package com.edukasi.kelase.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.edukasi.kelase.R;
import com.edukasi.kelase.Util;
import com.edukasi.kelase.entities.PustakaVideo;

/**
 * Created by ebizu-rizky on 12/13/15.
 */
public class PustakaVideoAdapter extends BaseAdapter<PustakaVideo> {

    public PustakaVideoAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final PustakaVideo pustakaVideo = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_pustaka_photo, parent, false);
            holder = new ViewHolder();
            holder.judulCatatan = (TextView) convertView.findViewById(R.id.judul_catatan);
            holder.date = (TextView) convertView.findViewById(R.id.date_created);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (pustakaVideo != null){
            holder.judulCatatan.setText(pustakaVideo.getNAMA_VIDEO());
            holder.date.setText(Util.prettyDate(pustakaVideo.getDATE_CREATED()));
        }
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    private static class ViewHolder {
        TextView judulCatatan, date;
        ImageView image;
    }
}

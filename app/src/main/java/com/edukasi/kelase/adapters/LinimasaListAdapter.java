package com.edukasi.kelase.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edukasi.kelase.App;
import com.edukasi.kelase.R;
import com.edukasi.kelase.activities.CommentActivity;
import com.edukasi.kelase.controllers.LikeStatusController;
import com.edukasi.kelase.entities.KelaseUtil;
import com.edukasi.kelase.entities.MomentListitem;
import com.edukasi.kelase.helpers.PictureHelper;
import com.pkmmte.view.CircularImageView;

/**
 * Created by ebizu-rizky on 11/19/15.
 */
public class LinimasaListAdapter extends BaseAdapter<MomentListitem> {

    private PictureHelper pictureHelper;

    public LinimasaListAdapter(Context context) {
        super(context);
        pictureHelper = PictureHelper.getInstance(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final MomentListitem momentListitem = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.linimasa_listitem, parent, false);
            holder = new ViewHolder();
            holder.textviewName = (TextView) convertView.findViewById(R.id.linimasamoment_namalbl);
            holder.textViewTime = (TextView) convertView.findViewById(R.id.linimasamoment_timestamplbl);
            holder.textviewLink = (TextView) convertView.findViewById(R.id.linimasamoment_link);
            holder.textViewLinkname = (TextView) convertView.findViewById(R.id.linimasamoment_linkname);
            holder.imageViewImagestatus = (ImageView) convertView.findViewById(R.id.linimasamoment_imagestatus);
            holder.textViewStatus = (TextView) convertView.findViewById(R.id.linimasamoment_statuslbl);
            holder.imageViewMetaimage = (ImageView) convertView.findViewById(R.id.linimasamoment_metaimage);
            holder.textViewMetatitle = (TextView) convertView.findViewById(R.id.linimasamoment_metatitle);
            holder.textViewMetadesc = (TextView) convertView.findViewById(R.id.linimasamoment_metadesc);
            holder.textViewMetauri = (TextView) convertView.findViewById(R.id.linimasamoment_metaurl);
            holder.metaLayout = (LinearLayout) convertView.findViewById(R.id.linimasamoment_metalayout);
            holder.imageViewProfpic = (CircularImageView) convertView.findViewById(R.id.linimasamoment_propic);
            holder.likeButton = (ImageView) convertView.findViewById(R.id.linimasa_like);
            holder.unlikeButton = (ImageView) convertView.findViewById(R.id.linimasa_unlike);
            holder.textViewFavcount = (TextView) convertView.findViewById(R.id.linimasamoment_favcount);
            holder.clickComment = (TextView) convertView.findViewById(R.id.linimasamoment_comment);
            holder.commentCount = (TextView) convertView.findViewById(R.id.linimasamoment_commentcount);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(momentListitem != null){
            pictureHelper.displayImage(KelaseUtil.KELASE_SERVICE_IMAGE_URL + momentListitem.getIMAGE(), holder.imageViewProfpic);
            holder.textviewName.setText(momentListitem.getNAMA_DEPAN() + " " + momentListitem.getNAMA_BELAKANG());
            holder.textViewTime.setText(momentListitem.getDATE_CREATED());
            holder.textViewStatus.setText(momentListitem.getSTATUS());
            holder.textViewStatus.setVisibility(View.VISIBLE);
            holder.textViewFavcount.setText(momentListitem.getCount_favorit());
            if (!momentListitem.getIs_rate().equals("0")) {
                holder.unlikeButton.setVisibility(View.VISIBLE);
                holder.likeButton.setVisibility(View.GONE);
            }else {
                holder.likeButton.setVisibility(View.VISIBLE);
                holder.unlikeButton.setVisibility(View.GONE);
            }

            holder.imageViewImagestatus.setVisibility(View.GONE);
            holder.textviewLink.setVisibility(View.GONE);
            holder.textViewLinkname.setVisibility(View.GONE);
            holder.imageViewMetaimage.setVisibility(View.GONE);
            holder.metaLayout.setVisibility(View.GONE);

            int typeStatus = Integer.parseInt(momentListitem.getTYPE_STATUS());
            switch(typeStatus){
                case 2:
                    holder.textViewStatus.setText(momentListitem.getSTATUS());
                    holder.textViewStatus.setVisibility(View.VISIBLE);
                    //load image status
                    if (momentListitem.getIMAGE_STATUS() != null || momentListitem.getIMAGE_STATUS() != "") {

                        pictureHelper.displayImage(KelaseUtil.KELASE_SERVICE_IMAGE_STATUS + momentListitem.getIMAGE_STATUS(),
                                holder.imageViewImagestatus);
                        holder.imageViewImagestatus.setVisibility(View.VISIBLE);
                    }
                    break;

                case 3:
                    holder.textViewStatus.setText(momentListitem.getSTATUS());
                    holder.textViewStatus.setVisibility(View.VISIBLE);
                    //load link
                    holder.textviewLink.setText(momentListitem.getLINK());
                    holder.textViewLinkname.setText(momentListitem.getLINK_NAME());


                    if (momentListitem.getLINK() != null) {
                        holder.textviewLink.setVisibility(View.VISIBLE);
                    }
                    if (momentListitem.getLINK_NAME() != null) {
                        holder.textViewLinkname.setVisibility(View.VISIBLE);
                    }
                    break;

                case 4:
                    holder.textViewStatus.setText(momentListitem.getSTATUS());
                    holder.textViewStatus.setVisibility(View.VISIBLE);
                    //load meta content

                    String none = "none";

                    if (momentListitem.getMETA_IMAGE().toString().equals(none)) {
                        holder.imageViewMetaimage.setVisibility(View.GONE);
                    }else{

                        String image_metauri = KelaseUtil.KELASE_SERVICE_IMAGE_STATUS + momentListitem.getMETA_IMAGE();
                        pictureHelper.displayImage(KelaseUtil.KELASE_SERVICE_IMAGE_STATUS + momentListitem.getIMAGE_STATUS(),
                                holder.imageViewMetaimage);
                        holder.imageViewMetaimage.setVisibility(View.VISIBLE);
                    }

                    holder.textViewMetatitle.setText(momentListitem.getMETA_TITLE());
                    holder.textViewMetadesc.setText(momentListitem.getMETA_DESCRIPTION());
                    holder.textViewMetauri.setText(momentListitem.getMETA_URL());

                    if (momentListitem.getMETA_DESCRIPTION() != null){
                        holder.textViewMetadesc.setVisibility(View.VISIBLE);
                    }

                    if (momentListitem.getMETA_TITLE() != null){
                        holder.textViewMetatitle.setVisibility(View.VISIBLE);
                    }

                    if (momentListitem.getMETA_URL() != null){
                        holder.textViewMetauri.setVisibility(View.VISIBLE);
                    }
                    holder.metaLayout.setVisibility(View.VISIBLE);

                    //load link

                    holder.textviewLink.setText(momentListitem.getLINK());
                    holder.textViewLinkname.setText(momentListitem.getLINK_NAME());


                    if (momentListitem.getLINK() != null) {
                        holder.textviewLink.setVisibility(View.VISIBLE);
                    }
                    if (momentListitem.getLINK_NAME() != null) {
                        holder.textViewLinkname.setVisibility(View.VISIBLE);
                    }


                    break;

                case 7:
                    holder.textViewStatus.setText(momentListitem.getSTATUS());
                    holder.textViewStatus.setVisibility(View.VISIBLE);

                    //load link
                    holder.textviewLink.setText(momentListitem.getLINK());
                    holder.textViewLinkname.setText(momentListitem.getLINK_NAME());


                    if (momentListitem.getLINK() != null) {
                        holder.textviewLink.setVisibility(View.VISIBLE);
                    }
                    if (momentListitem.getLINK_NAME() != null) {
                        holder.textViewLinkname.setVisibility(View.VISIBLE);
                    }
                    break;
            }

            holder.likeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new LikeStatusController(context, momentListitem.getID_PENGGUNA(), momentListitem.getID_STATUS_PENGGUNA(), "0"){
                        @Override
                        public void onAPIsuccess() {
                            super.onAPIsuccess();
                            int countfav = Integer.parseInt(String.valueOf(holder.textViewFavcount.getText()));
                            holder.likeButton.setVisibility(View.GONE);
                            holder.unlikeButton.setVisibility(View.VISIBLE);
                            countfav++;
                            String fav = String.valueOf(countfav);
                            holder.textViewFavcount.setText(fav);
                            momentListitem.setIs_rate("1");
                        }
                    }.executeAPI();
                }
            });

            holder.unlikeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new LikeStatusController(context, momentListitem.getID_PENGGUNA(), momentListitem.getID_STATUS_PENGGUNA(), "1"){
                        @Override
                        public void onAPIsuccess() {
                            super.onAPIsuccess();
                            int countfav = Integer.parseInt(String.valueOf(holder.textViewFavcount.getText()));
                            holder.unlikeButton.setVisibility(View.GONE);
                            holder.likeButton.setVisibility(View.VISIBLE);
                            countfav--;
                            String fav = String.valueOf(countfav);
                            holder.textViewFavcount.setText(fav);
                            momentListitem.setIs_rate("0");
                        }
                    }.executeAPI();
                }
            });

            holder.clickComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intents = new Intent(context, CommentActivity.class);
                    intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intents.putExtra("ID_STATUS_PENGGUNA", momentListitem.getID_STATUS_PENGGUNA());
                    intents.putExtra("ID_PEMILIK_STATUS", momentListitem.getID_PENGGUNA());
                    intents.putExtra("ID_PENGGUNA", App.session.getUserDetails().getId());
                    context.startActivity(intents);
                    ((Activity)context).finish();
                }
            });
            holder.commentCount.setText(momentListitem.getCOUNT_COMMENT());
        }
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        TextView textviewName, textViewTime, textviewLink, textViewLinkname, textViewStatus, textViewMetatitle, textViewMetadesc, textViewMetauri, textViewFavcount, commentCount, clickComment;
        ImageView imageViewImagestatus, imageViewMetaimage, likeButton, unlikeButton;
        LinearLayout metaLayout;
        CircularImageView imageViewProfpic;
    }
}

#!/bin/bash
adb shell "run-as $1 chmod -R 777 /data/data/$1/databases"
adb shell "mkdir -p /sdcard/tempDB"
adb shell "cp -r /data/data/$1/databases/ /sdcard/tempDB/$i"
sudo mkdir -p $1 
adb pull sdcard/tempDB/ $1
adb shell "rm -r /sdcard/tempDB/*"
